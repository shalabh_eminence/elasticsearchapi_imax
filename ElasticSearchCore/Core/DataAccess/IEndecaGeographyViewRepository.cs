﻿using ElasticSearchCore.Core.Domain;
using System.Collections.Generic;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaGeographyViewRepository
    {
        List<EndecaGeographyView> GetAllGeography();
        List<EndecaGeographyView> GetAllGeographyThatHaveProperties();
        List<EndecaGeographyView> GetGeographyByInventoryID(long inventoryID);
        List<long> GetInventoryIDsByCountry(List<string> countries);
        List<long> GetInventoryIDsByDestination(List<string> destinations);
        List<long> GetInventoryIDsByDestinationCategoryName(List<string> destinationCategoryNames);
    }
}
