﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaNeighborhoodViewRepository
    {
        List<EndecaNeighborhoodView> GetAllNeighborhoods();
        List<EndecaNeighborhoodView> GetAllNeighborhoodsWithProperties();
        List<EndecaNeighborhoodView> GetNeighborhoodsByInventoryID(long inventoryID);
    }
}
