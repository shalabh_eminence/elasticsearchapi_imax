﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IMerchandisingLandingPageRepository
    {
        List<MerchandisingLandingPageView> GetAllLandingPages();
        List<MerchandisingLandingPageView> GetAllLandingPagesWithProperties();
        List<MerchandisingLandingPageView> GetLandingPagesByInventoryID(long inventoryID);
    }
}
