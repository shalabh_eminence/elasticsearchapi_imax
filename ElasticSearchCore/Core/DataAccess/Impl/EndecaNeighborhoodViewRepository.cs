﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaNeighborhoodViewRepository: IEndecaNeighborhoodViewRepository
    {
        private Connection _connection; 

        public EndecaNeighborhoodViewRepository()
        {
            _connection = new Connection();
        }

        public List<EndecaNeighborhoodView> GetAllNeighborhoods()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaNeighborhoodViews
                        select x).ToList();
            }
        }

        public List<EndecaNeighborhoodView> GetAllNeighborhoodsWithProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.EndecaNeighborhoodViews on
                        x.InventoryID equals y.InventoryID
                        select y).ToList();
            }
        }

        public List<EndecaNeighborhoodView> GetNeighborhoodsByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaNeighborhoodViews
                        where x.InventoryID == inventoryID
                        select x).ToList();
            }
        }
    }
}
