﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaBusinessUnitRepository: IEndecaBusinessUnitRepository
    {
        private Connection _connection;

        public EndecaBusinessUnitRepository()
        {
            _connection = new Connection();
        }

        public List<EndecaBusinessUnit> GetAllBusinessUnits()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaBusinessUnits
                        join y in context.EndecaPropertyViews
                        on x.InventoryId equals y.InventoryID
                        where y.Status.ToLower() == "active"
                        select x).ToList();
            }
        }

        public List<string> GetBusinessUnitNames()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaBusinessUnits
                        select x.BusinessUnitName).Distinct().ToList();
            }
        }

        public List<EndecaBusinessUnit> GetBusinessUnitsByBusinessUnitName(string businessUnitName)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaBusinessUnits
                        join y in context.EndecaPropertyViews
                        on x.InventoryId equals y.InventoryID
                        where x.BusinessUnitName == businessUnitName &&
                        y.Status.ToLower() == "active"
                        select x).ToList();
            }
        }

        public List<EndecaBusinessUnit> GetBusinessUnitsByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaBusinessUnits
                        join y in context.EndecaPropertyViews
                        on x.InventoryId equals y.InventoryID
                        where x.InventoryId == inventoryID &&
                        y.Status.ToLower() == "active"
                        select x).ToList();
                //return (from x in context.EndecaBusinessUnits
                //         where x.InventoryId == inventoryID
                //         select x).ToList();
            }
        }

        public List<long> GetInventoryIDsByBusinessUnitNames(List<string> businessUnitNames)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> inventoryIDs = new List<long>();

                foreach(string busUnitName in businessUnitNames)
                {
                    inventoryIDs.AddRange((from x in context.EndecaBusinessUnits
                                           join y in context.EndecaPropertyViews
                                            on x.InventoryId equals y.InventoryID
                                           where x.BusinessUnitName.ToLower() == busUnitName.ToLower() &&
                                           y.Status.ToLower() == "active"
                                           select x.InventoryId).ToList());
                }

                return inventoryIDs;
            }
        }
    }
}
