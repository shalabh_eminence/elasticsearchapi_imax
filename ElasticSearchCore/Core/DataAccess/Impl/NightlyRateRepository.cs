﻿using ElasticSearchCore.Core.Domain;
using ElasticSearchCore.Models.Insert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class NightlyRateRepository : INightlyRateRepository
    {
        private Connection _connection;

        public NightlyRateRepository()
        {
            _connection = new Connection();
        }

        public List<NightlyRate> GetOneYearNightlyRatesForProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> ids = (from x in context.EndecaPropertyViews
                                  select x.InventoryID).ToList();

                List<NightlyRate> rates = new List<NightlyRate>();

                foreach (long id in ids)
                {
                    List<NightlyRate> r = (from x in context.RentalUnitContentViews
                                           join z in context.NightlyRates //supposed to be 
                                           on x.RentalUnitId equals z.RentalUnitId
                                           where x.InventoryId == id &&
                                           z.Date.Date >= DateTime.Now.Date && z.Date.Date <= DateTime.Now.AddDays(365).Date
                                           select z).ToList();

                    rates.AddRange(r);
                }

                return rates;
            }
        }

        public List<NightlyRate> GetOneYearNightlyRatesForProperties(long rentalUnitID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                //List<long> ids = (from x in context.EndecaPropertyViews
                //                  select x.InventoryID).ToList();

                List<NightlyRate> rates = new List<NightlyRate>();

                //foreach (long id in ids)
                //{
                    List<NightlyRate> r = (from x in context.RentalUnitContentViews
                                           join z in context.NightlyRates //supposed to be 
                                           on x.RentalUnitId equals z.RentalUnitId where
                                           //where x.InventoryId == id &&
                                           x.RentalUnitId == rentalUnitID &&
                                           z.Date.Date >= DateTime.Now.Date && z.Date.Date <= DateTime.Now.AddDays(365).Date
                                           select z).ToList();

                    rates.AddRange(r);
                //}

                return rates;
            }
        }

        public List<NightlyAverage> GetYearAverageNightlyRate()
        {
            List<NightlyAverage> averages = new List<NightlyAverage>();

            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> ids = (from x in context.EndecaPropertyViews
                                    select x.InventoryID).ToList();

                foreach (long id in ids)
                {
                    //get newest rates
                    List<decimal> rates = (from x in context.RentalUnitContentViews
                                             join z in context.NightlyRates //supposed to be 
                                             on x.RentalUnitId equals z.RentalUnitId
                                             where x.InventoryId == id &&
                                             z.Date.Date >= DateTime.Now.Date.AddDays(15) && z.Date.Date <= DateTime.Now.AddDays(45).Date
                                             select z.RackRate).ToList();

                    //if there are no rates, in this date range, get oldest rates
                    if(rates.Count == 0)
                    {
                        rates = (from x in context.RentalUnitContentViews
                                 join z in context.NightlyRates //supposed to be 
                                 on x.RentalUnitId equals z.RentalUnitId
                                 where x.InventoryId == id 
                                 orderby z.NightlyRateId ascending
                                 select z.RackRate).Take(30).ToList();
                    }

                    decimal lowAgv = 0/*decimal.MaxValue*/;

                    rates = (from x in rates
                             orderby x ascending
                             select x).ToList();

                    if(rates.Count >= 4)
                    {
                        lowAgv = (rates[0] + rates[1] + rates[2] + rates[3]) / (decimal)4;
                    }

                    //for (int i=0; i < rates.Count-4; i++)
                    //{
                    //    decimal average = (rates[i] + rates[i + 1] + rates[i + 2] + rates[i + 3]) / (decimal)4;

                    //    if (average < lowAgv)
                    //        lowAgv = average;
                    //}

                    NightlyAverage nightlyAverage = new NightlyAverage()
                    {
                        InventoryID = id,
                        YearAverageRate = lowAgv
                    };

                    if(lowAgv != 0)
                        averages.Add(nightlyAverage);
                }

                return averages;
            }
        }

        

        //public NightlyAverage GetYearAverageNightlyRateByInventoryID(long inventoryID)
        //{
        //    RentalUnitContentViewRepository rv = new RentalUnitContentViewRepository();
        //    List<RentalUnitContentView> rus = rv.GetRentalContentViewByInventoryID(inventoryID);
        //    RentalUnitContentView ru = null;

        //    ru = rus.Count > 0 ? rus[0] : new RentalUnitContentView();
        //    decimal sumRates = 0;

        //    using (ElasticSearchCoreDataContext context = _connection.GetContext())
        //    {
        //        List<NightlyRate> rates = (from x in context.NightlyRates
        //                                     where x.RentalUnitId == ru.RentalUnitId
        //                                     orderby x.NightlyRateId descending
        //                                     select x).Take(365).ToList();

        //        foreach(NightlyRate r in rates)
        //        {
        //            sumRates += r.RackRate;
        //        }
        //    }

        //    NightlyAverage avg = new NightlyAverage()
        //    {
        //        InventoryID = inventoryID,
        //        YearAverageRate = (double)sumRates / (double)365
        //    };

        //    return avg;
        //}


    }
}
