﻿using System.Collections.Generic;
using System.Linq;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaWeeklyRatesViewRepository : IEndecaWeeklyRatesViewRepository
    {
        private Connection _connection;

        public EndecaWeeklyRatesViewRepository()
        {
            _connection = new Connection();
        }

        public List<EndecaWeeklyRatesView> GetAllWeeklyRates()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaWeeklyRatesViews
                        select x).ToList();
            }
        }

        public List<EndecaWeeklyRatesView> GetAllWeeklyRatesWithProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.EndecaWeeklyRatesViews
                        on x.InventoryID equals y.InventoryID
                        select y).ToList();
            }
        }

        public List<EndecaWeeklyRatesView> GetWeeklyRatesByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaWeeklyRatesViews
                        where x.InventoryID == inventoryID
                        select x).ToList();
            }
        }
    }
}
