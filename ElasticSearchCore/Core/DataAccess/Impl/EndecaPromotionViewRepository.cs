﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaPromotionViewRepository : IEndecaPromotionViewRepository
    {
        private Connection _connection;

        public EndecaPromotionViewRepository()
        {
            _connection = new Connection();
        }

        public List<EndecaPromotionView> GetAllPromotions()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPromotionViews
                        select x).ToList();
            }
        }

        public List<EndecaPromotionView> GetAllPromotionsWithProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.EndecaPromotionViews
                        on x.InventoryID equals y.InventoryID
                        select y).ToList();
            }
        }

        public List<EndecaPromotionView> GetPromotionsByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPromotionViews
                        where x.InventoryID == inventoryID
                        select x).ToList();
            }
        }
    }
}
