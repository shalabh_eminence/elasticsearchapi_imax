﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class StandAloneRentalUnitRoomTypeRepository : IStandAloneRentalUnitRoomTypeRepository
    {
        private Connection _connection;

        public StandAloneRentalUnitRoomTypeRepository()
        {
            _connection = new Connection();
        }

        public List<StandAloneRentalUnitRoomTypeView> GetAllPropertiesview()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.StandAloneRentalUnitRoomTypeViews
                        select x).ToList();
            }
        }

        public StandAloneRentalUnitRoomTypeView GetPropertyView(long rentalUnitID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.StandAloneRentalUnitRoomTypeViews
                        where x.RentalUnitId == rentalUnitID
                        select x).FirstOrDefault();
            }
        }
    }
}
