﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaPropertyViewRepository
    {
        List<EndecaPropertyView> GetAllProperties();
        EndecaPropertyView GetPropertyByInventoryID(long inventoryID);
        List<long> GetInventoryIDsByBeds(List<string> beds);
        Tuple<List<long>, List<long>> GetPropertyInventoryIDsByCountry(string status, string luxury, List<string> countries, string businessUnit);
        List<long> GetFeaturedPropertyIDsByInventoryIDs(List<long> inventoryIDs);
        List<long> GetPropertyInventoryIDsByDestinations(string status, string luxury, List<string> destinations, string businessUnit);
        List<long> GetPropertyInventoryIDsByDestinationTypes(string status, string luxury, List<string> destinationTypes, string businessUnit);
    }
}
