﻿using ElasticSearch.Models.Responses;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace ElasticSearchCore.API.Connector
{
    public class ElasticSearchConnection
    {
        private string host = null;
        private string port = null;

        /// <summary>
        /// Establishes a connection to ElasticSearch
        /// </summary>
        public ElasticSearchConnection()
        {
            host = System.Configuration.ConfigurationSettings.AppSettings.Get("ElasticSearchHost");
            port = System.Configuration.ConfigurationSettings.AppSettings.Get("ElasticSearchPort");
        }

        /// <summary>
        /// Constructs a HTTPWebRequest to a given index on ElasticSearch
        /// </summary>
        /// <param name="path">the path to the index</param>
        /// <returns></returns>
        private HttpWebRequest GetWebRequest(string method, string path)
        {
            var baseAddress = $"http://{host}:{port}/{path}";

            var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
            http.Accept = "application/json";

            if(method.ToUpper() != "DELETE")
                http.ContentType = "application/json";

            http.Method = method.ToUpper();

            return http;
        }

        /// <summary>
        ///  Execute a given query on ElasticSearch
        /// </summary>
        /// <typeparam name="T">The type of return object from ElasticSearch</typeparam>
        /// <param name="path">the path to the index at ElasticSearch</param>
        /// <param name="query">the query to execute</param>
        /// <returns></returns>
        public T Execute<T>(string path, string query)
        {
            HttpWebRequest http = GetWebRequest("POST", path);

            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(query);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        /// <summary>
        ///  Execute a given query on ElasticSearch
        /// </summary>
        /// <typeparam name="T">The type of return object from ElasticSearch</typeparam>
        /// <param name="path">the path to the index at ElasticSearch</param>
        /// <param name="query">the query to execute</param>
        /// <param name="postMethod">PUT, POST, etc.</param>
        /// <returns></returns>
        public T Execute<T>(string path, string query, string postMethod)
        {
            HttpWebRequest http = GetWebRequest(postMethod.ToUpper(), path);

            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(query);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        /// <summary>
        /// Deletes record from ElasticSearch
        /// </summary>
        /// <param name="path">the path to delete</param>
        /// <returns>ElasticDeleteResults object</returns>
        public ElasticDeleteResults Delete(string path)
        {
            HttpWebRequest http = GetWebRequest("DELETE", path);

            try
            {
                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                ElasticDeleteResults result = JsonConvert.DeserializeObject<ElasticDeleteResults>(content, settings);
                return result;
            }
            catch(WebException ex)
            {
                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    //wasn't found for deletion
                }
                else
                {
                    throw (ex);
                }
            }

            return new ElasticDeleteResults();
        }

        /// <summary>
        /// Deletes record from ElasticSearch
        /// </summary>
        /// <param name="path">the path to delete</param>
        /// <returns></returns>
        public T Delete<T>(string path)
        {
            HttpWebRequest http = GetWebRequest("DELETE", path);

            try
            {
                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                T result = JsonConvert.DeserializeObject<T>(content, settings);
                return result;
            }
            catch (WebException ex)
            {
                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    //wasn't found for deletion
                }
                else
                {
                    throw (ex);
                }
            }

            return default(T);
        }

        /// <summary>
        /// Indexes an object into ElasticSearch
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="path">The path to index the object into</param>
        /// <param name="obj">the object to index</param>
        /// <returns>ElasticCreateResult object</returns>
        public ElasticCreateResult Insert<T>(string path, T obj)
        {
            HttpWebRequest http = GetWebRequest("POST", path);

            using (var streamWriter = new StreamWriter(http.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(obj);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)http.GetResponse();
            ElasticCreateResult ret = new ElasticCreateResult();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                ret = JsonConvert.DeserializeObject<ElasticCreateResult>(result);
            }

            return ret;
        }
    }
}