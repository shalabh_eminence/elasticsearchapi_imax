﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearchCore.API.Connector;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class PropertyQuery
    {
        //private ElasticSearchConnection elasticSearchConnection = null;
        //private string path = "props/properties/_search";

        ElasticLowLevelClient lowLevelClient = null;

        public PropertyQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Get properties with the supplied inventoryIDs
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        private ElasticPropertySearchResults GetPropertiesByIDs(List<long> properties)
        {
            string propertyIDs = "[";

            for (int i = 0; i < properties.Count(); i++)
            {
                propertyIDs += $"{properties[i]}";
                if (i != properties.Count() - 1)
                    propertyIDs += ",";
            }
            propertyIDs += "]";

            string propertySearch = "{" +
                                "\"size\": 10000," +
                                  "\"query\" : {" +
                                    "\"terms\": { " +
                                        "\"InventoryID\": " + propertyIDs +
                                        "}" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("properties", "property", propertySearch);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticPropertySearchResults resultProperties = GetResponse<ElasticPropertySearchResults>(responseJson);

                return resultProperties;
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Get properties that are marked as featured, by the supplied IDs
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        private ElasticPropertySearchResults GetFeaturedPropertiesByIDs(List<long> properties)
        {
            string propertyIDs = "[";

            for (int i = 0; i < properties.Count(); i++)
            {
                propertyIDs += $"{properties[i]}";
                if (i != properties.Count() - 1)
                    propertyIDs += ",";
            }
            propertyIDs += "]";

            string query = "{" +
                            "\"size\": 10000," +
                            "\"query\": {" +
                                "\"bool\": {" +
                                    "\"must\": [" +
                                        "{" +
                                        "\"terms\": {" +
                                        "\"InventoryID\": " + propertyIDs +
                                        "}" +
                                    "}" +
                                "]," +
                                "\"filter\": {" +
                                "\"term\": {" +
                                    "\"IsFeaturedProperty\": true" +
                                "}" +
                                "}" +
                            "}" +
                            "}" +
                        "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("properties", "property", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticPropertySearchResults props = GetResponse<ElasticPropertySearchResults>(responseJson);

                return props;
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties by country
        /// </summary>
        /// <param name="country">the specified country</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertiesByCountry(string country)
        {
            string geographySearch = "{" +
                "\"from\" : 0," +
                "\"size\": 10000," +
                "\"query\" : {" +
                  "\"match_phrase\": {" +
                            "\"Country\": \"" + country + "\"" +
                  "}" +
               "}" +
            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", geographySearch);

            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult geographies = GetResponse<ElasticGeographySearchResult>(responseJson);

                List<long> properties = (from x in geographies.hits.hits
                                         select x._source.InventoryID).ToList();

                return GetPropertiesByIDs(properties);
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Get properties by lodging Type
        /// </summary>
        /// <param name="lodgingType">The name of the loding type</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertiesByLodgingType(string lodgingType)
        {
            string query = $"{{" +
                              $"\"query\": {{"+
                                $"\"match_phrase\": {{" +
                                    $"\"BuildingType\": \"{lodgingType}\""+
                                    $"}}" +
                                 $"}}" +
                            $"}}";

            var searchResponse = lowLevelClient.Search<StringResponse>("properties", "property", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticPropertySearchResults resultProperties = GetResponse<ElasticPropertySearchResults>(responseJson);

                return resultProperties;
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties by City
        /// </summary>
        /// <param name="city">the specified city</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertiesbyCity(string city)
        {
            string geographySearch = "{" +
                "\"from\" : 0," +
                "\"size\": 10000," +
                "\"query\" : {" +
                  "\"match_phrase\": {" +
                            "\"CityName\": \"" + city + "\""+
                  "}" +
               "}" +
            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", geographySearch);

            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult geographies = GetResponse<ElasticGeographySearchResult>(responseJson);

                List<long> properties = (from x in geographies.hits.hits
                                         select x._source.InventoryID).ToList();

                return GetPropertiesByIDs(properties);
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties by State
        /// </summary>
        /// <param name="state">the specified state</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertiesbyState(string state)
        {
            string geographySearch = "{" +
                "\"from\" : 0," +
                "\"size\": 10000," +
                "\"query\" : {" +
                  "\"match_phrase\": {" +
                            "\"StateName\": \"" + state + "\"" +
                  "}" +
               "}" +
            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", geographySearch);

            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult geographies = GetResponse<ElasticGeographySearchResult>(responseJson);

                List<long> properties = (from x in geographies.hits.hits
                                         select x._source.InventoryID).ToList();

                return GetPropertiesByIDs(properties);
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties by Destination
        /// </summary>
        /// <param name="destination">the specified destination</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertiesbyDestination(string destination)
        {
            string geographySearch = "{" +
                "\"from\" : 0," +
                "\"size\": 10000," +
                "\"query\" : {" +
                  "\"match_phrase\": {" +
                            "\"Destination\": \"" + destination + "\"" +
                  "}" +
               "}" +
            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", geographySearch);

            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult geographies = GetResponse<ElasticGeographySearchResult>(responseJson);

                List<long> properties = (from x in geographies.hits.hits
                                         select x._source.InventoryID).ToList();

                return GetPropertiesByIDs(properties);
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Get properties by DestinationCategoryName
        /// </summary>
        /// <param name="destinationCatName">the specified destination category name</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertiesbyDestinationCategoryName(string destinationCatName)
        {
            string geographySearch = "{" +
                "\"from\" : 0," +
                "\"size\": 10000," +
                "\"query\" : {" +
                  "\"match_phrase\": {" +
                            "\"DestinationCategoryName\": \"" + destinationCatName + "\"" +
                  "}" +
               "}" +
            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", geographySearch);

            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult geographies = GetResponse<ElasticGeographySearchResult>(responseJson);

                List<long> properties = (from x in geographies.hits.hits
                                         select x._source.InventoryID).ToList();

                return GetPropertiesByIDs(properties);
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties by resort
        /// </summary>
        /// <param name="resort">the specified resort</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        [Obsolete("Method is now obsolete.")]
        private ElasticPropertySearchResults GetPropertiesbyResort(string resort)
        {
            throw new NotImplementedException();

            //string query = "{" +
            //                "\"size\": 10000," +
            //                "\"query\" : {" +
            //                    "\"has_child\": {" +
            //                        "\"type\": \"resorts\"," +
            //                        "\"query\": {" +
            //                            "\"match\": {" +
            //                                "\"Resort\": \"" + resort + "\"" +
            //                                "}" +
            //                           " }" +
            //                        "}" +
            //                    "}" +
            //                "}";

            //return elasticSearchConnection.Execute<ElasticPropertySearchResults>(path, query);
        }

        /// <summary>
        /// Gets the properties by Neighborhood
        /// </summary>
        /// <param name="neighborhood">the specified neighborhood</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertiesbyNeighborhood(string neighborhood)
        {
            string query = "{" +
                              "\"size\": 10000," +
                              "\"query\": {" +
                                    "\"match_phrase\": {" +
                                        "\"Neighborhood\": \""+ neighborhood +"\"" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("neighborhoods", "neighborhood", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticNeighborhoodSearchResult neighborhoods = GetResponse<ElasticNeighborhoodSearchResult>(responseJson);

                List<long> properties = (from x in neighborhoods.hits.hits
                                         select x._source.InventoryID).ToList();

                return GetPropertiesByIDs(properties);
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties by bedrooms
        /// </summary>
        /// <param name="bedRoom">the selected number of bedrooms</param>
        /// <returns></returns>
        public ElasticPropertySearchResults GetPropertiesbyBed(string bedRoom)
        {
            string br = bedRoom.Replace("+", "").Replace("BR", "").Trim();
            bool containsPlus = bedRoom.Contains("+");
            string query = "";

            if (!containsPlus)
            {
                query = "{" +
                            "\"size\": 10000," +
                                "\"query\" : {" +
                                "\"term\": { " +
                                    "\"Bedroom\": { " +
                                        "\"value\": " + br +
                                    "}" +
                                "}" +
                            "}" +
                        "}";
            }
            else
            {
                query = "{" +
                            "\"size\": 10000," +
                                "\"query\" : {" +
                                "\"range\": { " +
                                    "\"Bedroom\": { " +
                                        "\"gte\": "+ br +
                                    "}" +
                                "}" +
                            "}" +
                        "}";
            }

            var searchResponse = lowLevelClient.Search<StringResponse>("properties", "property", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticPropertySearchResults resultProperties = GetResponse<ElasticPropertySearchResults>(responseJson);

                return resultProperties;
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties by bedrooms, for luxury properties
        /// </summary>
        /// <param name="bedRoom">the selected number of bedrooms</param>
        /// <returns></returns>
        public ElasticPropertySearchResults GetLuxuryPropertiesbyBed(string bedRoom)
        {
            string br = bedRoom.Replace("+", "").Replace("BR", "").Trim();
            bool containsPlus = bedRoom.Contains("+");
            string query = "";

            if (!containsPlus)
            {
                query = "{" +
                            "\"size\": 10000,"+
                            "\"query\": {"+
                            "\"bool\": {"+
                                "\"must\": ["+
                                    "{"+
                                    "\"term\": {"+
                                    "\"Bedroom\": {"+
                                        "\"value\": " + br +
                                    "}"+
                                    "}"+
                                "},"+
                                "{"+
                                    "\"term\":{"+
                                    "\"IsLuxury\": {"+
                                        "\"value\": \"true\""+
                                    "}"+
                                    "}" +
                                "}" +
                                "]" +
                            "}" +
                            "}" +
                        "}";
            }
            else
            {

                query = "{" +
                            "\"size\": 10000," +
                            "\"query\": {" +
                            "\"bool\": {" +
                                "\"must\": [" +
                                    "{" +
                                    "\"range\": { " +
                                    "\"Bedroom\": { " +
                                        "\"gte\": " + br +
                                    "}" +
                                "}" +
                                "}," +
                                "{" +
                                    "\"term\":{" +
                                    "\"IsLuxury\": {" +
                                        "\"value\": \"true\"" +
                                    "}" +
                                    "}" +
                                "}" +
                                "]" +
                            "}" +
                            "}" +
                        "}";
            }

            var searchResponse = lowLevelClient.Search<StringResponse>("properties", "property", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticPropertySearchResults resultProperties = GetResponse<ElasticPropertySearchResults>(responseJson);

                return resultProperties;
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets properties that are marked as 'featured', by country
        /// </summary>
        /// <param name="country">the specified country</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetFeaturedPropertiesByCountry(string country)
        {
            ElasticPropertySearchResults countryProperties = GetPropertiesByCountry(country);
            List<long> countryPropertyIDs = (from x in countryProperties.hits.hits
                                            select x._source.InventoryID).ToList();

            return GetFeaturedPropertiesByIDs(countryPropertyIDs);
        }

        /// <summary>
        /// Gets featured properties by City
        /// </summary>
        /// <param name="city">the specified city</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetFeaturedPropertiesbyCity(string city)
        {
            ElasticPropertySearchResults cityProperties = GetPropertiesbyCity(city);
            List<long> cityPropertyIDs = (from x in cityProperties.hits.hits
                                             select x._source.InventoryID).ToList();

            return GetFeaturedPropertiesByIDs(cityPropertyIDs);
        }

        /// <summary>
        /// Get featured properties by State
        /// </summary>
        /// <param name="state">the specified state</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetFeaturedPropertiesbyState(string state)
        {
            ElasticPropertySearchResults stateProperties = GetPropertiesbyState(state);
            List<long> statePropertyIDs = (from x in stateProperties.hits.hits
                                          select x._source.InventoryID).ToList();

            return GetFeaturedPropertiesByIDs(statePropertyIDs);
        }

        /// <summary>
        /// Gets featured properties by Destination
        /// </summary>
        /// <param name="destination">the specified destination</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetFeaturedPropertiesbyDestination(string destination)
        {
            ElasticPropertySearchResults destinationProperties = GetPropertiesbyDestination(destination);
            List<long> destinationPropertyIDs = (from x in destinationProperties.hits.hits
                                           select x._source.InventoryID).ToList();

            return GetFeaturedPropertiesByIDs(destinationPropertyIDs);
        }

        /// <summary>
        /// Gets featured properties by Destination Category Name
        /// </summary>
        /// <param name="destination">the specified destination category name</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetFeaturedPropertiesbyDestinationCategoryName(string destinationCatName)
        {
            ElasticPropertySearchResults destinationCatNameProperties = GetPropertiesbyDestinationCategoryName(destinationCatName);
            List<long> destinationCatNamePropertyIDs = (from x in destinationCatNameProperties.hits.hits
                                                 select x._source.InventoryID).ToList();

            return GetFeaturedPropertiesByIDs(destinationCatNamePropertyIDs);
        }

        /// <summary>
        /// Gets featured properties by Resort 
        /// </summary>
        /// <param name="destinationCatName">the specified resort</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        [Obsolete("Method is now obsolete.")]
        public ElasticPropertySearchResults GetFeaturedPropertiesbyResort(string resort)
        {
            throw new NotImplementedException();

            //string query = "{" +
            //                "\"size\": 10000," +
            //                 "\"query\" : {" +
            //                   "\"bool\": {" +
            //                       "\"must\": [" +
            //                           "{" +
            //                               "\"has_child\": {" +
            //                               "\"type\": \"resorts\"," +
            //                                           "\"query\": {" +
            //                               "\"match\": {" +
            //                                   "\"Resort\": \"" + resort + "\"" +
            //                           "}" +
            //                       "}" +
            //                   "}" +
            //                   "}" +
            //                   "]," +
            //                   "\"must\": [" +
            //                   "{" +
            //                       "\"term\": {" +
            //                       "\"IsFeaturedProperty\": {" +
            //                           "\"value\": true" +
            //                       "}" +
            //                       "}" +
            //                   "}" +
            //                   "]" +
            //               "}" +
            //               "}" +
            //           "}";

            //return elasticSearchConnection.Execute<ElasticPropertySearchResults>(path, query);
        }

        /// <summary>
        /// Gets featured properties by neighborhood
        /// </summary>
        /// <param name="neighborhood">the specified neighborhood</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetFeaturedPropertiesbyNeighborhood(string neighborhood)
        {
            ElasticPropertySearchResults neighborhoods = GetPropertiesbyNeighborhood(neighborhood);
            List<long> neighborhoodIDs = (from x in neighborhoods.hits.hits
                                                        select x._source.InventoryID).ToList();

            return GetFeaturedPropertiesByIDs(neighborhoodIDs);
        }

        /// <summary>
        /// Gets a property by inventory ID
        /// </summary>
        /// <param name="inventoryID">the specified inventory ID</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertyByInventoryID(long inventoryID)
        {
            string query = "{"+
                            "\"size\": 10000," +
                              "\"query\":{" +
                                    "\"term\": {"+
                                        "\"InventoryID\": " + inventoryID +
                                    "}"+
                                "}"+
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("properties", "property", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticPropertySearchResults props = GetResponse<ElasticPropertySearchResults>(responseJson);

                return props;
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Gets a property by inventory IDs
        /// </summary>
        /// <param name="inventoryID">the specified inventory IDs</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertyByInventoryIDs(List<long> inventoryIDs)
        {
            return GetPropertiesByIDs(inventoryIDs);
        }

        /// <summary>
        /// Matches a phrase against property names
        /// </summary>
        /// <param name="name">the name or partial name of a property</param>
        /// <returns>ElasticPropertySearchResults object</returns>
        public ElasticPropertySearchResults GetPropertyByPartialName(string name)
        {
            string query = "{" +
                            "\"size\": 10000," +
                              "\"query\":{" +
                                    "\"match_phrase\" : {" +
                                      "\"ComplexName\":  \"" + name + "\""+
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("properties", "property", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticPropertySearchResults props = GetResponse<ElasticPropertySearchResults>(responseJson);

                return props;
            }

            return default(ElasticPropertySearchResults);
        }

        /// <summary>
        /// Deletes a property from ElasticSearch
        /// </summary>
        /// <param name="inventoryID">the inventory ID to delete</param>
        /// <returns></returns>
        public ElasticDeleteResults DeletePropertyByInventoryID(long inventoryID)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("properties", "property", inventoryID.ToString());
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }

        /// <summary>
        /// Gets all properties
        /// </summary>
        /// <returns></returns>
        public List<PropertySource> GetAllProperties()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("properties", "property", searchRequestParameters);

            List<PropertyHit> hits = new List<PropertyHit>();
            List<PropertySource> list = new List<PropertySource>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticPropertySearchResults props = GetResponse<ElasticPropertySearchResults>(responseJson);

                if(props.hits.hits.Count() != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticPropertySearchResults results = GetScrollResults<ElasticPropertySearchResults>(scrollID);

                    if (results.hits.hits.Count() == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            foreach(PropertyHit h in hits)
            {
                var x= (PropertySource)h._source;
                list.Add(x);
            }

            return list;
        }

        //Not implemented yet
        public void GetPropertiesBySearchPhrase(List<long> inventoryIDs, string searchText)
        {
            string ids = "";

            for(int i=0; i<inventoryIDs.Count(); i++)
            {
                ids += inventoryIDs[i].ToString();

                if (i != inventoryIDs.Count() - 1)
                    ids += ",";
            }

            string query = "{"+
                                "\"_source\": [\"highlights\", \"InventoryID\"], "+
                                "\"query\": { " +
                                "\"bool\": {"+
                                    "\"must\": ["+
                                    "{"+
                                        "\"terms\": {" +
                                        "\"InventoryID\": ["+
                                            ids +
                                        "]"+
                                        "}" +
                                    "}" +
                                    "], " +
                                    "\"should\": ["+
                                    "{"+
                                        "\"match_phrase\": {"+
                                        "\"ComplexName\": \""+ searchText +"\"" +
                                        "}"+
                                    "},"+
                                    "{"+
                                        "\"match_phrase\": {"+
                                        "\"InventoryDescription\": \""+ searchText +"\""+
                                        "}"+
                                    "}"+
                                    "],"+
                                    "\"minimum_should_match\": 1"+
                                "}" +
                                "},"+
                                "\"highlight\": {"+
                                "\"fields\": {"+
                                    "\"ComplexName\": {},"+
                                    "\"InventoryDescription\": {}"+
                                "}"+
                                "}"+
                            "}";


        }
    }
}