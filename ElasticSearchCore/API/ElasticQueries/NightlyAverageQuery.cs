﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses.NightlyAverage;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class NightlyAverageQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public NightlyAverageQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Gets a night average
        /// </summary>
        /// <param name="inventoryID">inventory id</param>
        /// <returns></returns>
        public ElasticNightlyAverageSearchResults GetNightlyAverageRateByInventoryID(long inventoryID)
        {
            //form the query to send to ElasticSearch

            string query = "{" +
                            "\"size\":10000," +
                              "\"query\" : { " +
                                    "\"term\" : {" +
                                      "\"InventoryID\":" + inventoryID +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("nightlyaverages", "nightlyAverage", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticNightlyAverageSearchResults results = GetResponse<ElasticNightlyAverageSearchResults>(responseJson);

                return results;
            }

            return default(ElasticNightlyAverageSearchResults);
        }

        /// <summary>
        /// Gets nightly averages
        /// </summary>
        /// <param name="inventoryIDs">list of inventory IDs</param>
        /// <returns></returns>
        public ElasticNightlyAverageSearchResults GetNightlyAverageRatesByInventoryIDs(List<long> inventoryIDs)
        {
            string idStr = "";

            for (int i = 0; i < inventoryIDs.Count; i++)
            {
                idStr += inventoryIDs[i].ToString();

                if (i < inventoryIDs.Count - 1)
                    idStr += ",";
            }

            //form the query to send to ElasticSearch
            string query = "{" +
                           "\"size\":10000," +
                             "\"query\" : { " +
                                   "\"terms\" : {" +
                                     "\"InventoryID\": [" + idStr + "]" +
                                   "}" +
                               "}" +
                           "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("nightlyaverages", "nightlyAverage", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticNightlyAverageSearchResults results = GetResponse<ElasticNightlyAverageSearchResults>(responseJson);

                return results;
            }

            return default(ElasticNightlyAverageSearchResults);
        }

        /// <summary>
        /// Gets all nightly averages from elasticsearch
        /// </summary>
        /// <returns></returns>
        public List<NightlyAverageHit> GetAllNightlyAverages()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("nightlyaverages", "nightlyAverage", searchRequestParameters);

            List<NightlyAverageHit> hits = new List<NightlyAverageHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticNightlyAverageSearchResults props = GetResponse<ElasticNightlyAverageSearchResults>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticNightlyAverageSearchResults results = GetScrollResults<ElasticNightlyAverageSearchResults>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }

        /// <summary>
        /// Deletes a document from the nightlyaverages index
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteAmenity(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("nightlyaverages", "nightlyAverage", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }
    }
}
