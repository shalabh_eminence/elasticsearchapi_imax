﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses.RentalUnitTypeView;
using ElasticSearchCore.Core.Domain;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class StandAloneRentalUnitRoomTypeViewQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public StandAloneRentalUnitRoomTypeViewQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        public List<RentalUnitTypeViewHit> GetAllRentalUnitTypeViews()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("rentalunittypeviews", "rentalTypeView", searchRequestParameters);

            List<RentalUnitTypeViewHit> hits = new List<RentalUnitTypeViewHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                RentalUnitTypeViewSearchResults props = GetResponse<RentalUnitTypeViewSearchResults>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    RentalUnitTypeViewSearchResults results = GetScrollResults<RentalUnitTypeViewSearchResults>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }
    }
}
