﻿using Elasticsearch.Net;
using ElasticSearchCore.Models.Responses;
using ElasticSearchCore.Models.Responses.Calendars;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class CalendarQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public CalendarQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Returns the rates and availability, by inventoryID
        /// </summary>
        /// <param name="inventoryID">The inventory Id of the unit/property</param>
        /// <returns></returns>
        public List<CalendarHit> GetRatesAndAvailability(long inventoryID)
        {
            string query = "{"+
                              "\"sort\" : ["+
                                    "{ \"Date\" : {\"order\" : \"asc\"}}" +
                                "]," +
                              "\"query\": {" +
                                "\"match\": {" +
                                  "\"InventoryID\": "+ inventoryID +
                                "}" +
                              "}" +
                            "}";

            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            var response = lowLevelClient.Search<StringResponse>("calendars", "calendar", query, searchRequestParameters);

            List<CalendarHit> hits = new List<CalendarHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticCalendarSearchResults props = GetResponse<ElasticCalendarSearchResults>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticCalendarSearchResults results = GetScrollResults<ElasticCalendarSearchResults>(scrollID);

                    if (results == null)
                        break;
                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }

        /// <summary>
        /// Returns the rates and availability, by inventoryID, between the dates provided (inclusive)
        /// </summary>
        /// <param name="inventoryID">The inventory Id of the unit/property</param>
        /// <param name="fromDate">The starting date (inclusive)</param>
        /// <param name="toDate">The ending date (inclusive)</param>
        /// <returns></returns>
        public List<CalendarHit> GetRatesAndAvailability(List<long> inventoryIDs, DateTime fromDate, DateTime toDate)
        {
            string from = $"{fromDate.Year}-{fromDate.Month.ToString("D2")}-{fromDate.Day.ToString("D2")}T00:00:00";
            string to = $"{toDate.Year}-{toDate.Month.ToString("D2")}-{toDate.Day.ToString("D2")}T00:00:00";

            string ids = "";

            for(int i=0; i<inventoryIDs.Count;i++)
            {
                ids += inventoryIDs[i].ToString();

                if (i != inventoryIDs.Count - 1)
                    ids += ",";
            }

            string query = "{" +
                              "\"query\": {"+
                                "\"bool\": {"+
                                    "\"must_not\": ["+
                                        "{"+
                                        "\"match\": {"+
                                            "\"ConfirmationMethod\": \"Unavailable\""+
                                        "}"+
                                        "}"+
                                  "],"+
                                  "\"must\": ["+
                                    "{"+
                                      "\"range\": {"+
                                        "\"Date\": {"+
                                          "\"gte\": \""+ from +"\","+
                                          "\"lte\": \""+ to +"\""+
                                        "}"+
                                      "}" +
                                    "}" +
                                  "]," +
                                  "\"filter\": {"+
                                    "\"terms\": {"+
                                      "\"InventoryID\": ["+ ids +"]"+
                                    "}"+
                                  "}" +
                                "}" +
                              "}" +
                            "}";

            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            var response = lowLevelClient.Search<StringResponse>("calendars", "calendar", query, searchRequestParameters);

            List<CalendarHit> hits = new List<CalendarHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticCalendarSearchResults props = GetResponse<ElasticCalendarSearchResults>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticCalendarSearchResults results = GetScrollResults<ElasticCalendarSearchResults>(scrollID);

                    if (results == null)
                        break;
                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }

        /// <summary>
        /// Deletes a calendar entry in Elasticsearch, by the Elasticsearch ID
        /// </summary>
        /// <param name="id">The Elasticsearch ID</param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteCalendar(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("calendars", "calendar", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }
    }
}
