﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Models
{
    public class Hits
    {
        public string total { get; set; }
        public string max_score { get; set; }
    }
}
