﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Insert
{
    public class NightlyAverage: GenericChild
    {
        public decimal YearAverageRate { get; set; }
    }
}
