﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Insert
{
    public class ElasticPluginKeyDomain
    {
        public string PluginKey { get; set; }
        public string PluginDomain { get; set; }
        public string PluginActivationKey { get; set; }
    }
}
