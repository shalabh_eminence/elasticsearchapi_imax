﻿
using ElasticSearchCore.Models.Insert;

namespace ElasticSearch.Models.Responses.PluginKeyDomain
{
    public class PluginKeyDomainHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public ElasticPluginKeyDomain _source { get; set; }
    }
}
