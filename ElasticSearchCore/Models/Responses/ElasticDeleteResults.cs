﻿using ElasticSearch.Models;

namespace ElasticSearchCore.Models.Responses
{
    public class ElasticDeleteResults
    {
        public string found { get; set; }
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _version { get; set; }
        public string result { get; set; }
        public Shard _shards { get; set; }
    }
}
