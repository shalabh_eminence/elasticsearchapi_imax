﻿using ElasticSearchCore.Models.Responses.Bulk;

namespace ElasticSearchCore.Models.Responses
{
    public class ElasticBulkInsertResult
    {
        public string took { get; set; }
        public string errors { get; set; }
        public Items[] items { get; set; }
    }
}
