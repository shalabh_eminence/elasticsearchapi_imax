﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.BusinessUnits
{
    public class BusinessUnitHits
    {
        public BusinessUnitHit[] hits { get; set; }
    }
}
