﻿using ElasticSearch.Models.Responses.LandingPages;

namespace ElasticSearch.Models.Responses
{
    public class ElasticLandingPageSearchResult
    {
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public LandingPageHits hits { get; set; }
    }
}
