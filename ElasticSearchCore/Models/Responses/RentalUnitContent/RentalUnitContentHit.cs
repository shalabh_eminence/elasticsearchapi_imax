﻿
namespace ElasticSearch.Models.Responses.RentalUnitContent
{
    public class RentalUnitContentHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public RentalUnitContentSource _source { get; set; }
    }
}
