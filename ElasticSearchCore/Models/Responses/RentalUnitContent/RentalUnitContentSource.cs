﻿
using ElasticSearchCore.Core.Domain;

namespace ElasticSearch.Models.Responses.RentalUnitContent
{
    public class RentalUnitContentSource: RentalUnitContentView
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
