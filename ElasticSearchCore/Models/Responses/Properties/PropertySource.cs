﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Models.Responses.Properties
{
    public class PropertySource
    {
        public PropertySource()
        {
            BusinessUnitNames = new List<string>();
        }

        public long InventoryID { get; set; }
        public string InventoryType { get; set; }
        public string PropertyView { get; set; }
        public long SourceInventoryID { get; set; }
        public string InventoryName { get; set; }
        public int NumImages { get; set; }
        public int Bedroom { get; set; }
        public int Sleeps { get; set; }
        public string InventoryDescription { get; set; }
        public string ComplexName { get; set; }
        public string BuildingName { get; set; }
        public string Policies { get; set; }
        public int DisplaySubOrder { get; set; }
        public string VirtualTourURL { get; set; }
        public string VirtualTourFilmDate { get; set; }
        public bool HasVirtualTour { get; set; }
        public string ExpertReview { get; set; }
        public int RandomDisplay { get; set; }
        public bool EmailBooking { get; set; }
        public string GeoPosition { get; set; }
        public string PropertyProximity { get; set; }
        public string Status { get; set; }
        public string CurrencyType { get; set; }
        public bool IsLuxury { get; set; }
        public string PropertyDisclaimer { get; set; }
        public bool IsFeaturedProperty { get; set; }
        public bool IsActiveOnIndustrySite { get; set; }
        public bool IsIndustrySiteOnlineBookingEnabled { get; set; }
        public bool IndustrySiteSupplierPhoneNumber { get; set; }
        public string GatewayProviderTypeCode { get; set; }
        public string SupplierName { get; set; }
        public int PropertyRating { get; set; }
        public string BuildingType { get; set; }
        public List<string> BusinessUnitNames { get; set; }
    }
}
