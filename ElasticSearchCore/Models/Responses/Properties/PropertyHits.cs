﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Models.Responses.Properties
{
    public class PropertyHits
    {
        public int total { get; set; }
        public float? max_score { get; set; }
        public PropertyHit[] hits { get; set; }
    }
}
