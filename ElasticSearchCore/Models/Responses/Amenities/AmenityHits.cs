﻿
namespace ElasticSearch.Models.Responses.Amenities
{
    public class AmenityHits
    {
        public AmenityHit[] hits { get; set; }
    }
}
