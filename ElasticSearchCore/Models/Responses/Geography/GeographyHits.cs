﻿
namespace ElasticSearch.Models.Responses.Geography
{
    public class GeographyHits
    {
        public GeographyHit[] hits { get; set; }
    }
}
