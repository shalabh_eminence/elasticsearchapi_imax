﻿
namespace ElasticSearch.Models.Responses.Geography
{
    public class GeographySource
    {
        public long InventoryID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string IslandName { get; set; }
        public string Destination { get; set; }
        public string DestinationCategoryName { get; set; }
    }
}
