﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Images
{
    public class ImageHits
    {
        public ImageHit[] hits { get; set; }
    }
}
