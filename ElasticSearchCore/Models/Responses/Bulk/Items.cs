﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Bulk
{
    public class Items
    {
        public Index index { get; set; }
    }
}
