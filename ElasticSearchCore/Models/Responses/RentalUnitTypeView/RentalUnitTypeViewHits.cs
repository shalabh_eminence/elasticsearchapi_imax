﻿
namespace ElasticSearch.Models.Responses.RentalUnitTypeView
{
    public class RentalUnitTypeViewHits
    {
        public RentalUnitTypeViewHit[] hits { get; set; }
    }
}
