﻿
using ElasticSearchCore.Core.Domain;

namespace ElasticSearch.Models.Responses.RentalUnitTypeView
{
    public class RentalUnitTypeViewSource : StandAloneRentalUnitRoomTypeView
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
