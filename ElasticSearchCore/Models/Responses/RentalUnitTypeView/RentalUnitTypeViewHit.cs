﻿
namespace ElasticSearch.Models.Responses.RentalUnitTypeView
{
    public class RentalUnitTypeViewHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public RentalUnitTypeViewSource _source { get; set; }
    }
}
