﻿
namespace ElasticSearch.Models.Responses.Neighborhoods
{
    public class NeighborhoodHits
    {
        public NeighborhoodHit[] hits { get; set; }
    }
}
