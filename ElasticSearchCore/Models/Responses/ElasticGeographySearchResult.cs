﻿
using ElasticSearch.Models.Responses.Geography;

namespace ElasticSearch.Models.Responses
{
    public class ElasticGeographySearchResult
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public GeographyHits hits { get; set; }
    }
}
