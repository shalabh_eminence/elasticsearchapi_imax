﻿using ElasticSearch.Models;
using ElasticSearch.Models.Responses.RentalUnitTypeView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses
{
    public class RentalUnitTypeViewSearchResults
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public RentalUnitTypeViewHits hits { get; set; }
    }
}
