﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// Contains the info related to a specific property.
    /// </summary>
    public class PropertyInfo
    {
        /// <summary>
        /// Constructor for PropertyInfo
        /// </summary>
        public PropertyInfo()
        {
            Images = new List<string>();
        }

        /// <summary>
        /// The RealVoice inventory ID number
        /// </summary>
        public long InventoryID { get; set; }
        /// <summary>
        /// The RealVoice rentalUnitID associated with this property/unit
        /// </summary>
        public long RentalUnitID { get; set; }
        /// <summary>
        /// The Destination this property is associated with
        /// </summary>
        public string Destination { get; set; }
        /// <summary>
        /// The inventory type: Standalone or Complex
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// The Rental Unit View Type
        /// </summary>
        public string ViewType { get; set; }
        /// <summary>
        /// The unit number for this property, if any
        /// </summary>
        public string UnitNumber { get; set; }
        /// <summary>
        /// This unit/property is considered luxury: true/false
        /// </summary>
        public bool IsLuxury { get; set; }
        /// <summary>
        /// This unit/property is marked as featured by Realvoice: true/false
        /// </summary>
        public bool IsFeatured { get; set; }
        /// <summary>
        /// This unit/property is bookable online
        /// </summary>
        public bool IsBookable { get; set; }
        /// <summary>
        /// Maximum number of people that can occupy this unit/property
        /// </summary>
        public int Occupancy { get; set; }
        /// <summary>
        /// The disclaimer for this unit/property
        /// </summary>
        public string Disclaimer { get; set; }
        /// <summary>
        /// Policies related to this unit/property
        /// </summary>
        public string Policies { get; set; }
        /// <summary>
        /// Check-in time for this unit/property
        /// </summary>
        public string CheckInTime { get; set; }
        /// <summary>
        /// Check-out time for this unit/property
        /// </summary>
        public string CheckOutTime { get; set; }
        /// <summary>
        /// The geographical latitude of this unit/property
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// The geographical longitude of this unit/property
        /// </summary>
        public string Longitude { get; set; }
        /// <summary>
        /// The name where this unit/property is located
        /// </summary>
        public string ComplexName { get; set; }
        /// <summary>
        /// The description where this unit/property is located
        /// </summary>
        public string ComplexDescription { get; set; }
        /// <summary>
        /// The description about the unit
        /// </summary>
        public string UnitDescription { get; set; }
        /// <summary>
        /// How many can sleep at this unit/property
        /// </summary>
        public string Sleeps { get; set; }
        /// <summary>
        /// How many bedrooms this unit/property have
        /// </summary>
        public string BedRooms { get; set; }
        /// <summary>
        /// The number of bathrooms this unit/property has
        /// </summary>
        public string Baths { get; set; }
        /// <summary>
        /// The most recent pricing average for this unit/property
        /// </summary>
        public string Pricing { get; set; }
        /// <summary>
        /// The images associated with this unit/property
        /// </summary>
        public List<string> Images { get; set; }
    }
}