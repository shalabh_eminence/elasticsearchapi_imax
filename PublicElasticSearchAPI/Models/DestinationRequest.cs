﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// Used to get Destination information
    /// </summary>
    public class DestinationRequest
    {
        public string Destination { get; set; }
        /// <summary>
        /// Authentication for the Realvoice API
        /// </summary>
        [Required]
        public Authentication Authentication { get; set; }
    }
}