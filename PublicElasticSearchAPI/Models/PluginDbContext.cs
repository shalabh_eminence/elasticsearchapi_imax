namespace PublicElasticSearchAPI.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PluginDbContext : DbContext
    {
        public PluginDbContext()
            : base("name=PluginDbContext")
        {
        }

        public virtual DbSet<PluginKeyDomain> PluginKeyDomains { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PluginKeyDomain>()
                .Property(e => e.PluginKEY)
                .IsUnicode(false);

            modelBuilder.Entity<PluginKeyDomain>()
                .Property(e => e.PluginDomain)
                .IsUnicode(false);
        }
    }
}
