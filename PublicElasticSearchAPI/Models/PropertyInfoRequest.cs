﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// A request for property information
    /// </summary>
    public class PropertyInfoRequest
    {
        /// <summary>
        /// The RealVoice inventory ID number for the unit/property
        /// </summary>
        public long InventoryID { get; set; }
        /// <summary>
        /// Authentication for the Realvoice API
        /// </summary>
        [Required]
        public Authentication Authentication { get; set; }
    }
}