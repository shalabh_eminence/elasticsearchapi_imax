﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// Used in requesting rates and availabilty 
    /// </summary>
    public class CalendarRequest
    {
        /// <summary>
        /// The RealVoice inventory ID numbers you want to get amenities for
        /// </summary>
        public long InventoryID { get; set; }
        /// <summary>
        /// Authentication for the Realvoice API
        /// </summary>
        [Required]
        public Authentication Authentication { get; set; }
    }
}