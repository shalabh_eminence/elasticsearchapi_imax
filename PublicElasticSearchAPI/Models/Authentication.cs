﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// Authentication for the Realvoice API
    /// </summary>
    public class Authentication
    {
        /// <summary>
        /// Your RealVoice APIKey
        /// </summary>
        [Required]
        public string APIKey { get; set; }
    }
}