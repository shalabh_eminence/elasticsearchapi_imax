namespace PublicElasticSearchAPI.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Net.Http;

    [Table("PluginKeyDomain")]
    public partial class PluginKeyDomain
    {
        [Key]
        public int PluginID { get; set; }

        [Required]
        [StringLength(1250)]
        public string PluginKEY { get; set; }

        [StringLength(500)]
        public string PluginDomain { get; set; }
    }

    public class AuthResponse
    {
        public AuthResponse()
        {
            this.Status = "";
            this.Message = "";
            this.Token = "";
        }

        public string Status { get; set; }
        public string Token { get; set; }
        public string Message { get; set; }

    }
}
