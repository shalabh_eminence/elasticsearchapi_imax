﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// The City Object
    /// </summary>
    public class City
    {
        /// <summary>
        /// The city's name
        /// </summary>
        public string CityName { get; set; }
        /// <summary>
        /// The count of properties within this city
        /// </summary>
        public int Count { get; set; }
    }
    /// <summary>
    /// The State Object
    /// </summary>
    public class State
    {
        /// <summary>
        /// Constructor for the State Object
        /// </summary>
        public State()
        {
            Cities = new List<City>();
            Count = 0;
        }
        /// <summary>
        /// The state's name
        /// </summary>
        public string StateName { get; set; }
        /// <summary>
        /// Number of properties within this state
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// A list of city names that are in the state
        /// </summary>
        public List<City> Cities { get; set; }
    }
    /// <summary>
    /// The Country Object
    /// </summary>
    public class Country
    {
        /// <summary>
        /// Constructor for the Country Object
        /// </summary>
        public Country()
        {
            States = new List<State>();
            Count = 0;
        }
        /// <summary>
        /// The country name
        /// </summary>
        public string CountryName { get; set; }
        /// <summary>
        /// The number of properties in this country
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// The list of states that are in the country
        /// </summary>
        public List<State> States { get; set; }
    }
    /// <summary>
    /// A country-state-city structure, to be used to query the API for properties
    /// </summary>
    public class GeographyStructure
    {
        /// <summary>
        /// Constructor for the GeographyStructure Object
        /// </summary>
        public GeographyStructure()
        {
            Countries = new List<Country>();
        }
        /// <summary>
        /// A list of countries
        /// </summary>
        public List<Country> Countries { get; set; }
    }
}