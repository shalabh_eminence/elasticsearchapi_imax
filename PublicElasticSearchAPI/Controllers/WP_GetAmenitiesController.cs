﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Amenities;
using ElasticSearchCore.API.ElasticQueries;
using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    public class WP_GetAmenitiesController : ApiController
    {
        /// <summary>
        /// Gets a full list of amenities for a given inventory ID.
        /// </summary>
        /// <param name="amenityRequest">The request for amenities</param>
        [Authorize]
        public IEnumerable<string> Post([FromBody]AmenityRequest amenityRequest)
        {
            AmenityQuery amenityQuery = new AmenityQuery();
            ElasticAmenitySearchResult result = amenityQuery.GetAmenitiesByInventoryID((long)amenityRequest.InventoryID);
            List<string> amenities = new List<string>();

            Helpers helper = new Helpers();
            string businessUnitName = helper.Decrypt(amenityRequest.Authentication.APIKey, "Real Voice");

            PropertyQuery propertyQuery = new PropertyQuery();
            ElasticPropertySearchResults propertiesResults = propertyQuery.GetPropertyByInventoryID(amenityRequest.InventoryID);

            List<AmenityHit> amenHits = (from x in result.hits.hits
                                         join y in propertiesResults.hits.hits
                                         on x._source.InventoryID equals y._source.InventoryID
                                         where y._source.BusinessUnitNames.Contains(businessUnitName)
                                         select x).ToList();

            foreach (AmenityHit hit in amenHits)
            {
                amenities.Add(hit._source.AttribName);
            }

            return amenities;
        }
    }
}