﻿using ElasticSearch.Models.Responses.Geography;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses.BusinessUnits;
using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Returns a list of valid Country, State, City name combinations for a given inventory ID list, that can be used for refining searches. If the inventory ID list is empty, it returns all geographies that can be used for refining searches.
    /// </summary>
    public class GetGeographyListController : ApiController
    {
        /// <summary>
        /// Gets a list of all valid Country, State, City name combinations.
        /// </summary>
        /// <param name="auth">Realvoice Authentication Obj</param>
        /// <returns></returns>
        public GeographyStructure Post([FromBody]GeographyRequest geoRequest)
        {
            Helpers helper = new Helpers();
            string businessUnitName = helper.Decrypt(geoRequest.Authentication.APIKey, "Real Voice");

            BusinessUnitQuery businessUnitQuery = new BusinessUnitQuery();
            List<BusinessUnitHit> buHits = businessUnitQuery.GetAllBusinessUnitsByBusinessUnitName(businessUnitName);

            List<long> inventoryIDs = (from x in buHits
                                       select x._source.InventoryId).ToList();

            GeographyQuery geographyQuery = new GeographyQuery();
            List<GeographyHit> geoHits = geographyQuery.GetGeographyByInventoryIds(inventoryIDs).hits.hits.ToList();// geographyQuery.GetAllGeographies();

            GeographyStructure geoStructures = new GeographyStructure();
            List<Country> countries = new List<Country>();

            foreach (GeographyHit hit in geoHits)
            {
                string country = hit._source.Country;
                string state = hit._source.StateName;
                string city = hit._source.CityName;

                Country c = (from x in countries
                             where x.CountryName == country
                             select x).FirstOrDefault();

                // If this country doesn't exist in the list yet
                if (c == default(Country))
                {
                    Country countryObj = new Country();
                    State stateObj = new State();
                    City cityObj = new City();

                    countryObj.CountryName = country;
                    stateObj.StateName = state;
                    cityObj.CityName = city;

                    stateObj.Cities.Add(cityObj);
                    countryObj.States.Add(stateObj);

                    countries.Add(countryObj);
                }
                else // If a country already exists
                {
                    State stateObj = (from x in c.States
                                      where x.StateName == state
                                      select x).FirstOrDefault();

                    // If this state doesn't exist
                    if (stateObj == default(State))
                    {
                        stateObj = new State();
                        City cityObj = new City();

                        stateObj.StateName = state;
                        cityObj.CityName = city;

                        stateObj.Cities.Add(cityObj);
                    }
                    else //If the state does exist
                    {
                        City cityObj = (from x in stateObj.Cities
                                        where x.CityName == city
                                        select x).FirstOrDefault();

                        // If there is no city by that Name
                        if (cityObj == default(City))
                        {
                            cityObj = new City();
                            cityObj.Count++;
                            cityObj.CityName = city;
                            stateObj.Cities.Add(cityObj);
                        }
                        else
                        {
                            cityObj.Count++;
                        }
                    }
                }
            }

            foreach(Country c in countries)
            {
                List<State> states = c.States;

                foreach (State s in states)
                {
                    List<City> cits = s.Cities;

                    foreach (City cit in cits)
                    {
                        s.Count += cit.Count;
                    }

                    c.Count += s.Count;
                }
            }

            geoStructures.Countries.AddRange(countries);

            return geoStructures;
        }
    }
}
