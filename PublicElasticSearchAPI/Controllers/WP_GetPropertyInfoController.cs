﻿using ElasticSearch.Models.Responses;
using ElasticSearchCore.API.ElasticQueries;
using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    public class WP_GetPropertyInfoController : ApiController
    {
        // POST: api/GetPropertyInfo
        /// <summary>
        /// Returns information about a given unit/property
        /// </summary>
        /// <param name="propertyInfoRequest">The property info request</param>
        [Authorize]
        public PropertyInfo Post([FromBody]PropertyInfoRequest propertyInfoRequest)
        {
            Helpers helpers = new Helpers();
            List<PropertyInfo> properties = helpers.FillPropertyInfos(new List<long>() { propertyInfoRequest.InventoryID });

            Helpers helper = new Helpers();
            string businessUnitName = helper.Decrypt(propertyInfoRequest.Authentication.APIKey, "Real Voice");

            PropertyQuery propertyQuery = new PropertyQuery();
            ElasticPropertySearchResults propertiesResults = propertyQuery.GetPropertyByInventoryID(propertyInfoRequest.InventoryID);

            properties = (from x in properties
                          join y in propertiesResults.hits.hits
                          on x.InventoryID equals y._source.InventoryID
                          where y._source.BusinessUnitNames.Contains(businessUnitName)
                          select x).ToList();

            if (properties.Count == 1)
            {
                properties[0].Images = helper.GetImages(properties[0].InventoryID);
                return properties[0];
            }

            return new PropertyInfo();
        }
    }
}