﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses.Calendars;
using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Gets inventory search results, based upon the parameters passed in.
    /// </summary>
    public class WP_GetSearchResultsController : ApiController
    {
        /// <summary>
        /// Returns inventory search results, based upon the request provided
        /// </summary>
        /// <param name="searchRequest">The search request object</param>
        /// <returns></returns>
        [Authorize]
        public SearchResults Post([FromBody]SearchRequest searchRequest)
        {
            Helpers helper = new Helpers();
            //string enc = helper.Encrypt("VacationRoost", "Real Voice");
            string businessUnitName = helper.Decrypt(searchRequest.Authentication.APIKey, "Real Voice");

            // Paging parameters, passed in from user
            int pageSize = searchRequest.PageSize;
            int pageNumber = searchRequest.PageNumber;

            // Get the base query results
            Tuple<List<PropertyHit>, List<PropertyHit>> baseQueryProperties = GetBaseQueryResults(searchRequest, businessUnitName);

            //set up the sorted properties
            List<PropertyHit> sortedProperties = baseQueryProperties.Item1;
            List<PropertyHit> sortedFeatures = baseQueryProperties.Item2;

            if (searchRequest.IsFeaturedLuxury == false)
            {
                sortedFeatures = (from x in sortedFeatures
                                  where x._source.IsLuxury == false
                                  select x).ToList();
            }
            else if (searchRequest.IsFeaturedLuxury == true)
            {
                sortedFeatures = (from x in sortedFeatures
                                  where x._source.IsLuxury == true
                                  select x).ToList();
            }

            // Filter properties by amenities, if any
            if (searchRequest.Amenities != null)
            {
                if (searchRequest.Amenities.Count() > 0)
                {
                    sortedProperties = helper.FilterByAmenities(searchRequest.Amenities, sortedProperties);
                }
            }

            //filter by sleeps
            if (searchRequest.Sleeps != null)
            {
                if (searchRequest.Sleeps.Count > 0)
                {
                    sortedProperties = helper.FilterBySleeps(searchRequest.Sleeps, sortedProperties, searchRequest.IsLuxury);
                }
            }

            if (searchRequest.LodgingType != null)
            {
                if (searchRequest.LodgingType.Count > 0)
                {
                    sortedProperties = helper.FilterByLodingType(sortedProperties, searchRequest.LodgingType);
                }
            }

            if (searchRequest.Bedrooms != null)
            {
                if (searchRequest.Bedrooms.Count > 0)
                {
                    sortedProperties = helper.FilterByBedrooms(searchRequest.Bedrooms, sortedProperties);
                }
            }

            if (searchRequest.ViewType != null)
            {
                if (searchRequest.ViewType.Count > 0)
                {
                    sortedProperties = helper.FilterByViewType(searchRequest.ViewType, sortedProperties);
                }
            }

            //Add a numerical rate to the base query results, for sorting
            helper.FillInRatesAndMerchOrder(sortedProperties);

            //Sort properties
            //0 = default, 1 = Pricing Low to High, 2 = Pricing High to Low, 3 = Sleeps Low to High, and 4 = Sleeps High to Low
            //5 = Bedrooms High to Low, and 6 = Bedrooms Low to High
            switch (searchRequest.Sorting)
            {
                case 0: //0 = default
                    sortedProperties = sortedProperties.OrderBy(x => x.merchandiceOrder).ToList();
                    break;
                case 1: //1 = Pricing Low to High
                    sortedProperties = sortedProperties.OrderBy(x => x.rate).ToList();
                    break;
                case 2: //2 = Pricing High to Low
                    sortedProperties = sortedProperties.OrderByDescending(x => x.rate).ToList();
                    break;
                case 3: //3 = Sleeps Low to High
                    sortedProperties = sortedProperties.OrderBy(x => x._source.Sleeps).ToList();
                    break;
                case 4: //4 = Sleeps High to Low
                    sortedProperties = sortedProperties.OrderByDescending(x => x._source.Sleeps).ToList();
                    break;
                case 5: //5 = Bedrooms High to Low
                    sortedProperties = sortedProperties.OrderByDescending(x => x._source.Bedroom).ToList();
                    break;
                case 6: //6 = Bedrooms Low to High
                    sortedProperties = sortedProperties.OrderBy(x => x._source.Bedroom).ToList();
                    break;
                default: //0 = default
                    sortedProperties = sortedProperties.OrderBy(x => x.merchandiceOrder).ToList();
                    break;
            }

            // Get current refinements (Amenities, Destinations, Sleeps, etc.), using the sorted properties
            List<Dictionary<string, int>> searchRefinements = helper.GetSearchRefinements(sortedProperties);

            // Get the correct page for the results
            List<PropertyHit> subset = new List<PropertyHit>();
            int recordOffset = pageSize * pageNumber;

            if (sortedProperties.Count > (recordOffset + pageSize))
            {
                subset = sortedProperties.GetRange(recordOffset != 0 ? recordOffset - 1 : 0, pageSize);
            }
            else if (recordOffset == 0 && sortedProperties.Count < pageSize)
            {
                subset = sortedProperties;
            }
            else if ((recordOffset + pageSize) - sortedProperties.Count < pageSize)
            {
                subset = sortedProperties.GetRange(recordOffset - 1 >= 0 ? recordOffset - 1 : 0, sortedProperties.Count - recordOffset);
            }

            SearchResults searchResults = new SearchResults()
            {
                Inventories = helper.FillPropertyInfos((from x in subset select x._source.InventoryID).ToList()),
                FeaturedInventories = helper.FillPropertyInfos((from x in sortedFeatures select x._source.InventoryID).ToList()),
                Amenities = searchRefinements[0],
                Destinations = searchRefinements[1],
                Cities = searchRefinements[2],
                States = searchRefinements[3],
                Countries = searchRefinements[4],
                Sleeps = searchRefinements[5],
                TotalResults = sortedProperties.Count
            };

            return searchResults;
        }

        private Tuple<List<PropertyHit>, List<PropertyHit>> GetBaseQueryResults(SearchRequest searchRequest, string businessName)
        {
            PropertyQuery propertyQuery = new PropertyQuery();
            List<PropertyHit> propertySources = new List<PropertyHit>();
            List<PropertyHit> featuredPropertySources = new List<PropertyHit>();
            string status = "active";

            Helpers helper = new Helpers();

            //string encrypted = helper.Encrypt(searchRequest.Authentication.APIKey, "Real Voice");

            string businessUnitName = helper.Decrypt(searchRequest.Authentication.APIKey, "Real Voice");

            if (searchRequest.Country.Count() != 0)
            {
                foreach (string c in searchRequest.Country)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesByCountry(c);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            #region destinations
            if (searchRequest.Destination.Count > 0)
            {
                foreach (string d in searchRequest.Destination)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyDestination(d);

                    if (propertyResults != null)
                    {
                        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                        {
                            propertySources.AddRange(propertyResults.hits.hits);
                        }
                    }
                }
            }
            #endregion

            #region destinationType
            //if (destinationTypeCategories.Count > 0)
            //{
            //    foreach (string d in destinationTypeCategories)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyDestinationCategoryName(d);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}
            #endregion

            #region resorts
            //if (resorts.Count > 0)
            //{
            //    foreach (string r in resorts)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyResort(r);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}
            #endregion

            if (searchRequest.City.Count > 0)
            {
                foreach (string c in searchRequest.City)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyCity(c);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (searchRequest.State.Count > 0)
            {
                foreach (string s in searchRequest.State)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyState(s);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            #region neighborhoods
            //if (neighborhoods.Count > 0)
            //{
            //    foreach (string n in neighborhoods)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyNeighborhood(n);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}
            #endregion

            /*if (searchRequest.Sleeps.Count > 0)
            {
                foreach (int b in searchRequest.Sleeps)
                {
                    ElasticPropertySearchResults propertyResults = null;

                    if (searchRequest.IsLuxury == null)
                    {
                        propertyResults = propertyQuery.GetLuxuryPropertiesbyBed(b.ToString());
                        propertyResults = propertyQuery.GetPropertiesbyBed(b.ToString());
                    }
                    else if ((bool)searchRequest.IsLuxury)
                        propertyResults = propertyQuery.GetLuxuryPropertiesbyBed(b.ToString());
                    else
                        propertyResults = propertyQuery.GetPropertiesbyBed(b.ToString());

                    propertySources.AddRange(propertyResults.hits.hits);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }*/

            if (searchRequest.IsLuxury != null)
            {
                if ((bool)searchRequest.IsLuxury)
                {
                    propertySources = (from x in propertySources
                                       where x._source.IsLuxury == true
                                       select x).ToList();
                }
                if (!(bool)searchRequest.IsLuxury)
                {
                    propertySources = (from x in propertySources
                                       where x._source.IsLuxury == false
                                       select x).ToList();
                }
            }

            if (status != string.Empty)
            {
                propertySources = (from x in propertySources
                                   where x._source.Status.ToLower().Equals(status.ToLower())
                                   select x).ToList();
            }

            featuredPropertySources = (from x in propertySources
                                       where x._source.IsFeaturedProperty == true
                                       select x).ToList();

            //BusinessUnitQuery businessUnitQuery = new BusinessUnitQuery();
            //List<BusinessUnitHit> buh = businessUnitQuery.GetAllBusinessUnitsByBusinessUnitName(businessName);

            List<Task> tasks = new List<Task>();

            tasks.Add(Task.Run(() => {
                propertySources = (from x in propertySources
                                   where x._source.BusinessUnitNames.Contains(businessUnitName)
                                   select x).ToList();
            }));

            tasks.Add(Task.Run(() => {
                featuredPropertySources = (from x in featuredPropertySources
                                           where x._source.BusinessUnitNames.Contains(businessUnitName)
                                           select x).ToList();
            }));
            Task.WaitAll(tasks.ToArray());

            if (searchRequest.FromDate != null && searchRequest.ToDate != null)
            {
                ConcurrentBag<PropertyHit> availPropsByDate = new ConcurrentBag<PropertyHit>();
                List<Task> tasks2 = new List<Task>();

                CalendarQuery calendarQuery = new CalendarQuery();
                List<CalendarHit> calHits = calendarQuery.GetRatesAndAvailability((from x in propertySources select x._source.InventoryID).ToList(), (DateTime)searchRequest.FromDate, (DateTime)searchRequest.ToDate);

                foreach (PropertyHit ph in propertySources)
                {
                    tasks2.Add(Task.Run(() => {

                        bool avail = true;
                        int days = ((DateTime)searchRequest.ToDate - (DateTime)searchRequest.FromDate).Days;

                        int cnt = (from x in calHits
                                   where x._source.InventoryID == ph._source.InventoryID
                                   select x).Count();

                        if (cnt != days + 1)
                        {
                            avail = false;
                        }

                        if (avail)
                        {
                            availPropsByDate.Add(ph);
                        }
                    }));
                }

                Task.WaitAll(tasks2.ToArray());

                return new Tuple<List<PropertyHit>, List<PropertyHit>>(availPropsByDate.ToList(), featuredPropertySources);
            }

            return new Tuple<List<PropertyHit>, List<PropertyHit>>(propertySources, featuredPropertySources);
        }
    }
}