﻿using ElasticSearch.Models.Responses;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses.BusinessUnits;
using ElasticSearchCore.Models.Responses.Calendars;
using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Gets the rates and availability for a given unit/property.
    /// </summary>
    public class GetRatesAndAvailabilityController : ApiController
    {
        /// <summary>
        /// Retrieves rates and availability for an individual unit/property
        /// </summary>
        /// <param name="calendarRequest">The calendar request object</param>
        /// <returns></returns>
        public List<CalendarSource> Post([FromBody]CalendarRequest calendarRequest)
        {
            CalendarQuery calendarQuery = new CalendarQuery();
            List<CalendarHit> calHits = calendarQuery.GetRatesAndAvailability(calendarRequest.InventoryID);

            PropertyQuery propertyQuery = new PropertyQuery();
            ElasticPropertySearchResults properties = propertyQuery.GetPropertyByInventoryID(calendarRequest.InventoryID);

            Helpers helper = new Helpers();
            string businessUnitName = helper.Decrypt(calendarRequest.Authentication.APIKey, "Real Voice");

            return (from x in calHits
                    join y in properties.hits.hits
                    on x._source.InventoryID equals y._source.InventoryID
                    where y._source.BusinessUnitNames.Contains(businessUnitName)
                    select x._source).ToList();
        }
    }
}
