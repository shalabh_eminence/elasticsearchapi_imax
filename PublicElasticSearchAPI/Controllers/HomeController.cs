﻿using PublicElasticSearchAPI.Controllers.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PublicElasticSearchAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            //Helpers helpers = new Helpers();
            //ViewBag.Title = helpers.Decrypt("ptW3pWOdZTbXXBUdDAoa2AXaWdI9+0rQ9lKqmILkAvg=", "Real Voice");// helpers.Encrypt("Silver Summit Management", "Real Voice");

            return View();
        }

        
    }
}
