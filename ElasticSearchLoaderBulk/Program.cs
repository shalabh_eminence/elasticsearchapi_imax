﻿using ElasticSearch.Models.Responses;
using ElasticSearchCore.API.Connector;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Core.DataAccess;
using ElasticSearchCore.Core.DataAccess.Impl;
using ElasticSearchCore.Core.Domain;
using ElasticSearchCore.Models.Insert;
using ElasticSearchCore.Models.Responses;
using ElasticSearchCore.Models.Responses.Bulk;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ElasticSearchLoaderBulk
{
    class Log
    {
        public void WriteLog(string message)
        {
            using (var writer = new StreamWriter(@"log.txt", true))
            {
                writer.WriteLine(message);
            }
        }
    }

    class Program
    {
        private static string host = ConfigurationSettings.AppSettings.Get("ElasticSearchHost");
        private static int port = int.Parse(ConfigurationSettings.AppSettings.Get("ElasticSearchPort"));

        private static EndecaAmenitiesViewRepository _amenitiesRepo;
        private static EndecaPropertyViewRepository _propertiesRepo;
        private static EndecaWeeklyRatesViewRepository _weeklyRatesRepo;
        private static NightlyRateRepository _nightlyRateRepository;
        private static EndecaPromotionViewRepository _promotionRepo;
        private static MerchandisingLandingPageRepository _landingPageRepo;
        private static EndecaResortViewRepository _resortRepo;
        private static EndecaNeighborhoodViewRepository _neighborhoodRepo;
        private static EndecaGeographyViewRepository _geographyRepo;
        private static RentalUnitContentViewRepository _rentalUnitContentRepo;

        private static List<EndecaPropertyView> properties;

        private static HttpClient client;

        static void Main(string[] args)
        {
            Log l = new Log();
            l.WriteLog($"{DateTime.Now} Starting");

            client = new HttpClient();
            client.BaseAddress = client.BaseAddress = new Uri($"http://{host}:{port}");

            _propertiesRepo = new EndecaPropertyViewRepository();
            _amenitiesRepo = new EndecaAmenitiesViewRepository();
            _weeklyRatesRepo = new EndecaWeeklyRatesViewRepository();
            _nightlyRateRepository = new NightlyRateRepository();
            _promotionRepo = new EndecaPromotionViewRepository();
            _landingPageRepo = new MerchandisingLandingPageRepository();
            _resortRepo = new EndecaResortViewRepository();
            _neighborhoodRepo = new EndecaNeighborhoodViewRepository();
            _geographyRepo = new EndecaGeographyViewRepository();
            _rentalUnitContentRepo = new RentalUnitContentViewRepository();

            try
            {
                EndecaPropertyView pv = _propertiesRepo.GetPropertyByInventoryID(0);
            }
            catch(Exception)
            {
                string exception = "Could not connect to the database";
                l.WriteLog(exception);
                System.Threading.Thread.Sleep(3000);
                throw new System.InvalidOperationException(exception);
            }

            //get startup commands/information
            string structure = File.ReadAllText("ElasticSearchStructure.txt");

            ElasticSearchConnection elasticSearchConnection = new ElasticSearchConnection();
            ElasticDropResult delResult = null;
            ElasticPutResult putResult = null;

            delResult = elasticSearchConnection.Delete<ElasticDropResult>("props");

            if(delResult == null)
            {

            }
            else if (delResult.acknowledged.ToLower().Equals("true"))
            {
                putResult = elasticSearchConnection.Execute<ElasticPutResult>("props", structure, "PUT");

                if (!putResult.acknowledged.ToLower().Equals("true") && !putResult.shards_acknowledged.ToLower().Equals("true"))
                {
                    string exception = "Could not PUT mappings of index /props";
                    l.WriteLog(exception);
                    System.Threading.Thread.Sleep(3000);
                    throw new System.InvalidOperationException(exception);
                }
            }
            else
            {
                string exception = "Could not delete props from ElasticSearch";
                l.WriteLog(exception);
                System.Threading.Thread.Sleep(3000);
                throw new System.InvalidOperationException("Could not delete props from ElasticSearch");
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();

            ProcessProperties();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("1/10 Finished Properties");

            Task t1 = Task.Run(() =>
            {
                ProcessChildEntity<EndecaAmenitiesView>();
                Console.SetCursorPosition(0, 1);
                Console.WriteLine("2/10 Finished Amenities");
            });

            Task t2 = Task.Run(() =>
            {
                ProcessChildEntity<NightlyAverage>();
                Console.SetCursorPosition(0, 2);
                Console.WriteLine("3/10 Finished Nightly Averages");
            });

            Task t3 = Task.Run(() =>
            {
                ProcessChildEntity<EndecaPromotionView>();
                Console.SetCursorPosition(0, 3);
                Console.WriteLine("4/10 Finished Promotions");
            });

            Task t4 = Task.Run(() =>
            {
                ProcessChildEntity<MerchandisingLandingPageView>();
                Console.SetCursorPosition(0, 4);
                Console.WriteLine("5/10 Finished Landing Pages");
            });

            Task t5 = Task.Run(() =>
            {
                ProcessChildEntity<EndecaResortView>();
                Console.SetCursorPosition(0, 5);
                Console.WriteLine("6/10 Finished Resorts");
            });

            Task t6 = Task.Run(() =>
            {
                ProcessChildEntity<EndecaNeighborhoodView>();
                Console.SetCursorPosition(0, 6);
                Console.WriteLine("7/10 Finished Neighborhoods");
            });

            Task t7 = Task.Run(() =>
            {
                ProcessChildEntity<EndecaGeographyView>();
                Console.SetCursorPosition(0, 7);
                Console.WriteLine("8/10 Finished Geographies");
            });

            Task t8 = Task.Run(() =>
            {
                ProcessChildEntity<RentalUnitContentView>();
                Console.SetCursorPosition(0, 8);
                Console.WriteLine("9/10 Finished Rental Unit Content");
            });

            Task t9 = Task.Run(() =>
            {
                ProcessChildEntity<EndecaWeeklyRatesView>();
                Console.SetCursorPosition(0, 9);
                Console.WriteLine("10/10 Finished Weekly Rates");
            });

            Task[] tasks = new Task[9];
            tasks[0] = t1;
            tasks[1] = t2;
            tasks[2] = t3;
            tasks[3] = t4;
            tasks[4] = t5;
            tasks[5] = t6;
            tasks[6] = t7;
            tasks[7] = t8;
            tasks[8] = t9;

            Task.WaitAll(tasks);

            sw.Stop();
            Console.SetCursorPosition(0, 9);
            Console.WriteLine($"Total time: {sw.ElapsedMilliseconds}");
            Console.WriteLine($"Processed properties");

            l.WriteLog($"Total time: {sw.ElapsedMilliseconds}");

            System.Threading.Thread.Sleep(3000);
        }

        static void ProcessProperties()
        {
            properties = _propertiesRepo.GetAllProperties();

            int batchCount = 10000;

            while(properties.Count > 0)
            {
                List<EndecaPropertyView> pvs = (from x in properties
                                                select x).Take(batchCount).ToList();

                string bulkPost = "";
                List<Task> tasks = new List<Task>();
                ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

                foreach (EndecaPropertyView pv in pvs)
                {
                    Task t = Task.Run(() =>
                    {
                        bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"props\", \"_type\": \"property\", \"_routing\":\"props\"} }" + Environment.NewLine + JsonConvert.SerializeObject(pv) + Environment.NewLine);
                    });

                    tasks.Add(t);
                }

                Task.WaitAll(tasks.ToArray());

                foreach(string p in bulkPostParts)
                {
                    bulkPost += p;
                }

                string path = "props/property/_bulk?refresh=wait_for";
                ElasticSearchConnection elasticSearchConnection = new ElasticSearchConnection();
                ElasticBulkInsertResult result = elasticSearchConnection.Execute<ElasticBulkInsertResult>(path, bulkPost);

                if (result.items.Count() != batchCount)
                {
                    if (result.items.Count() != properties.Count())
                    {
                        Log l = new Log();
                        l.WriteLog($"{DateTime.Now} Bulk Property insert result count didn't match input count");
                    }
                }

                foreach (Items it in result.items)
                {
                    if (it.index.created != "true")
                    {
                        Log l = new Log();
                        l.WriteLog($"{DateTime.Now} Property {it.index._id} failed to be inserted");
                    }
                }

                try
                {
                    properties.RemoveRange(0, batchCount);
                }
                catch(ArgumentOutOfRangeException)
                {
                    properties.RemoveRange(0, properties.Count);
                }
                catch(ArgumentException)
                {
                    properties.RemoveRange(0, properties.Count);
                }
            }
        }

        static void ProcessChildEntity<T>()
        {
            List<GenericChild> childList = null;
            string stringType = "";

            if (typeof(T) == typeof(EndecaAmenitiesView))
            {
                childList = _amenitiesRepo.GetAllAmenitiesWithProperties().Cast<GenericChild>().ToList();
                stringType = "amenities";
            }
            if(typeof(T) == typeof(NightlyAverage))
            {
                childList = _nightlyRateRepository.GetYearAverageNightlyRate().Cast<GenericChild>().ToList(); ;
                stringType = "nightlyAverage";
            }
            if (typeof(T) == typeof(EndecaPromotionView))
            {
                childList = _promotionRepo.GetAllPromotionsWithProperties().Cast<GenericChild>().ToList();
                stringType = "promotions";
            }
            if (typeof(T) == typeof(MerchandisingLandingPageView))
            {
                childList = _landingPageRepo.GetAllLandingPagesWithProperties().Cast<GenericChild>().ToList();
                stringType = "landingPages";
            }
            if (typeof(T) == typeof(EndecaResortView))
            {
                childList = _resortRepo.GetAllResortsWithProperties().Cast<GenericChild>().ToList();
                stringType = "resorts";
            }
            if (typeof(T) == typeof(EndecaNeighborhoodView))
            {
                childList = _neighborhoodRepo.GetAllNeighborhoodsWithProperties().Cast<GenericChild>().ToList();
                stringType = "neighborhoods";
            }
            if (typeof(T) == typeof(EndecaGeographyView))
            {
                childList = _geographyRepo.GetAllGeographyThatHaveProperties().Cast<GenericChild>().ToList();
                stringType = "geography";
            }
            if (typeof(T) == typeof(RentalUnitContentView))
            {
                childList = _rentalUnitContentRepo.GetAllRentalContentViewWithProperties().Cast<GenericChild>().ToList();
                stringType = "rentalUnitContent";
            }
            if (typeof(T) == typeof(EndecaWeeklyRatesView))
            {
                childList = _weeklyRatesRepo.GetAllWeeklyRates().Cast<GenericChild>().ToList();// _nightlyRateRepository.GetYearAverageNightlyRate().Cast<GenericChild>().ToList(); ;
                stringType = "weeklyRates";
            }

            int batchCount = 10000;

            while(childList.Count > 0)
            {
                List<GenericChild> children = (from x in childList
                                    select x).Take(batchCount).ToList();

                string bulkPost = "";
                List<Task> tasks = new List<Task>();
                ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

                foreach (GenericChild gv in children)
                {
                    PropertyQuery propertyQuery = new PropertyQuery();
                    ElasticPropertySearchResults propertyResults = null;

                    if (typeof(T) == typeof(EndecaAmenitiesView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((EndecaAmenitiesView)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(EndecaWeeklyRatesView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((EndecaWeeklyRatesView)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(NightlyAverage))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((NightlyAverage)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(EndecaPromotionView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((EndecaPromotionView)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(MerchandisingLandingPageView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((MerchandisingLandingPageView)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(EndecaResortView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((EndecaResortView)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(EndecaNeighborhoodView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((EndecaNeighborhoodView)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(EndecaGeographyView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((EndecaGeographyView)gv).InventoryID);
                    }
                    if (typeof(T) == typeof(RentalUnitContentView))
                    {
                        propertyResults = propertyQuery.GetPropertyByInventoryID(((RentalUnitContentView)gv).InventoryId);
                    }

                    if (propertyResults.hits.hits.Length == 1)
                    {
                        Task t = Task.Run(() =>
                        {
                            bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"props\", \"_type\": \""+ stringType +"\", \"_parent\":\"" + propertyResults.hits.hits[0]._id + "\", \"_routing\":\"props\"} }" + Environment.NewLine + JsonConvert.SerializeObject(gv) + Environment.NewLine);
                        });

                        tasks.Add(t);
                    }
                }

                Task.WaitAll(tasks.ToArray());

                foreach (string p in bulkPostParts)
                {
                    bulkPost += p;
                }

                if (bulkPost == string.Empty)
                {
                    try
                    {
                        childList.RemoveRange(0, batchCount);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        childList.RemoveRange(0, childList.Count);
                    }
                    catch (ArgumentException)
                    {
                        childList.RemoveRange(0, childList.Count);
                    }

                    continue;
                }

                string path = $"props/{stringType}/_bulk?refresh=wait_for";
                ElasticSearchConnection elasticSearchConnection = new ElasticSearchConnection();
                ElasticBulkInsertResult result = elasticSearchConnection.Execute<ElasticBulkInsertResult>(path, bulkPost);

                if (result.items.Count() != batchCount)
                {
                    if (result.items.Count() != childList.Count())
                    {
                        Log l = new Log();
                        l.WriteLog($"{DateTime.Now} Bulk {stringType} insert result count didn't match input count");
                    }
                }

                foreach (Items it in result.items)
                {
                    if (it.index.created != "true")
                    {
                        Log l = new Log();
                        l.WriteLog($"{DateTime.Now} {stringType} {it.index._id} failed to be inserted");
                    }
                }

                try
                {
                    childList.RemoveRange(0, batchCount);
                }
                catch (ArgumentOutOfRangeException)
                {
                    childList.RemoveRange(0, childList.Count);
                }
                catch (ArgumentException)
                {
                    childList.RemoveRange(0, childList.Count);
                }
            }
        }
    }
}
