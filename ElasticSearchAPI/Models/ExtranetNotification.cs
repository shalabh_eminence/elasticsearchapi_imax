﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class ExtranetNotification
    {
        public long SupplierId { get; set; }
        public EmailAddress ExtranetNotificationEmailAddress { get; set; }
    }
}