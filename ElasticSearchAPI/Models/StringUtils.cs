﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace ElasticSearchAPI.Models
{
    public static class StringUtils
    {
        private static IList<string> TEST_CREDIT_CARD_NUMBERS;

        public static string LowerFirstCharacter(this string str)
        {
            return str.Substring(0, 1).ToLowerInvariant() + str.Substring(1, str.Length - 1);
        }

        public static int ToInt(this string stringToParse)
        {
            int result;
            Int32.TryParse(stringToParse, out result);
            return result;
        }

        public static decimal ToDecimal(this string stringToParse)
        {
            decimal result;
            decimal.TryParse(stringToParse, out result);
            return result;
        }

        public static decimal ToDecimalPercentage(this string value)
        {
            decimal result = value.ToDecimal();
            if (result < 1)
            {
                return value.ToDecimal();
            }
            else
            {
                return result / 100;
            }
        }

        public static double ToDouble(this string stringToParse)
        {
            double result;
            double.TryParse(stringToParse, out result);
            return result;
        }

        public static double ToDoubleZeroWhenEmpty(this string stringToParse)
        {
            double result;
            double.TryParse(stringToParse, out result);
            return result;
        }

        public static double? ToDoubleNullWhenEmpty(this string stringToParse)
        {
            double result;
            if (double.TryParse(stringToParse, out result))
            {
                return result;
            }
            return null;
        }

        public static long ToLong(this string stringToParse)
        {
            long result;
            long.TryParse(stringToParse, out result);
            return result;
        }

        public static long? ToLongNullWhenEmpty(this string value)
        {
            long result;
            if (long.TryParse(value, out result))
            {
                return result;
            }
            return null;
        }

        public static long? ToLongNullWhenEmptyOrZero(this string value)
        {
            string nullableString = NullWhenEmpty(value);
            long? result = null;
            if (!string.IsNullOrEmpty(nullableString))
            {
                long resultLong;
                if (long.TryParse(nullableString, out resultLong))
                {
                    result = resultLong;
                }
                if (result == 0)
                {
                    result = null;
                }
            }
            return result;
        }

        public static bool ToBoolFalseWhenEmpty(this string stringToParse)
        {
            bool result;
            bool.TryParse(stringToParse, out result);
            return result;
        }

        public static bool? ToBoolNullWhenEmpty(this string stringToParse)
        {
            if (!string.IsNullOrWhiteSpace(stringToParse))
            {
                return (ToBoolFalseWhenEmpty(stringToParse));
            }

            return null;
        }

        public static DateTime ToDateTime(this string stringDate)
        {
            DateTime result;
            DateTime.TryParse(stringDate, out result);
            return result;
        }

        public static DateTime? ToDateTimeNullWhenEmpty(this string stringDate)
        {
            if (!string.IsNullOrEmpty(stringDate))
            {
                DateTime result;
                if (DateTime.TryParse(stringDate, out result))
                {
                    return result;
                }
            }
            return null;
        }

        public static bool AreEqual(string a, string b)
        {
            string trimA = (a == null) ? string.Empty : a.Trim();
            string trimB = (b == null) ? string.Empty : b.Trim();

            return trimA.Equals(trimB);
        }

        public static string[] SplitToSize(string longString, int length)
        {
            int lastStringLength = longString.Length % length;
            int numberOfElements = longString.Length / length + (lastStringLength > 0 ? 1 : 0);

            string[] results = new string[numberOfElements];

            int startIndex = 0;
            for (int i = 0; i < numberOfElements; i++)
            {
                results[i] = longString.Substring(startIndex, (i == (numberOfElements - 1)) ? lastStringLength : length);
                startIndex += length;
            }

            return results;
        }

        public static string NullWhenEmpty(this string value)
        {
            string result = null;
            if (value != null && !string.Empty.Equals(value.Trim()))
            {
                result = value;
            }
            return result;
        }

        public static int? ToIntNullWhenEmpty(this string value)
        {
            string nullableString = NullWhenEmpty(value);
            int? result = null;
            if (!string.IsNullOrEmpty(nullableString))
            {
                int resultInt;
                if (int.TryParse(nullableString, out resultInt))
                {
                    result = resultInt;
                }
            }
            return result;
        }

        public static int? ToIntNullWhenEmptyOrZero(this string value)
        {
            string nullableString = NullWhenEmpty(value);
            int? result = null;
            if (!string.IsNullOrEmpty(nullableString))
            {
                int resultInt;
                if (int.TryParse(nullableString, out resultInt))
                {
                    result = resultInt;
                }
                if (result == 0)
                {
                    result = null;
                }
            }
            return result;
        }

        public static decimal? ToDecimalNullWhenEmpty(this string value)
        {
            string nullableString = NullWhenEmpty(value);
            decimal? result = null;
            if (!string.IsNullOrEmpty(nullableString))
            {
                decimal resultDecimal;
                if (decimal.TryParse(nullableString, out resultDecimal))
                {
                    result = resultDecimal;
                }
            }
            return result;
        }

        public static decimal? ToDecimalNullWhenEmptyOrZero(this string value)
        {
            string nullableString = NullWhenEmpty(value);
            decimal? result = null;
            if (!string.IsNullOrEmpty(nullableString))
            {
                decimal resultDecimal;
                if (decimal.TryParse(nullableString, out resultDecimal) && resultDecimal != 0)
                {
                    result = resultDecimal;
                }
            }
            return result;
        }



        public static bool IsNullOrEmpty(this string stringValue)
        {
            return string.IsNullOrEmpty(stringValue);
        }

        public static bool IsNullOrTrimmedEmpty(this string stringValue)
        {
            return (stringValue == null || string.IsNullOrEmpty(stringValue.Trim()));
        }

        public static string StripNonNumeric(this string value)
        {
            if (value == null)
            {
                return null;
            }

            string retVal = string.Empty;
            const string pattern = "[^0-9]";
            if (!string.IsNullOrEmpty(value))
            {
                retVal = Regex.Replace(value, pattern, string.Empty).RemoveWhiteSpace();
            }
            return retVal;
        }

        public static string FormatPhoneWithParens(this string value)
        {
            return FormatToPhone(value, "($1) $2-$3");
        }

        public static string FormatPhoneWithHyphens(this string value)
        {
            return FormatToPhone(value, "$1-$2-$3");
        }

        public static Guid ToGuid(this string guidString)
        {
            return new Guid(guidString);
        }

        public static string FormatToPhone(this string value, string mask)
        {
            string phoneNumber = value.StripNonNumeric();
            if (phoneNumber == null)
            {
                return null;
            }

            if (phoneNumber.Length == 11 && phoneNumber.StartsWith("1"))
            {
                phoneNumber = phoneNumber.Substring(1);
            }

            if (phoneNumber.Length == 10)
            {
                phoneNumber = Regex.Replace(phoneNumber, @"(\d{3})(\d{3})(\d{4})", mask);
            }
            else
            {
                phoneNumber = null;
            }
            return phoneNumber;
        }

        public static string MakeJavscriptFriendly(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string friendly = Regex.Replace(value, @"\</.+\>", string.Empty);
                friendly = Regex.Replace(friendly, @"\<.+\>", string.Empty);
                return friendly.Replace("\r\n", " ").Replace("\"", string.Empty).Replace("'", string.Empty);
            }
            return string.Empty;
        }

        public static bool IsTestCreditCardNumber(this string creditCardNumber)
        {
            if (TEST_CREDIT_CARD_NUMBERS == null)
            {
                TEST_CREDIT_CARD_NUMBERS = new List<string>
                {
                    "4111111111111111",
                    "5105105105105100",
                    "5555555555554444",
                    "4222222222222",
                    "4012888888881881",
                    "378282246310005",
                    "371449635398431",
                    "378734493671000",
                    "38520000023237",
                    "30569309025904",
                    "6011111111111117",
                    "6011000990139424",
                };
            }

            return TEST_CREDIT_CARD_NUMBERS.Contains(creditCardNumber);
        }

        public static string ToTitleCase(this string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return string.Empty;

            TextInfo textFormat = new CultureInfo("en-US", false).TextInfo;

            var result = textFormat.ToTitleCase(value.ToLower());
            if (result.Contains("'"))
            {
                var resultParts = result.Split('\'');
                result = string.Join("'", resultParts.Select(x => textFormat.ToTitleCase(x.ToLower())));
            }
            return result;
        }

        public static string ReplaceSpecialCharsWithSpace(this string value)
        {
            return Regex.Replace(value, "[-._]", " ");
        }

        public static string ReplaceNewLinesWithSpaces(this string value)
        {
            return Regex.Replace(value, "\r?\n", " ");
        }

        public static string EscapeSpecialChars(this string value)
        {
            return value == null ? string.Empty : Regex.Replace(value, "(['\"])", "\\$1").ReplaceNewLinesWithSpaces();
        }

        public static string DisplayFriendly(this string value, int length)
        {
            string display = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                display = value;
                if (value.Length > length)
                {
                    display = value.Substring(0, length);
                }
            }
            return display;
        }

        public static string ReplaceSmartQuotes(this string value)
        {
            return !string.IsNullOrEmpty(value) ? value.Replace('\u2018', '\'').Replace('\u2019', '\'').Replace('\u201c', '\"').Replace('\u201d', '\"') : value;
        }

        public static string StripNewLines(this string value)
        {
            return !string.IsNullOrEmpty(value)
                ? value.Replace("\r", "").Replace("\n", "")
                : value;
        }

        public static string RemoveWhiteSpace(this string value)
        {
            const string pattern = @"\s";
            var rgx = new Regex(pattern);
            return !string.IsNullOrEmpty(value) ? rgx.Replace(value, " ") : value;
        }

        public static string RemoveSpecialCharacters(this string value)
        {
            return !string.IsNullOrEmpty(value) ? Regex.Replace(value, "[^a-zA-Z0-9]+", string.Empty) : value;
        }

        public static string FullName(this string firstName, string lastName)
        {
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                return string.Format("{0}, {1}", lastName, firstName);
            }
            if (!string.IsNullOrEmpty(firstName))
            {
                return firstName;
            }
            if (!string.IsNullOrEmpty(lastName))
            {
                return lastName;
            }

            return string.Empty;
        }

        public static string ShortenTo(this string value, int totalCharacters)
        {
            string returnValue = value;
            if (value.Length > totalCharacters)
            {
                returnValue = string.Format("{0}...", value.Substring(0, totalCharacters));
            }
            return returnValue;
        }

        public static bool EqualsIgnoreCase(this string value, string compareValue)
        {
            return value.Equals(compareValue, StringComparison.OrdinalIgnoreCase);
        }

        public static string[] Split(this string value, string separator)
        {
            return value.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] Split(this string value, string separator, StringSplitOptions options)
        {
            return value.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string JoinNonEmptyParts(this IEnumerable<string> dashedParts, string separator)
        {
            return string.Join(separator, dashedParts.Where(x => !string.IsNullOrWhiteSpace(x)));
        }

        public static string Last(this string input, int length)
        {
            return input.Substring(input.Length - length, length);
        }

        public static string ToEmptyStringWhenNull(this string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value;
        }
    }
}