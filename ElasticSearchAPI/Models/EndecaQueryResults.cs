﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class EndecaQueryResults
    {
        public const string SupplementalFeatured = "Featured Properties";

        public Dictionary<string, List<Record>> SupplementalResults { get; set; }

        private readonly List<Record> _results = new List<Record>();
        public ReadOnlyCollection<Record> Results
        {
            get
            {
                return _results.AsReadOnly();
            }
        }

        public void AddResult(Record record)
        {
            _results.Add(record);
        }

        public void AddResultsRange(IEnumerable<Record> collection)
        {
            _results.AddRange(collection);
        }

        public EndecaQueryResults WithRecord(Record record)
        {
            AddResult(record);

            return this;
        }

        private readonly NavigationRefinements _navigationRefinements = new NavigationRefinements();
        public NavigationRefinements NavigationRefinements
        {
            get { return _navigationRefinements; }
        }

        public long TotalRecords { get; set; }

        public int ResultsPerPage { get; set; }
    }
}