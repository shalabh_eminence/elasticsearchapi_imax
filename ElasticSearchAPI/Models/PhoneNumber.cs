﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class PhoneNumber : IValueObject
    {
        public enum DisplayFormat
        {
            /// <summary>
            /// (800) 123-4567
            /// </summary>
            ParensSpaceDash,
            /// <summary>
            /// 1 (800) 123-4567
            /// </summary>
            ParensSpaceDashWithOne,
            /// <summary>
            /// 1.800.123.4567
            /// </summary>
            DotSeparatedWithOne,
            /// <summary>
            /// 800.123.4567
            /// </summary>
            DotSeparated,
            /// <summary>
            /// 1-800-123-4567
            /// </summary>
            DashSeparatedWithOne,
            /// <summary>
            /// 800-123-4567
            /// </summary>
            DashSeparated,
        }

        private const string ParensSpaceDash = "($1) $2-$3";
        private const string ParensSpaceDashWithOne = "1 ($1) $2-$3";
        private const string ParensSpaceDashInternational = "$1 ($2) $3-$4";
        private const string DotSeparatedWithOne = "1.$1.$2.$3";
        private const string DotSeparated = "$1.$2.$3";
        private const string DashSeparatedWithOne = "1-$1-$2-$3";
        private const string DashSeparated = "$1-$2-$3";
        private const string InternationalFormatString = "$1 $2 $3 $4";
        private const string LargeInternationalFormatString = "$1 $2";

        public static PhoneNumber VacationRoostDefault = new PhoneNumber("888-337-6678");

        private readonly string _phoneNumber;
        private readonly string _extension;

        public PhoneNumber(string phoneNumber) : this(phoneNumber, null) { }

        public PhoneNumber(PhoneNumber phoneNumber, string extension) : this(phoneNumber.Value, extension) { }

        public PhoneNumber(string phoneNumber, string extension)
        {
            if (!IsValid(phoneNumber))
            {
                throw new ArgumentException("The specified value must be a valid phone number.", "phoneNumber");
            }

            _phoneNumber = FormatForStorage(phoneNumber);
            _extension = extension;
        }

        private static string FormatForStorage(string phoneNumber)
        {
            if (phoneNumber.StartsWith("1-") || phoneNumber.StartsWith("1 "))
            {
                phoneNumber = phoneNumber.Substring(2);
            }
            if (phoneNumber.StartsWith("011"))
            {
                phoneNumber = phoneNumber.Substring(3);
            }

            phoneNumber = phoneNumber.StripNonNumeric();

            return phoneNumber;
        }

        public string Value
        {
            get { return _phoneNumber; }
        }

        public string Extension
        {
            get { return _extension; }
        }

        public string DisplayValue
        {
            get { return GetDisplayValue(DisplayFormat.ParensSpaceDash); }
        }

        public string GetDisplayValue(DisplayFormat format)
        {
            if (Value.Length == 10)
            {
                string formatString;
                switch (format)
                {
                    case DisplayFormat.ParensSpaceDashWithOne:
                        formatString = ParensSpaceDashWithOne;
                        break;
                    case DisplayFormat.DotSeparatedWithOne:
                        formatString = DotSeparatedWithOne;
                        break;
                    case DisplayFormat.DotSeparated:
                        formatString = DotSeparated;
                        break;
                    case DisplayFormat.DashSeparatedWithOne:
                        formatString = DashSeparatedWithOne;
                        break;
                    case DisplayFormat.DashSeparated:
                        formatString = DashSeparated;
                        break;
                    default:
                        formatString = ParensSpaceDash;
                        break;
                }

                return AppendExtensionForDisplay(Regex.Replace(Value, "(\\d{3})(\\d{3})(\\d{4})", formatString));
            }

            if (Value.Length == 11)
            {
                return AppendExtensionForDisplay(Regex.Replace(Value, "(\\d)(\\d{3})(\\d{3})(\\d{4})", ParensSpaceDashInternational));
            }

            if (Value.Length == 12)
            {
                return AppendExtensionForDisplay(Regex.Replace(Value, "(\\d{2})(\\d{3})(\\d{3})(\\d{4})", InternationalFormatString));
            }

            if (Value.Length == 13)
            {
                return AppendExtensionForDisplay(Regex.Replace(Value, "(\\d{2})(\\d{3})(\\d{4})(\\d{4})", InternationalFormatString));
            }

            return AppendExtensionForDisplay(Regex.Replace(Value, "(\\d{2})(\\d+)", LargeInternationalFormatString));
        }

        private string AppendExtensionForDisplay(string formattedMainNumber)
        {
            if (_extension == null)
            {
                return formattedMainNumber;
            }

            return formattedMainNumber + " x" + _extension;
        }
        public override string ToString()
        {
            return DisplayValue;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var phoneNumber = obj as PhoneNumber;

            if (phoneNumber != null)
            {
                return this == phoneNumber;
            }

            return false;
        }

        public static bool operator ==(PhoneNumber left, PhoneNumber right)
        {
            // if it's the same reference (including both null), they're equal
            if (ReferenceEquals(left, right)) return true;

            // otherwise, if either is null then they're not equal 
            if (ReferenceEquals(null, left)) return false;
            if (ReferenceEquals(null, right)) return false;

            return (string.Compare(left.Value, right.Value, true) == 0) && (string.Compare(left.Extension, right.Extension, true) == 0);
        }

        public static bool operator !=(PhoneNumber left, PhoneNumber right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return (_phoneNumber != null ? _phoneNumber.GetHashCode() : 0);
        }

        public static bool IsValid(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return false;
            }
            if (phoneNumber.StartsWith("1-") || phoneNumber.StartsWith("1 "))
            {
                phoneNumber = phoneNumber.Substring(2);
            }
            if (phoneNumber.StartsWith("011"))
            {
                phoneNumber = phoneNumber.Substring(3);
            }

            int length = phoneNumber.StripNonNumeric().Length;

            //international numbers can be between 11 and 15 digits
            return 10 <= length && length <= 15;
        }

        public static PhoneNumber Parse(string phoneNumber)
        {
            return IsValid(phoneNumber) ? new PhoneNumber(phoneNumber) : null;
        }
    }
}