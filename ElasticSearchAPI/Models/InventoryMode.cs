﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class InventoryMode : CodeValue<InventoryMode>, IComparable<InventoryMode>
    {
        public static class Codes
        {
            public const string RUN_OF_HOUSE = "RUN_OF_HOUSE";
            public const string UNIT_SPECIFIC = "UNIT_SPECIFIC";
        }

        public static readonly InventoryMode RunOfHouse = new InventoryMode(Codes.RUN_OF_HOUSE, "Run of House");
        public static readonly InventoryMode UnitSpecific = new InventoryMode(Codes.UNIT_SPECIFIC, "Unit Specific");

        private InventoryMode(string code, string description) : base(code, description) { }

        public int CompareTo(InventoryMode other)
        {
            // Description sort if Code is equal ascending
            if (Description == other.Description)
            {
                return Description.CompareTo(other.Description);
            }
            // Default to sort by Code. [ascending]
            return Code.CompareTo(other.Description);
        }
    }
}