﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public static class EnumerableUtils
    {
        public static IEnumerable<T> Distinct<T, U>(this IEnumerable<T> source, Func<T, U> uniqueKeySelector)
        {
            var itemSet = new HashSet<U>();
            return source.Where(item => itemSet.Add(uniqueKeySelector(item)));
        }

        public static IEnumerable<T> Except<T>(this IEnumerable<T> collection, T itemToExclude)
        {
            return collection.Except(new[] { itemToExclude });
        }

        public static bool ContainsAll<T>(this IEnumerable<T> containingCollection, IEnumerable<T> collectionOfItemsContained)
        {
            return collectionOfItemsContained.All(containingCollection.Contains);
        }

        public static IEnumerable<T> And<T>(this IEnumerable<T> first, IEnumerable<T> second)
        {
            foreach (T item in first)
            {
                yield return item;
            }
            foreach (T item in second)
            {
                yield return item;
            }
        }

        public static IEnumerable<T> And<T>(this IEnumerable<T> first, T other)
        {
            foreach (T item in first)
            {
                yield return item;
            }
            yield return other;
        }

        public static string AsReadableList(this IEnumerable<string> source)
        {
            var items = source.ToArray();
            if (items.Length < 2)
            {
                return items.FirstOrDefault();
            }
            return string.Join(", ", items.Take(items.Length - 1)) + " and " + items.Last();
        }

        public static IEnumerable<IEnumerable<T>> SplitIntoChunks<T>(this IEnumerable<T> source, int itemsPerChunk)
        {
            var enumerator = source.GetEnumerator();
            while (true)
            {
                var currentHelper = new SplitHelper<T>(enumerator, itemsPerChunk);
                if (!currentHelper.Done)
                {
                    yield return currentHelper;
                }
                else
                {
                    yield break;
                }
            }
        }

        private class SplitHelper<T> : IEnumerable<T>
        {
            private int _currentItem;
            private readonly IEnumerator<T> _source;
            private readonly int _itemsPerChunk;

            public SplitHelper(IEnumerator<T> source, int itemsPerChunk)
            {
                _source = source;
                _itemsPerChunk = itemsPerChunk;

                Done = !_source.MoveNext();
            }

            public bool Done { get; private set; }

            public IEnumerator<T> GetEnumerator()
            {
                do
                {
                    yield return _source.Current;
                    if (++_currentItem == _itemsPerChunk)
                    {
                        Done = false;
                        yield break;
                    }
                } while (_source.MoveNext());
                Done = true;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}