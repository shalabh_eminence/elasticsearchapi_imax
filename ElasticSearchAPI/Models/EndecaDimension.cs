﻿using System.Collections.Generic;

namespace ElasticSearchAPI.Models
{
    public class EndecaDimension : CodeValue<EndecaDimension>
    {
        private readonly long _dimensionId;
        private readonly string _urlParameter;
        private static readonly Dictionary<long, EndecaDimension> _mapById = new Dictionary<long, EndecaDimension>();
        private static readonly Dictionary<string, EndecaDimension> _mapByUrlParameter = new Dictionary<string, EndecaDimension>();

        public static class Codes
        {
            public const string COUNTRY_OR = "COUNTRY_OR";
            public const string STATE = "STATE";
            public const string STATE_OR = "STATE_OR";
            public const string DESTINATION = "DESTINATION";
            public const string DESTINATION_OR = "DESTINATION_OR";
            public const string DESTINATION_TYPE_CATEGORY = "DESTINATIONTYPECATEGORY";
            public const string DESTINATION_TYPE_CATEGORY_OR = "DESTINATIONTYPECATEGORY_OR";
            public const string CITY = "CITY";
            public const string CITY_OR = "CITY_OR";
            public const string BEDROOMS_OR = "BEDROOMS_OR";
            public const string SPECIAL_BY_DESTINATION = "SPECIALBYDESTINATION";
            public const string HAS_VIRTUAL_TOUR = "HASVIRTUALTOUR";
            public const string PROMOTION = "PROMOTION";
            public const string MERCHANDISING_GROUP_PAGE = "MERCHANDISING_LANDING_PAGE";
            public const string SOURCE_TYPE = "SOURCE_TYPE";
            public const string NEIGHBORHOOD_OR = "NEIGHBORHOOD_OR";
            public const string RESORT = "RESORT";
            public const string RESORT_OR = "RESORT_OR";
            public const string ISLAND_OR = "ISLAND_OR";
            public const string PROXIMITY = "PROXIMITY";
            public const string PROXIMITY_OR = "PROXIMITY_OR";
            public const string AMENITIES = "AMENITIES";
            public const string AMENITIES_AND = "AMENITIES_AND";
            public const string PROPERTY_TYPE_OR = "PROPERTY_TYPE_OR";
            public const string SLEEPS = "SLEEPS";
            public const string SLEEPS_OR = "SLEEPS_OR";
            public const string SUPPLIER_OR = "SUPPLIER_OR";
            public const string RATING = "RATING";
            public const string BUILDING_CATEGORY_OR = "BUILDING_CATEGORY_OR";
            public const string BUILDING_TYPE_OR = "BUILDING_TYPE_OR";
        }

        public static readonly EndecaDimension CountryOr = new EndecaDimension(Codes.COUNTRY_OR, "Country Or", 273, "Country");
        public static readonly EndecaDimension State = new EndecaDimension(Codes.STATE, "State", 138, "State");
        public static readonly EndecaDimension StateOr = new EndecaDimension(Codes.STATE_OR, "State OR", 163, "StateOr");
        public static readonly EndecaDimension Destination = new EndecaDimension(Codes.DESTINATION, "Destination", 11, "Destination");
        public static readonly EndecaDimension DestinationOr = new EndecaDimension(Codes.DESTINATION_OR, "Destination Or", 162, "DestinationOr");
        public static readonly EndecaDimension DestinationTypeCategory = new EndecaDimension(Codes.DESTINATION_TYPE_CATEGORY, "Destination Type Category", 159, "DestinationTypeSingle");
        public static readonly EndecaDimension DestinationTypeCategoryOr = new EndecaDimension(Codes.DESTINATION_TYPE_CATEGORY_OR, "Destination Type Category Or", 310, "DestinationType");
        public static readonly EndecaDimension City = new EndecaDimension(Codes.CITY, "City", 137, "City");
        public static readonly EndecaDimension CityOr = new EndecaDimension(Codes.CITY_OR, "City Or", 164, "CityOr");
        public static readonly EndecaDimension BedroomsOr = new EndecaDimension(Codes.BEDROOMS_OR, "Bedrooms Or", 250, "Bedrooms");
        public static readonly EndecaDimension SpecialByDestination = new EndecaDimension(Codes.SPECIAL_BY_DESTINATION, "Special By Destination", 188, "SpecialByDestination");
        public static readonly EndecaDimension HasVirtualTour = new EndecaDimension(Codes.HAS_VIRTUAL_TOUR, "Has Virtual Tour", 223, "HasVirtualTour");
        public static readonly EndecaDimension Promotion = new EndecaDimension(Codes.PROMOTION, "Promotion", 248, "Promotion");
        public static readonly EndecaDimension MerchandisingGroupPage = new EndecaDimension(Codes.MERCHANDISING_GROUP_PAGE, "Merchandising Group Page", 249, "MerchandisingGroupPage");
        public static readonly EndecaDimension SourceType = new EndecaDimension(Codes.SOURCE_TYPE, "SourceType", 149, "SourceType");
        public static readonly EndecaDimension NeighborhoodOr = new EndecaDimension(Codes.NEIGHBORHOOD_OR, "Neighborhood Or", 270, "Neighborhood");
        public static readonly EndecaDimension ResortOr = new EndecaDimension(Codes.RESORT_OR, "Resort Or", 313, "Resort");
        public static readonly EndecaDimension IslandOr = new EndecaDimension(Codes.ISLAND_OR, "Island Or", 311, "Island");
        public static readonly EndecaDimension ProximityOr = new EndecaDimension(Codes.PROXIMITY_OR, "Proximity Or", 274, "Proximity");
        public static readonly EndecaDimension Amenities = new EndecaDimension(Codes.AMENITIES, "Amenities", 131, "AmenitiesAll");
        public static readonly EndecaDimension AmenitiesAnd = new EndecaDimension(Codes.AMENITIES_AND, "Amenities And", 171, "Amenities");
        public static readonly EndecaDimension SleepsOr = new EndecaDimension(Codes.SLEEPS_OR, "Sleeps Or", 264, "Sleeps");
        public static readonly EndecaDimension SupplierOr = new EndecaDimension(Codes.SUPPLIER_OR, "Supplier Or", 327, "Supplier");
        public static readonly EndecaDimension Rating = new EndecaDimension(Codes.RATING, "Rating", 328, "Rating");
        public static readonly EndecaDimension BuildingCategoryOr = new EndecaDimension(Codes.BUILDING_CATEGORY_OR, "Building Category", 331, "PropertyCategory");
        public static readonly EndecaDimension BuildingTypeOr = new EndecaDimension(Codes.BUILDING_TYPE_OR, "Building Type", 337, "PropertyType");

        private EndecaDimension(string code, string description, long dimensionId, string urlParameter) : base(code, description)
        {
            _dimensionId = dimensionId;
            _urlParameter = urlParameter;
            _mapById.Add(dimensionId, this);
            _mapByUrlParameter.Add(urlParameter.ToUpper(), this);
        }

        public long DimensionId
        {
            get { return _dimensionId; }
        }

        public string UrlParameter
        {
            get { return _urlParameter; }
        }

        public static EndecaDimension Lookup(long dimensionId)
        {
            return _mapById.ContainsKey(dimensionId) ? _mapById[dimensionId] : null;
        }

        public static EndecaDimension LookupByUrlParameter(string urlParameter)
        {
            string upper = urlParameter.ToUpper();
            return _mapByUrlParameter.ContainsKey(upper) ? _mapByUrlParameter[upper] : null;
        }
    }
}