﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class TravelDates : IEnumerable<DateTime>
    {
        private readonly DateRange _stayRange;

        public TravelDates(DateTime checkIn, DateTime checkOut)
        {
            if (checkOut.Date <= checkIn.Date)
            {
                throw new ArgumentException("Argument checkOut must be later than argument checkIn.");
            }

            _stayRange = new DateRange(checkIn, checkOut.AddDays(-1));
        }

        public TravelDates(DateTime checkIn, DateTime checkOut, ICheckInDateSpecification checkInDateSpecification)
            : this(checkIn, checkOut)
        {
            if (!checkInDateSpecification.IsCheckInAllowed(checkIn))
            {
                throw new ArgumentException("Argument does not meet the current check in date specification.", "checkIn");
            }
        }

        public TravelDates(DateTime checkIn, int numberOfNights)
            : this(checkIn, checkIn.AddDays(numberOfNights))
        {
        }

        public TravelDates(DateTime checkIn, int numberOfNights, ICheckInDateSpecification checkInDateSpecification)
            : this(checkIn, checkIn.AddDays(numberOfNights), checkInDateSpecification)
        {
        }

        public int DaysToCheckin
        {
            get { return StayRange.Begin.Subtract(DateTime.Today).Days; }
        }

        public DateTime CheckIn
        {
            get { return StayRange.Begin; }
        }

        public DateTime CheckOut
        {
            get { return StayRange.End.AddDays(1); }
        }

        public DateRange StayRange
        {
            get { return _stayRange; }
        }

        public string CheckInWeek
        {
            get { return CalculateCheckinWeek(CheckIn); }
        }

        public int TotalNights
        {
            get { return (CheckOut - CheckIn).Days; }
        }

        public DateTime LastStayDate
        {
            get { return StayRange.End; }
        }

        public int BookingPace
        {
            get { return CheckIn.Subtract(DateTime.Today).Days; }
        }

        public static TravelDates Validate(DateTime? checkIn, DateTime? checkOut, out string error)
        {
            TravelDates travelDates = null;
            error = string.Empty;

            if ((checkIn.HasValue && !checkOut.HasValue) || (!checkIn.HasValue && checkOut.HasValue))
            {
                error = "Both check in and check out are required";
            }
            if (checkIn.HasValue && checkOut.HasValue)
            {
                if (checkIn.Value > checkOut.Value)
                {
                    error = "Check in date must be prior to check out date";
                }
                else if (checkIn < DateTime.Today)
                {
                    error = "Date cannot be in the past";
                }
                else
                {
                    travelDates = new TravelDates(checkIn.Value, checkOut.Value);
                }
            }
            return travelDates;
        }

        public static TravelDates Parse(string checkIn, string checkOut)
        {
            if (string.IsNullOrEmpty(checkIn) || string.IsNullOrEmpty(checkOut))
            {
                return null;
            }

            DateTime checkInDate;
            if (!DateTime.TryParse(checkIn, out checkInDate))
            {
                return null;
            }

            DateTime checkOutDate;
            if (!DateTime.TryParse(checkOut, out checkOutDate))
            {
                return null;
            }

            if (checkOutDate <= checkInDate)
            {
                return null;
            }

            return new TravelDates(checkInDate, checkOutDate);
        }

        public static TravelDates Parse(string checkIn, string checkOut, ICheckInDateSpecification checkInDateSpecification)
        {
            DateTime checkInDate;
            if (!DateTime.TryParse(checkIn, out checkInDate))
            {
                return null;
            }
            if (!checkInDateSpecification.IsCheckInAllowed(checkInDate))
            {
                return null;
            }
            return Parse(checkIn, checkOut);
        }

        public static TravelDates Parse(DateTime checkIn, DateTime checkOut, ICheckInDateSpecification checkInDateSpecification)
        {
            if (!checkInDateSpecification.IsCheckInAllowed(checkIn))
            {
                return null;
            }

            return Parse(checkIn.ToShortDateString(), checkOut.ToShortDateString());
        }

        public static bool CanDetermineAverageNightlyRate(DateTime? checkIn, DateTime? checkOut)
        {
            if (checkIn.HasValue && checkOut.HasValue)
            {
                return (checkIn.Value < checkOut.Value);
            }

            return false;
        }

        public static string CalculateCheckinWeek(DateTime? checkin, DateTime referenceDate)
        {
            if (!checkin.HasValue)
            {
                return "01";
            }

            var cultureInfo = new CultureInfo("en-US");
            int thisWeek = cultureInfo.Calendar.GetWeekOfYear(referenceDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday); //-- 35
            int thisYear = cultureInfo.Calendar.GetYear(referenceDate); //-- 2008

            DateTime checkinDate = checkin.Value; //-- 03/17/2009
            int checkinYear = cultureInfo.Calendar.GetYear(checkinDate); //-- 2009
            int checkinWeekNumber = cultureInfo.Calendar.GetWeekOfYear(checkinDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday) + 1; //-- 11
            if (checkinYear > thisYear && checkinWeekNumber < 54)
            {
                checkinWeekNumber = (checkinWeekNumber - 1) + (54 - thisWeek);
            }
            else
            {
                checkinWeekNumber = checkinWeekNumber <= thisWeek ? 1 : checkinWeekNumber - thisWeek;
            }
            return checkinWeekNumber.ToString().PadLeft(2, char.Parse("0"));
        }

        public static string CalculateCheckinWeek(DateTime? checkin)
        {
            return CalculateCheckinWeek(checkin, DateTime.Now);
        }

        public bool Contains(DateTime date)
        {
            return StayRange.Contains(date);
        }

        public IEnumerator<DateTime> GetEnumerator()
        {
            return _stayRange.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            TravelDates other = (TravelDates)obj;
            return CheckIn == other.CheckIn && CheckOut == other.CheckOut;
        }

        public override int GetHashCode()
        {
            return ObjectUtilities.CombineHashCodes(CheckIn.GetHashCode(), CheckOut.GetHashCode());
        }

        public static bool operator ==(TravelDates lhs, TravelDates rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(TravelDates lhs, TravelDates rhs)
        {
            return !(lhs == rhs);
        }

        public override string ToString()
        {
            return CheckIn.ToShortDateString() + " - " + CheckOut.ToShortDateString();
        }

        public string ShortDisplay
        {
            get { return string.Format("{0:M/d} - {1:M/d}", CheckIn, CheckOut); }
        }

        public TravelDates MergeWith(TravelDates travelDates)
        {
            return new TravelDates(CheckIn < travelDates.CheckIn ? CheckIn : travelDates.CheckIn, CheckOut > travelDates.CheckOut ? CheckOut : travelDates.CheckOut);
        }
    }

    public static class TravelDatesUtils
    {
        public static TravelDates MinimalSuperset(this IEnumerable<TravelDates> source)
        {
            return new TravelDates(source.Min(x => x.CheckIn), source.Max(x => x.CheckOut));
        }

        public static bool IsDuringWinterMonths(this TravelDates travelDates)
        {
            return WinterMonths.Contains(travelDates.CheckIn.Month);
        }

        public static bool IsDuringSummerMonths(this TravelDates travelDates)
        {
            return SummerMonths.Contains(travelDates.CheckIn.Month);
        }

        private static List<int> WinterMonths
        {
            get
            {
                return new List<int> { 11, 12, 1, 2, 3, 4 };
            }
        }

        private static List<int> SummerMonths
        {
            get
            {
                return new List<int> { 5, 6, 7, 8, 9, 10 };
            }
        }
    }
}