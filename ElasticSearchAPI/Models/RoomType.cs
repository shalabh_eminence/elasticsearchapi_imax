﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class RoomType : IRoomType
    {
        protected readonly List<string> _roomTypeViews = new List<string>();
        protected readonly List<string> _roomTypeSplurgeAmenities = new List<string>();

        public long RentalUnitId { get; set; }
        public string RoomName { get; set; }
        public string RoomClassPrefix { get; set; }
        public string RoomClassSuffix { get; set; }
        public string SplurgeView { get; set; }
        public IFloorPlan FloorPlan { get; set; }

        public string RoomTypeName
        {
            get
            {
                if (string.IsNullOrEmpty(RoomName))
                {
                    var result = string.Format("{0} {1} {2}", RoomClassPrefix, FloorPlan != null ? FloorPlan.FloorPlanDescription : string.Empty, RoomClassSuffix);
                    return result.Trim();
                }
                return string.Format("{0} ({1})", RoomName, FloorPlan.FloorPlanDescription);
            }
        }

        public ReadOnlyCollection<string> Views
        {
            get
            {
                return _roomTypeViews.AsReadOnly();
            }
        }

        public ReadOnlyCollection<string> ViewsIncludingSplurgeView
        {
            get
            {
                var allViews = new List<string>(Views);
                if (!string.IsNullOrWhiteSpace(SplurgeView))
                {
                    allViews.Insert(0, SplurgeView);
                }

                return allViews.AsReadOnly();
            }
        }

        public string ViewsDisplay
        {
            get
            {
                return string.Join(",", ViewsIncludingSplurgeView.ToArray());
            }
        }

        public ReadOnlyCollection<string> SplurgeAmenities
        {
            get
            {
                return _roomTypeSplurgeAmenities.AsReadOnly();
            }
        }

        public string SplurgeAmenitiesDisplay
        {
            get { return string.Join(", ", _roomTypeSplurgeAmenities); }
        }

        public string SplurgeAmenitiesDisplayWithPlusSeparator
        {
            get { return string.Join(" + ", _roomTypeSplurgeAmenities); }
        }

        public void AddView(string roomTypeView)
        {
            if (_roomTypeViews.SingleOrDefault(i => i == roomTypeView) == null)
                _roomTypeViews.Add(roomTypeView);
        }

        public void RemoveView(string roomTypeView)
        {
            _roomTypeViews.Remove(roomTypeView);
        }

        public void AddSplurgeAmenity(string splurgeAmenity)
        {
            if (_roomTypeSplurgeAmenities.SingleOrDefault(i => i == splurgeAmenity) == null)
                _roomTypeSplurgeAmenities.Add(splurgeAmenity);
        }

        public void RemoveSplurgeAmenity(string splurgeAmenity)
        {
            _roomTypeSplurgeAmenities.Remove(splurgeAmenity);
        }

        public void RemoveAllViews()
        {
            _roomTypeViews.Clear();
        }

        public void RemoveAllSplurgeAmenities()
        {
            _roomTypeSplurgeAmenities.Clear();
        }
    }
}