﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class SortKey
    {
        public SortKey(string key)
            : this(key, false)
        {

        }

        public SortKey(string key, bool isDescending)
        {
            _key = key;
            _isDescending = isDescending;
        }

        private readonly string _key;
        public string Key { get { return _key; } }

        private readonly bool _isDescending;
        public bool IsDescending { get { return _isDescending; } }

        public string GetSortString()
        {
            return Key + (IsDescending ? "|1" : string.Empty);
        }

        public static List<SortKey> ParseList(string sortKeyList)
        {
            string[] sortKeyStrings = sortKeyList.Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

            var result = new List<SortKey>();

            foreach (string sortKeyString in sortKeyStrings)
            {
                result.Add(Parse(sortKeyString));
            }

            return result;
        }

        public static SortKey Parse(string sortKeyString)
        {
            if (string.IsNullOrEmpty(sortKeyString))
            {
                return null;
            }

            string[] sortKeyParts = sortKeyString.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            if (sortKeyParts.Length > 1)
            {
                return new SortKey(sortKeyParts[0], sortKeyParts[1] == "1");
            }

            return new SortKey(sortKeyParts[0]);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (SortKey)obj;

            return Key == other.Key && IsDescending == other.IsDescending;
        }

        public override int GetHashCode()
        {
            return ObjectUtilities.CombineHashCodes(Key.GetHashCode(), IsDescending.GetHashCode());
        }

        public static bool operator ==(SortKey lhs, SortKey rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(SortKey lhs, SortKey rhs)
        {
            return !(lhs == rhs);
        }

        public override string ToString()
        {
            return GetSortString();
        }

    }
}