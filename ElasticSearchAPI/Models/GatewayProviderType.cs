﻿using System.Collections.Generic;
using System.Linq;

namespace ElasticSearchAPI.Models
{
    public sealed class GatewayProviderType : CodeValue<GatewayProviderType>
    {
        public static class Codes
        {
            public const string BAREFOOT = "BAREFOOT";
            public const string BAREFOOT_NON_MOR = "BAREFOOT_NON_MOR";
            public const string GENARES = "GENARES";
            public const string HBR = "HBR";
            public const string INNTOPIA = "INNTOPIA";
            public const string INNTOPIA_PMC_RESERVATIONS = "INNTOPIAPMCRESERVATIONS";
            public const string INTERHOME = "INTERHOME";
            public const string IQWARE = "IQWARE";
            public const string ISI = "ISI";
            public const string ISI_NON_MOR = "ISINONMOR";
            public const string LEISURELINK = "LEISURELINK";
            public const string LEISURELINK_DIRECT = "LEISURELINK-DIRECT";
            public const string LTA_MANUAL = "LTA_MANUAL";
            public const string MANUAL = "MANUAL";
            public const string MANUAL_NON_MOR = "MANUALNONMOR";
            public const string SYNXIS = "SYNXIS";
            public const string VACATIONROOOST = "VACATIONROOST";
        }

        public static readonly GatewayProviderType BareFoot = new GatewayProviderType(Codes.BAREFOOT, "Barefoot", false, true, false, false, false);
        public static readonly GatewayProviderType BareFootNonMOR = new GatewayProviderType(Codes.BAREFOOT_NON_MOR, "Barefoot Non-MOR", false, false, false, false, false);
        public static readonly GatewayProviderType Genares = new GatewayProviderType(Codes.GENARES, "Genares", false, true, false, false, false);
        public static readonly GatewayProviderType HBR = new GatewayProviderType(Codes.HBR, "Hawaiian Beach Rentals", false, true, false, true, true);
        public static readonly GatewayProviderType Inntopia = new GatewayProviderType(Codes.INNTOPIA, "Inntopia", false, true, true, false, false);
        public static readonly GatewayProviderType InntopiaPmcReservations = new GatewayProviderType(Codes.INNTOPIA_PMC_RESERVATIONS, "Inntopia PMC Reservations", true, false, false, false, false);
        public static readonly GatewayProviderType InterHome = new GatewayProviderType(Codes.INTERHOME, "InterHome", false, false, true, false, false);
        public static readonly GatewayProviderType IQWare = new GatewayProviderType(Codes.IQWARE, "IQWare", true, true, false, true, true);
        public static readonly GatewayProviderType ISI = new GatewayProviderType(Codes.ISI, "ISI", false, true, false, false, false);
        public static readonly GatewayProviderType ISINonMOR = new GatewayProviderType(Codes.ISI_NON_MOR, "ISI Non-MOR", false, false, false, false, false);
        public static readonly GatewayProviderType LeisureLink = new GatewayProviderType(Codes.LEISURELINK, "LeisureLink", false, true, false, true, false);
        public static readonly GatewayProviderType LeisureLinkDirect = new GatewayProviderType(Codes.LEISURELINK_DIRECT, "LeisureLink-Direct", false, true, false, true, false);
        public static readonly GatewayProviderType LTAManual = new GatewayProviderType(Codes.LTA_MANUAL, "LTA", true, true, false, false, false);
        public static readonly GatewayProviderType Manual = new GatewayProviderType(Codes.MANUAL, "Manual", true, true, false, false, false);
        public static readonly GatewayProviderType ManualNonMOR = new GatewayProviderType(Codes.MANUAL_NON_MOR, "Manual Non-MOR", true, false, false, false, false);
        public static readonly GatewayProviderType Synxis = new GatewayProviderType(Codes.SYNXIS, "Synxis", false, true, true, false, false);
        public static readonly GatewayProviderType VacationRoost = new GatewayProviderType(Codes.VACATIONROOOST, "VacationRoost", true, true, false, false, false);

        private readonly bool _isManual;
        private readonly bool _isMerchantOfRecord;
        private readonly bool _supportsCancel;
        private readonly bool _usesGatewayMappingService;
        private readonly bool _supportsGatewayConnectionVerify;

        private GatewayProviderType(string code, string description, bool isManual, bool isMerchantOfRecord, bool supportsCancel, bool usesGatewayMappingService, bool supportsGatewayConnectionVerify)
            : base(code, description)
        {
            _isManual = isManual;
            _isMerchantOfRecord = isMerchantOfRecord;
            _supportsCancel = supportsCancel;
            _usesGatewayMappingService = usesGatewayMappingService;
            _supportsGatewayConnectionVerify = supportsGatewayConnectionVerify;
        }

        public bool IsManual
        {
            get { return _isManual; }
        }

        public static List<GatewayProviderType> AllMORGatewayTypes
        {
            get { return Map.List.Where(t => t.IsMerchantOfRecord && !t.IsBlank).ToList(); }
        }

        public static List<GatewayProviderType> AllNonMORGatewayTypes
        {
            get { return Map.List.Where(t => !t.IsMerchantOfRecord && !t.IsBlank).ToList(); }
        }

        public static List<GatewayProviderType> AllOnlineGatewayTypes
        {
            get { return Map.List.Where(t => !t.IsManual && !t.IsBlank).ToList(); }
        }

        public bool IsMerchantOfRecord
        {
            get { return _isMerchantOfRecord; }
        }

        public MerchantOfRecord MerchantOfRecord
        {
            get { return _isMerchantOfRecord ? MerchantOfRecord.Retailer : MerchantOfRecord.Supplier; }
        }

        public bool SupportsCancel
        {
            get { return _supportsCancel; }
        }

        public bool UsesGatewayMappingService
        {
            get { return _usesGatewayMappingService; }
        }

        public GatewayProviderType ManualVersion
        {
            get
            {
                switch (Code)
                {
                    case Codes.LTA_MANUAL:
                        return LTAManual;
                    case Codes.ISI_NON_MOR:
                    case Codes.MANUAL_NON_MOR:
                        return ManualNonMOR;
                    default:
                        return Manual;
                }
            }
        }

        public bool SupportsGatewayConnectionVerify
        {
            get { return _supportsGatewayConnectionVerify; }
        }
    }
}