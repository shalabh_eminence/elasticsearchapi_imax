﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class RentalUnitContent : IRentalUnitContent
    {
        long IPersistedRentalUnitWithRoomType.RentalUnitId { get { return Id; } }

        private const string PhotoDisclaimer = "Photos are representative of the property and may differ from the actual unit that is reserved.";

        public long Id { get; set; }

        public InventoryMode InventoryMode { get; set; }

        public string ComplexName { get; set; }

        public BuildingType BuildingType { get; set; }

        public string ComplexDescription { get; set; }

        public GeoLocation GeoLocation { get; set; }

        public string Proximity { get; set; }

        public IRoomType RoomType { get; set; }

        public string BathsDisplay
        {
            get
            {
                if (!IsBathroomPrivate)
                {
                    return "Shared";
                }

                var parts = new List<string>
                {
                    FullBathrooms > 0 ? FullBathrooms + " Full" : null,
                    ThreeQuarterBathrooms > 0 ? ThreeQuarterBathrooms + " 3/4 Shower Only - No Tub" : null,
                    HalfBathrooms > 0 ? HalfBathrooms + " Half" : null,
                };

                return parts.JoinNonEmptyParts(" & ");
            }
        }

        public bool IsBathroomPrivate { get; set; }
        public int FullBathrooms { get; set; }
        public int ThreeQuarterBathrooms { get; set; }
        public int HalfBathrooms { get; set; }
        public int ImageCount { get; set; }

        public decimal BathroomCount
        {
            get
            {
                decimal result = 0;
                if (IsBathroomPrivate)
                {
                    result += FullBathrooms;
                    result += HalfBathrooms > 0 ? 0.5m : 0;
                    result += ThreeQuarterBathrooms;
                }
                return result;
            }
        }

        /// <summary>
        /// Only use this property for working with external partners. Internally, we want to treat Hotel, Suite, Studio, etc. units as having 0 bedrooms. For partners
        /// that can't handle our more detailed bedroom model, we will treat those as havnig 1 bedroom.
        /// </summary>
        public int NeverZeroBedroomCount
        {
            get
            {
                var bedroomCount = RoomType.FloorPlan.Bedrooms.BedroomCount;
                return bedroomCount == 0 ? 1 : bedroomCount;
            }
        }

        public PartialAddress Address { get; set; }

        public string BuildingName { get; set; }

        public string BuildingDescription { get; set; }

        public string UnitNumber { get; set; }

        public string UnitNumberDisplay
        {
            get
            {
                return string.IsNullOrEmpty(UnitNumber) ? string.Empty : string.Format("#{0}", UnitNumber);
            }
        }

        public long SupplierId { get { return Supplier.Id.Value; } }
        public ISupplier Supplier { get; set; }

        public RentalUnitType Type { get; set; }
        public CurrencyType CurrencyCode { get; set; }
        public int MerchandisingOrder { get; set; }
        public bool IsLuxury { get; set; }
        public bool IsFeaturedProperty { get; set; }
        public string GatewayPropertyId { get; set; }
        public bool IsAvailableForBooking { get; set; }
        public int Occupancy { get; set; }
        public long? StayRestrictionCalendarId { get; set; }
        public long? AvailabilityCalendarId { get; set; }
        public bool RatesRequireVerification { get; set; }
        public bool RequiresManualPricing { get; set; }
        public bool PrivateCalendar { get; set; }
        public long? NoteCollectionId { get; set; }
        public long? RateCalendarId { get; set; }
        public bool IsActive { get; set; }

        public string UnitDescription { get; set; }
        public string UnitDisclaimer { get; set; }
        public string TaxId { get; set; }
        public string PermitNumber { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public bool DisplayOnline { get; set; }

        public string Bedding { get; set; }


        public string FullName
        {
            get
            {
                //This logic is all duplicated in RentalUnitSearchResult.FullName
                string complexLevel = new List<string> { ComplexName, BuildingName }.JoinNonEmptyParts(" - ");
                string splurgeAmenities = RoomType.SplurgeAmenities.Select(x => "+ " + x).JoinNonEmptyParts(" ");
                string roomTypeLevel = new List<string> { RoomType.RoomTypeName, RoomType.SplurgeView, splurgeAmenities }.JoinNonEmptyParts(" ");

                string complexAndRoomTypeLevel = complexLevel + " - " + roomTypeLevel;

                string unitLevel = new List<string> { Bedding, UnitNumberDisplay }.JoinNonEmptyParts(" ");

                return new List<string> { complexAndRoomTypeLevel, unitLevel }.JoinNonEmptyParts(" ");
            }
        }

        public IEnumerable<string> TopTwoTags
        {
            get
            {
                int tagCount = 0;
                if (!string.IsNullOrWhiteSpace(Proximity))
                {
                    tagCount++;
                    yield return Proximity;
                }
                if (!string.IsNullOrWhiteSpace(RoomType.SplurgeView))
                {
                    tagCount++;
                    yield return RoomType.SplurgeView;
                }
                if (IsLuxury && tagCount < 2)
                {
                    tagCount++;
                    yield return "Luxury";
                }
                if (RoomType.Views.Count > 0 && tagCount < 2)
                {
                    yield return RoomType.Views[0];
                }
            }
        }

        #region BasicRentalUnit

        public long InventoryId { get; set; }

        #endregion

        private const string ItalicStyleTag = "<p class=\"disclaimer\">{0}</p>";
        private const string PermitNumberAndTaxIdTag = "<p class=\"disclaimer\">Permit Number: {0}. Tax Number: {1}.</p>";
        private const string PermitNumberTag = "<p class=\"disclaimer\">Permit Number: {0}.</p>";
        private const string TaxIdTag = "<p class=\"disclaimer\">Tax Number: {0}.</p>";
        private const string SimpleTag = "<p>{0}</p>";

        public string CombinedDescription
        {
            get
            {
                var result = new List<string>();
                if (!string.IsNullOrEmpty(ComplexDescription))
                    result.Add(string.Format(SimpleTag, ComplexDescription));
                if (!string.IsNullOrEmpty(BuildingDescription))
                    result.Add(string.Format(SimpleTag, BuildingDescription));
                if (!string.IsNullOrEmpty(UnitDescription))
                    result.Add(string.Format(SimpleTag, UnitDescription));
                if (!string.IsNullOrEmpty(UnitDisclaimer))
                    result.Add(string.Format(ItalicStyleTag, UnitDisclaimer));
                if (DisplayPhotoDisclaimer)
                    result.Add(string.Format(ItalicStyleTag, PhotoDisclaimer));
                if (!string.IsNullOrEmpty(TaxId) || !string.IsNullOrEmpty(PermitNumber))
                {
                    if (string.IsNullOrEmpty(TaxId))
                    {
                        result.Add(string.Format(PermitNumberTag, PermitNumber));
                    }
                    else if (string.IsNullOrEmpty(PermitNumber))
                    {
                        result.Add(string.Format(TaxIdTag, TaxId));
                    }
                    else
                    {
                        result.Add(string.Format(PermitNumberAndTaxIdTag, PermitNumber, TaxId));
                    }
                }
                return string.Join("", result);
            }
        }

        public bool DisplayPhotoDisclaimer { get; set; }
        public long CityId { get; set; }
        public long? IslandId { get; set; }
        public long DestinationId { get; set; }
        public long? NeighborhoodId { get; set; }
        public string NeighborhoodName { get; set; }
        public long? ResortGroupId { get; set; }
        public int? PropertyRating { get; set; }

        protected readonly LazyList<string> _primaryResortName = new LazyList<string>();
        public string PrimaryResortName
        {
            get { return _primaryResortName.List.FirstOrDefault(); }
        }

        protected readonly LazyList<long> _resortIds = new LazyList<long>();
        public List<long> ResortIds
        {
            get { return _resortIds.List; }
        }

        public IList<Hashtable> PersistedPrimaryResort
        {
            set { _primaryResortName.AddLazyLoad(() => value.Select(x => (string)x["ResortName"])); }
        }

        public IList<Hashtable> PersistedResortIds
        {
            set { _resortIds.AddLazyLoad(() => value.Select(x => (long)x["ResortId"])); }
        }
    }
}