﻿namespace ElasticSearchAPI.Models
{
    public class DimensionRefinement
    {
        public long NavId { get; set; }
        public string Name { get; set; }
        public string Count { get; set; }
        public string UrlName { get; set; }
        public string SelectionPath { get; set; }
        public EndecaDimension Dimension { get; set; }
    }
}