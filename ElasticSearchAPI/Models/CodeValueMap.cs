﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ElasticSearchAPI.Models
{
    public class CodeValueMap<T> where T : CodeValue<T>
    {
        private readonly IDictionary<string, T> _instancesByCode = new Dictionary<string, T>();
        private List<T> _list;
        private ReadOnlyCollection<T> _listExcludingBlank;
        private readonly object _lockObject = new object();

        public ReadOnlyCollection<T> List
        {
            get
            {
                if (_list == null)
                {
                    lock (_lockObject)
                    {
                        _list = _instancesByCode.Values.ToList();
                    }
                }
                return _list.AsReadOnly();
            }
        }

        public ReadOnlyCollection<T> ListExcludingBlank
        {
            get
            {
                if (_listExcludingBlank == null)
                {
                    lock (_lockObject)
                    {
                        var excludingBlank = _instancesByCode.Values.Where(x => !x.IsBlank).ToList();
                        _listExcludingBlank = excludingBlank.AsReadOnly();
                    }
                }
                return _listExcludingBlank;
            }
        }

        public ReadOnlyCollection<T> ListExcludingValues(IList values)
        {
            {
                List<T> excludingValues = new List<T>();
                lock (_lockObject)
                {
                    foreach (KeyValuePair<string, T> keyValuePair in _instancesByCode)
                    {
                        if (!values.Contains(keyValuePair.Value))
                        {
                            excludingValues.Add(keyValuePair.Value);
                        }
                    }
                }
                return excludingValues.AsReadOnly();
            }
        }

        internal void Add(T codeValue)
        {
            lock (_lockObject)
            {
                if ((codeValue.Code != null) && (!_instancesByCode.ContainsKey(codeValue.Code.ToLowerInvariant())))
                {
                    _list = null; //regenerate lists the next time they are retrieved
                    _listExcludingBlank = null;
                    _instancesByCode.Add(codeValue.Code.ToLowerInvariant(), codeValue);
                }
            }
        }

        public T Lookup(string code)
        {
            if (code == null)
            {
                return null;
            }
            lock (_lockObject)
            {
                T returnValue;
                return _instancesByCode.TryGetValue(code.ToLowerInvariant(), out returnValue) ? returnValue : null;
            }
        }
    }
}