﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public sealed class SoftwareType : CodeValue<SoftwareType>
    {
        private readonly string _shortCode;

        public static class Codes
        {
            public const string AVMAIN = "AVMAIN";
            public const string ENTECH = "ENTECH";
            public const string PPLUS = "PPLUS";
            public const string RQIRSS = "RQIRSS";
            public const string V12 = "V12";
            public const string UNKNOWN = "UNKNOWN";
        }

        public readonly static SoftwareType AVMain = new SoftwareType(Codes.AVMAIN, "AVMAIN", "A");
        public readonly static SoftwareType Entech = new SoftwareType(Codes.ENTECH, "ENTECH", "E");
        public readonly static SoftwareType PropertyPlus = new SoftwareType(Codes.PPLUS, "PPLUS", "P");
        public readonly static SoftwareType RQI_RSS = new SoftwareType(Codes.RQIRSS, "RQI-RSS", "Q");
        public readonly static SoftwareType V12 = new SoftwareType(Codes.V12, "V12", "V");
        public readonly static SoftwareType Unknown = new SoftwareType(Codes.UNKNOWN, "UNKNOWN", null);

        private SoftwareType(string code, string description, string shortCode) : base(code, description)
        {
            _shortCode = shortCode;
        }

        public string ShortCode
        {
            get { return _shortCode; }
        }

        public static SoftwareType LookupByShortCode(string shortCode)
        {
            switch (shortCode)
            {
                case "A":
                    return AVMain;
                case "E":
                    return Entech;
                case "P":
                    return PropertyPlus;
                case "Q":
                    return RQI_RSS;
                case "V":
                    return V12;
            }
            return Unknown;
        }
    }
}