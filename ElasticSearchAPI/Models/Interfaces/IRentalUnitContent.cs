﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IRentalUnitContent : IEntity, IBasicRentalUnit, IPersistedRentalUnitWithRoomType
    {
        InventoryMode InventoryMode { get; set; }
        string ComplexName { get; set; }
        string ComplexDescription { get; set; }
        GeoLocation GeoLocation { get; set; }
        BuildingType BuildingType { get; set; }
        new IRoomType RoomType { get; set; }
        PartialAddress Address { get; set; }
        string Proximity { get; set; }
        string FullName { get; }
        string BuildingName { get; set; }
        string BuildingDescription { get; set; }
        string UnitNumber { get; set; }
        long SupplierId { get; }
        ISupplier Supplier { get; set; }
        RentalUnitType Type { get; set; }
        CurrencyType CurrencyCode { get; set; }
        int MerchandisingOrder { get; set; }
        bool IsLuxury { get; set; }
        bool IsFeaturedProperty { get; set; }
        string GatewayPropertyId { get; set; }
        bool IsAvailableForBooking { get; set; }
        int Occupancy { get; set; }
        long? StayRestrictionCalendarId { get; set; }
        long? AvailabilityCalendarId { get; set; }
        bool IsActive { get; set; }

        decimal BathroomCount { get; }
        int NeverZeroBedroomCount { get; }

        string UnitDescription { get; set; }
        string UnitDisclaimer { get; set; }
        string CombinedDescription { get; }

        bool IsBathroomPrivate { get; set; }
        int FullBathrooms { get; set; }
        int ThreeQuarterBathrooms { get; set; }
        int HalfBathrooms { get; set; }
        string BathsDisplay { get; }


        long CityId { get; set; }
        //        string LocationText { get; }
        long? NeighborhoodId { get; set; }
        string NeighborhoodName { get; set; }
        long? ResortGroupId { get; set; }
        string Bedding { get; set; }
        string UnitNumberDisplay { get; }
        bool DisplayPhotoDisclaimer { get; set; }
        IEnumerable<string> TopTwoTags { get; }
        int ImageCount { get; set; }
        string TaxId { get; set; }
        string PermitNumber { get; set; }

        bool RatesRequireVerification { get; set; }
        bool RequiresManualPricing { get; set; }
        bool DisplayOnline { get; set; }
        bool PrivateCalendar { get; set; }
        long? NoteCollectionId { get; set; }
        long? RateCalendarId { get; set; }
        long DestinationId { get; set; }
        List<long> ResortIds { get; }
        long? IslandId { get; set; }
        string PrimaryResortName { get; }
        string CheckInTime { get; set; }
        string CheckOutTime { get; set; }
        int? PropertyRating { get; set; }
    }
}
