﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface ISupplier
    {
        bool IsActive { get; set; }
        long? Id { get; set; }
        string GatewaySupplierId { get; set; }
        GatewayProviderType GatewayProviderType { get; set; }
        string Name { get; set; }
        string PhoneNumber { get; set; }
        string PhoneNumberToDisplayToAgent { get; }
        string ReservationPhoneNumber { get; set; }
        string FaxNumber { get; set; }
        string Email { get; set; }
        string URL { get; set; }
        PhoneNumber AfterHoursPhone { get; set; }
        string EscalationProcedure { get; set; }
        string ContractAgreement { get; set; }
        string RentalAgreement { get; set; }
        string PropertyDisclaimer { get; set; }
        bool IsActiveInGateway { get; set; }

        PartialAddress Address { get; set; }
        string Address1 { get; }
        string Address2 { get; }
        string City { get; }
        string State { get; }
        string PostalCode { get; }
        string Country { get; }

        PartialAddress BillingAddress { get; set; }
        string BillingAddress1 { get; }
        string BillingAddress2 { get; }
        string BillingCity { get; }
        string BillingState { get; }
        string BillingCountry { get; }
        string BillingPostalCode { get; }

        long? PrimaryDestinationId { get; set; }
        string PrimaryCountryCode { get; set; }
        string PrimaryISOStateCode { get; set; }

        int NumberOfUnits { get; set; }
        string CCAccountNumber { get; set; }
        string CCAccountTag { get; set; }
        SoftwareType SoftwareType { get; set; }
        string PromoCode { get; set; }
        bool IsAvailableForBooking { get; set; }
        bool SupplierProvidesRackRates { get; set; }
        string LinkUrl { get; }
        bool IsManualGateway { get; }
        bool IsSecurityDepositProtectionRequired { get; set; }
        int? Tier { get; set; }
        bool DisplayGatewayNameWithConfirmationNumber { get; set; }
        bool RetailerManagesInventory { get; set; }
        BookWithSupplierType RetailerBooksWithSupplierType { get; set; }
        bool AreExtranetNotificationsEnabled { get; }
        ReadOnlyCollection<ExtranetNotification> ExtranetNotificationEmailAddresses { get; }
        List<long> RateCalendarTemplateIds { get; }
        List<long> FeeCalendarTemplateIds { get; }
        long? NoteCollectionId { get; set; }
        void AddExtranetNotificationEmailAddresses(ExtranetNotification extranetNotificationEmailAddresses);
        void RemoveExtranetNotificationEmailAddresses(ExtranetNotification extranetNotificationEmailAddresses);
        void ClearExtranetNotificationEmailAddresses();
        void DeactivateInGateway(IEventAggregator eventAggregator);
        void ActivateInGateway(IEventAggregator eventAggregator);
        void AddFeeCalendarTemplateId(long feeCalendarTemplateId);
        void EnableExtranetNotifications(IEventAggregator eventAggregator);
        void DisableExtranetNotifications(IEventAggregator eventAggregator);
        string ContactGroupIdentifier { get; set; }
        bool HasCustomTaxesAndFees { get; set; }
    }
}