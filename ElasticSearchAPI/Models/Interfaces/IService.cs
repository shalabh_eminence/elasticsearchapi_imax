﻿namespace ElasticSearchAPI.Models.Interfaces
{
    /// <summary>
    /// A service should provide some functionality seperate from creating (see IFactory) or persisting (IRepository) objects.
    /// A service should not have state and shouldn't do something that belongs in an entity or value object.
    /// </summary>
    public interface IService
    {

    }
}