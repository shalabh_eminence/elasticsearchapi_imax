﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface ICheckInDateSpecification
    {
        bool IsCheckInAllowed(DateTime checkIn);
        DateTime EarliestAllowedCheckIn { get; }
    }
}
