﻿namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IEventAggregator : IService
    {
        void Raise<T>(T args) where T : IDomainEvent;
    }
}