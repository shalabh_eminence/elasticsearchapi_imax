﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAPI.Models.Interfaces
{
    /// <summary>
    /// A value object should be immutable (cannot be changed after constructor is called) and equatable with other value objects of the same type.
    /// Value objects should override Equals, GetHashCode, equality operators, and ToString
    /// </summary>
    public interface IValueObject
    {

    }
}
