﻿namespace ElasticSearchAPI.Models.Interfaces
{
    /// <summary>
    /// An entity should have an identifying property, such as id. Entities should not be equatable to other entities. 
    /// Entities should only be accessible through their Aggregate Root.
    /// </summary>
    public interface IEntity
    {

    }
}