﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IBrandedPath
    {
        string BaseDomain { get; }
        string PageKey { get; }
        ReadOnlyCollection<string> PathParts { get; }
        IBrandedPath GetPathWithoutSearch();
    }
}
