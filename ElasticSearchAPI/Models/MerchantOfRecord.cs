﻿namespace ElasticSearchAPI.Models
{
    public class MerchantOfRecord : CodeValue<MerchantOfRecord>
    {
        public static class Codes
        {
            public const string RETAILER = "RETAILER";
            public const string SUPPLIER = "SUPPLIER";
        }

        /// <summary>
        /// Retailer means that the retailer collects the money from the customer - previously this was referred to as MOR
        /// </summary>
        public static readonly MerchantOfRecord Retailer = new MerchantOfRecord(Codes.RETAILER, "Retailer");

        /// <summary>
        /// Supplier means that the supplier collects the money from the customer - previously this was referred to as NonMOR
        /// </summary>
        public static readonly MerchantOfRecord Supplier = new MerchantOfRecord(Codes.SUPPLIER, "Supplier");

        private MerchantOfRecord(string code, string description) : base(code, description)
        {
        }
    }
}