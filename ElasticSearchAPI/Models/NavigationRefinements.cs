﻿namespace ElasticSearchAPI.Models
{
    public class NavigationRefinements
    {
        public NavigationRefinements()
        {
            CurrentRefinements = new DimensionList();
            RefinementDimensions = new DimensionList();
        }

        public DimensionList CurrentRefinements { get; set; }
        public DimensionList RefinementDimensions { get; set; }
    }
}