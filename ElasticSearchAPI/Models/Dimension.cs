﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ElasticSearchAPI.Models
{
    public class Dimension
    {
        public long DimensionId { get; set; }

        public string DimensionName { get; set; }

        private readonly List<DimensionRefinement> _refinements = new List<DimensionRefinement>();
        public ReadOnlyCollection<DimensionRefinement> Refinements { get { return _refinements.AsReadOnly(); } }

        public void AddRefinement(DimensionRefinement refinement)
        {
            _refinements.Add(refinement);
        }

        public Dimension WithId(long dimensionId)
        {
            DimensionId = dimensionId;
            return this;
        }

        public Dimension WithName(string name)
        {
            DimensionName = name;
            return this;
        }

        public Dimension WithRefinement(DimensionRefinement refinement)
        {
            AddRefinement(refinement);
            return this;
        }

        public Dimension WithRefinements(IEnumerable<DimensionRefinement> refinements)
        {
            foreach (DimensionRefinement refinement in refinements)
            {
                AddRefinement(refinement);
            }

            return this;
        }
    }
}