﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class EndecaQuerySpecification
    {
        public int MaxReturn { get; set; }

        public int RecordOffset { get; set; }

        public ReadOnlyCollection<SortKey> SortKeys { get; }

        public EndecaRecordFilterBase RecordFilter { get; set; }

        public TravelDates TravelDates { get; set; }

        public string SearchTerm { get; }
        public string SearchKey { get; }
        public bool HasSearch { get; }

        public ReadOnlyCollection<EndecaDimension> ExposedRefinementDimensions { get; }

        public ReadOnlyCollection<EndecaDimensionValue> NavigationDimensionValues { get; }

        public ReadOnlyCollection<long> NavigationDimensionValueIds { get; }
    }
}