﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ElasticSearchAPI.Models
{
    public class DimensionList : List<Dimension>
    {
        public DimensionList()
        {

        }

        public DimensionList(IEnumerable<Dimension> dimensions)
            : base(dimensions)
        {

        }

        public IEnumerable<Dimension> GetDimensions(EndecaDimension dimension)
        {
            return this.Where(x => x.DimensionId == dimension.DimensionId);
        }

        public Dimension GetDimension(EndecaDimension dimension)
        {
            return this.FirstOrDefault(x => x.DimensionId == dimension.DimensionId);
        }

        public ReadOnlyCollection<DimensionRefinement> GetRefinementsForDimension(EndecaDimension dimension)
        {
            IEnumerable<Dimension> dim = GetDimensions(dimension);

            List<DimensionRefinement> refinements = new List<DimensionRefinement>();

            foreach (var dimension1 in dim)
            {
                refinements.AddRange(dimension1.Refinements);
            }

            return refinements.AsReadOnly();

        }

        public IEnumerable<DimensionRefinement> GetAllRefinements()
        {
            return this.SelectMany(x => x.Refinements).OrderBy(x => x.Name);
        }
    }
}