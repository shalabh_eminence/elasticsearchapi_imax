﻿using ElasticSearchAPI.Models;
using ElasticSearchCore.Core.DataAccess.Impl;
using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ElasticSearchAPI.Controllers
{
    public class DatabaseController : ApiController
    {
        // POST: api/Database
        public void Post([FromBody]EndecaQueryContainer container)
        {
            string baseQuery = container.BaseQuery;
            string dimensions = "";// container.Dimensions;
            string searchPhrase = container.SearchPhrase;
            int maxReturns = container.MaxReturns;
            int page = container.RecordOffset;

            string status = "";
            string luxury = "";
            string businessUnit = "";

            List<string> countries = new List<string>();
            List<string> destinations = new List<string>();
            List<string> destinationTypeCategories = new List<string>();
            List<string> resorts = new List<string>();
            List<string> propertyTypes = new List<string>();
            List<string> bedrooms = new List<string>();
            List<string> cities = new List<string>();

            bool containsNot = baseQuery.Contains("NOT(");

            string[] delimiters = new string[] { "AND(", "OR(", "NOT(" };
            baseQuery = baseQuery.Replace("{", "").Replace("}", "");
            baseQuery = baseQuery.Replace(")", "");

            string[] s = baseQuery.Split(delimiters, StringSplitOptions.None);

            foreach (string x in s)
            {
                string[] entries = x.Split(',');

                foreach (string e in entries)
                {
                    string[] terms = e.Split(':');

                    if (terms.Length > 1)
                    {
                        switch (terms[0].ToLower())
                        {
                            case "status":
                                status = terms[1];
                                break;
                            case "luxury":
                                luxury = terms[1];
                                break;
                            case "bedrooms":
                                if (!bedrooms.Contains(terms[1]))
                                    bedrooms.Add(terms[1]);
                                break;
                            case "city":
                                if (!cities.Contains(terms[1]))
                                    cities.Add(terms[1]);
                                break;
                            case "country":
                                if (!countries.Contains(terms[1]))
                                    countries.Add(terms[1]);
                                break;
                            case "destination":
                                if (!destinations.Contains(terms[1]))
                                    destinations.Add(terms[1]);
                                break;
                            case "destinationtypecategory":
                                if (!destinationTypeCategories.Contains(terms[1]))
                                    destinationTypeCategories.Add(terms[1]);
                                break;
                            case "resort":
                                if (!resorts.Contains(terms[1]))
                                    resorts.Add(terms[1]);
                                break;
                            case "property type":
                                if (!propertyTypes.Contains(terms[1]))
                                    propertyTypes.Add(terms[1]);
                                break;
                            case "businessunit":
                                    businessUnit = terms[1];
                                break;
                        }
                    }
                }
            }
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //List<long> baseQueryInventoryIDs = GetBaseQueryInventoryIDs(status, luxury, countries, destinations, destinationTypeCategories, resorts, propertyTypes, businessUnit, bedrooms, cities);
            Tuple<List<long>, List<long>> tuples = GetBaseQueryInventoryIDs(status, luxury, countries, destinations, destinationTypeCategories, resorts, propertyTypes, businessUnit, bedrooms, cities);

            List<long> baseQueryInventoryIDs = tuples.Item1;
            List<long> baseQueryFeaturedInventoryIDs = tuples.Item2;

            List<PropertyInfo> propertyInfos = new List<PropertyInfo>();
            List<PropertyInfo> featuredProperties = new List<PropertyInfo>();

            Task[] tasks = new Task[2];

            Task t1 = Task.Run(() => FillInventoryIDs(baseQueryInventoryIDs.GetRange(page * maxReturns, maxReturns), propertyInfos));
            Task t2 = Task.Run(() => FillInventoryIDs(baseQueryFeaturedInventoryIDs, featuredProperties));
            tasks[0] = t1;
            tasks[1] = t2;

            Task.WaitAll(tasks);

            sw.Stop();
            Debug.WriteLine("base query" + sw.ElapsedMilliseconds);
        }

        private Tuple<List<long>, List<long>> GetBaseQueryInventoryIDs(string status, string luxury, List<string> countries, List<string> destinations, List<string> destinationTypeCategories,
            List<string> resorts, List<string> propertyTypes, string businessUnit, List<string> bedrooms, List<string> cities)
        {
            //[From GeographyView]
            //get by country ✔
            //get bt destinations ✔
            //get by destinationcategories ✔
            

            List<long> inventoriesByCountry = new List<long>();
            List<long> inventoriesByCountryFeatured = new List<long>();
            //List<long> inventoriesByDestination = new List<long>();
            //List<long> inventoriesByDestinationCategory = new List<long>();
            //List<long> inventoriesByResorts = new List<long>();
            //List<long> inventoriesByBedrooms = new List<long>();
            //List<long> inventoriesByCities = new List<long>();

            //Debug.WriteLine(sw.ElapsedMilliseconds);
            EndecaPropertyViewRepository endecaPropertyViewRepository = new EndecaPropertyViewRepository();

            if(countries.Count > 0)
            {
                Tuple<List<long>,List<long>> countryTuples = endecaPropertyViewRepository.GetPropertyInventoryIDsByCountry(status, luxury, countries, businessUnit);

                return countryTuples;
                inventoriesByCountry = countryTuples.Item1;
                inventoriesByCountryFeatured = countryTuples.Item2;
            }

            return null;
                
            //if(destinations.Count > 0)
            //    inventoriesByDestination = endecaPropertyViewRepository.GetPropertyInventoryIDsByDestinations(status, luxury, destinations, businessUnit);
            //if(destinationTypeCategories.Count > 0)
            //    inventoriesByDestinationCategory = endecaPropertyViewRepository.GetPropertyInventoryIDsByDestinationTypes(status, luxury, destinations, businessUnit);

            ////[from RentalUnitResorts]
            ////get properties by resorts ✔
            //if (resorts.Count > 0)
            //{
            //    EndecaResortViewRepository endecaResortViewRepository = new EndecaResortViewRepository();
            //    inventoriesByResorts = endecaResortViewRepository.GetInventoryIDsByResortName(resorts);
            //}

            ////propertyType
            //List<BuildingType> buildingTypes = new List<BuildingType>();
            //foreach (string buildingType in propertyTypes)
            //{
            //    buildingTypes.Add(BuildingType.LookupByDescription(buildingType));
            //}

            ////status
            //    //dealt with up top

            ////[From BusinessUnits]
            ////businessUnits ✔
            ////EndecaBusinessUnitRepository endecaBusinessUnitRepository = new EndecaBusinessUnitRepository();
            ////List<long> inventoriesByBusinessUnit = endecaBusinessUnitRepository.GetInventoryIDsByBusinessUnitName(businessUnits);

            ////[From PropertyView]
            ////bedrooms  ✔
            //if(bedrooms.Count > 0)
            //    inventoriesByBedrooms = endecaPropertyViewRepository.GetInventoryIDsByBeds(bedrooms);

            ////[From RentalUnitContent]
            ////get properties by cities ✔
            //if (cities.Count > 0)
            //{
            //    RentalUnitContentViewRepository rentalUnitContentViewRepository = new RentalUnitContentViewRepository();
            //    inventoriesByCities = rentalUnitContentViewRepository.GetInventoryIDsByCity(cities);
            //}

            ////join all together, except for countries and propertyTypes and businessUnits
            //List<long> orList = new List<long>();
            //orList.Concat(inventoriesByDestination);
            //orList.Concat(inventoriesByDestinationCategory);
            //orList.Concat(inventoriesByResorts);
            //orList.Concat(inventoriesByBedrooms);
            //orList.Concat(inventoriesByCities);

            //orList = orList.Select(x => x).Distinct().ToList();

            /*List<long> returnList = new List<long>();

            if (inventoriesByCountry.Count > 0)
            {
                return inventoriesByCountry;
                //List<long> andCountry = new List<long>();

                //foreach(long countryInventoryId in inventoriesByCountry)
                //{
                //    andCountry.AddRange((from x in orList
                //                         where x == countryInventoryId
                //                         select x).ToList());
                //}

                //inventoriesByCountry.AddRange(andCountry);
                //List<long> andCountryUniqueCountries = inventoriesByCountry.Distinct().ToList();

                ////if (businessUnits.Count() > 0)
                ////{
                ////    List<long> andBusinessUnits = new List<long>();

                ////    foreach (long busUnitInventoryID in inventoriesByBusinessUnit)
                ////    {
                ////        andBusinessUnits.AddRange((from x in andCountryUniqueCountries
                ////                                   where x == busUnitInventoryID
                ////                                   select x).ToList());
                ////    }

                ////    returnList = andBusinessUnits;
                ////    //return andBusinessUnits;
                ////}

                //returnList = andCountry;
                ////return andCountry;
            }
            else
            {
                ////if there are business units, AND them with the others
                //if(businessUnits.Count() > 0)
                //{
                //    List<long> andBusinessUnits = new List<long>();

                //    foreach(long busUnitInventoryID in inventoriesByBusinessUnit)
                //    {
                //        andBusinessUnits.AddRange((from x in orList
                //                                   where x == busUnitInventoryID
                //                                   select x).ToList());
                //    }

                //    returnList = andBusinessUnits;
                //    //return andBusinessUnits;
                //}

                returnList = orList;
                //return orList;
            }

            return returnList;*/
        }

        private void FillInventoryIDs(List<long> inventoryIDs, List<PropertyInfo> infos)
        {
            EndecaPropertyViewRepository endecaPropertyViewRepository = new EndecaPropertyViewRepository();
            EndecaGeographyViewRepository endecaGeographyViewRepository = new EndecaGeographyViewRepository();
            RentalUnitContentViewRepository rentalUnitContentViewRepository = new RentalUnitContentViewRepository();

            Task[] tasks = new Task[inventoryIDs.Count];
            int i = 0;

            foreach (long inventoryID in inventoryIDs)
            {
                Task t = Task.Run(() =>
                { 
                EndecaPropertyView pv = endecaPropertyViewRepository.GetPropertyByInventoryID(inventoryID);

                List<EndecaGeographyView> gvs = endecaGeographyViewRepository.GetGeographyByInventoryID(inventoryID);
                EndecaGeographyView gv = gvs.Count > 0 ? gvs[0] : new EndecaGeographyView();

                List<RentalUnitContentView> rus = rentalUnitContentViewRepository.GetRentalContentViewByInventoryID(inventoryID);
                RentalUnitContentView ru = rus.Count > 0 ? rus[0] : new RentalUnitContentView();

                PropertyInfo pi = new PropertyInfo();
                pi.spec = pv.InventoryID.ToString();
                pi.RentalUnitId = (int)ru.RentalUnitId;
                pi.InventoryId = pv.InventoryID;
                pi.PromoText = "";
                pi.Description = pv.InventoryDescription;

                pi.ImageCount = ru.ImageCount;
                pi.Sleeps = pv.Sleeps == null ? 0 : (int)pv.Sleeps;
                pi.VirtualTourUrl = pv.VirtualTourURL;
                pi.ExpertReview = pv.ExpertReview;
                pi.EmailBooking = pv.EmailBooking.ToString();
                pi.PropertyDisclaimer = pv.PropertyDisclaimer;
                pi.Policies = pv.Policies;

                pi.IsLuxury = pv.IsLuxury;
                pi.Address1 = ru.Address1;
                pi.Address2 = ru.Address2;
                pi.City = gv.CityName;
                pi.State = gv.StateName;
                pi.StateCode = gv.StateCode;
                pi.PostalCode = ru.PostalCode;
                pi.Country = gv.Country;
                pi.CountryCode = gv.CountryCode;
                pi.Latitude = (float?)pv.Lat;
                pi.Longitude = (float?)pv.Long;
                pi.Island = gv.IslandName;
                pi.Status = pv.Status;
                pi.FromRate = 4000; //////////////
                pi.SpecialFromRate = 0;/////////
                pi.FromRateEndDate = DateTime.Parse("2016-08-14T00:00:00"); ///////
                pi.FromRateStartDate = DateTime.Parse("2016 - 08 - 18T00: 00:00"); ///////////
                pi.CurrencyType = pv.CurrencyType;
                pi.SpecialDescription = ""; ////
                pi.SpecialStartDate = null;////
                pi.SpecialEndDate = null;////
                pi.SpecialBookByDate = null;////////
                pi.SpecialExclusions = null;////
                pi.SpecialTermsAndConditions = null;////

                //VacationRoost.Domain.GatewayProviderType x = new VacationRoost.Domain.GatewayProviderType;

                pi.GatewayProviderType = null;// VacationRoost.Domain.GatewayProviderType;
                string propRating = pv.PropertyRating.ToString();
                pi.PropertyRating = (propRating == string.Empty || propRating == null) ? null : (float?)float.Parse(propRating);

                infos.Add(pi);
                });

                tasks[i] = t;
                i++;
            }

            Task.WaitAll(tasks);
        }

        private void MakeRefinements(List<long> inventoryIDs)
        {

        }
    }
}
