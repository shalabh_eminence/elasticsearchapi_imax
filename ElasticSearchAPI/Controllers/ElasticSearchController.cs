﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearch.Models.Responses.RentalUnitContent;
using ElasticSearchAPI.Models;
using ElasticSearchAPI.Models.ControllerHelpers;
using ElasticSearchAPI.Models.ReturnToClient;
using ElasticSearchCore.API.ElasticQueries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;

namespace ElasticSearchAPI.Controllers
{
    public class ElasticSearchController : ApiController
    {
        // POST: api/ElasticSearch
        public ReturnToLegoLand Post([FromBody]EndecaQueryContainer container)
        {
            //Extract the info we need, from what the client posts
            string baseQuery = container.BaseQuery;
            SelectedDimension[] selectedDimensions = container.Dimensions;
            string searchPhrase = container.SearchPhrase;
            int maxReturns = container.MaxReturns;
            int recordOffset = container.RecordOffset;
            SortKey2 sortKey =  container.SortKey;

            string status = "";
            string luxury = "";

            //Lists for holding base query detail
            List<string> countries = new List<string>();
            List<string> destinations = new List<string>();
            List<string> destinationTypeCategories = new List<string>();
            List<string> resorts = new List<string>();
            List<string> propertyTypes = new List<string>();
            List<string> businessUnits = new List<string>();
            List<string> bedrooms = new List<string>();
            List<string> cities = new List<string>();
            List<string> neighborhoods = new List<string>();
            List<string> states = new List<string>();

            Helpers helper = new Helpers();

            bool containsNot = baseQuery.Contains("NOT(");

            string[] delimiters = new string[] { "AND(", "OR(", "NOT(" };
            baseQuery = baseQuery.Replace("{", "").Replace("}", "");
            baseQuery = baseQuery.Replace(")", "");

            //get the specifications for the base query
            string[] s = baseQuery.Split(delimiters, StringSplitOptions.None);

            //parse and categories the pieces for the base query
            foreach (string x in s)
            {
                string[] entries = x.Split(',');

                foreach (string e in entries)
                {
                    string[] terms = e.Split(':');

                    if (terms.Length > 1)
                    {
                        switch (terms[0].ToLower())
                        {
                            case "status":
                                status = terms[1];
                                break;
                            case "isluxury":
                                luxury = terms[1];
                                break;
                            case "bedrooms":
                                if (!bedrooms.Contains(terms[1]))
                                    bedrooms.Add(terms[1]);
                                break;
                            case "city":
                                if (!cities.Contains(terms[1]))
                                    cities.Add(terms[1]);
                                break;
                            case "country":
                                if (!countries.Contains(terms[1]))
                                    countries.Add(terms[1]);
                                break;
                            case "destination":
                                if (!destinations.Contains(terms[1]))
                                    destinations.Add(terms[1]);
                                break;
                            case "destinationtypecategory":
                                if (!destinationTypeCategories.Contains(terms[1]))
                                    destinationTypeCategories.Add(terms[1]);
                                break;
                            case "resort":
                                if (!resorts.Contains(terms[1]))
                                    resorts.Add(terms[1]);
                                break;
                            case "property type":
                                if (!propertyTypes.Contains(terms[1]))
                                    propertyTypes.Add(terms[1]);
                                break;
                            case "businessunit":
                                if (!businessUnits.Contains(terms[1]))
                                    businessUnits.Add(terms[1]);
                                break;
                            case "neighborhood":
                                if (!neighborhoods.Contains(terms[1]))
                                    neighborhoods.Add(terms[1]);
                                break;
                            case "state":
                                if (!states.Contains(terms[1]))
                                    states.Add(terms[1]);
                                break;
                        }
                    }
                }
            }

            //Get the base query results
            Tuple<List<PropertyHit>, List<PropertyHit>> baseQueryProperties = GetBaseQueryResults(status, luxury, countries, destinations, destinationTypeCategories, resorts, propertyTypes, businessUnits, bedrooms, cities, neighborhoods, states);

            //set up the sorted properties
            List<PropertyHit> sortedProperties = baseQueryProperties.Item1;
            List<PropertyHit> sortedFeatures = baseQueryProperties.Item2;

            //if there is text from a search
            if (searchPhrase != string.Empty && searchPhrase != "=")
            {
                PropertyQuery propertyQuery = new PropertyQuery();
                List<PropertyInfo> pi = new List<PropertyInfo>();
                List<PropertyInfo> featured = new List<PropertyInfo>();
                List<PropertyHit> propHit = new List<PropertyHit>();
                int value = 0;

                ElasticPropertySearchResults psr = null;

                //search by ID
                if (int.TryParse(searchPhrase, out value))
                {
                    //get the list of inventoryIDs from the base query
                    List<long> inventoryIDs = sortedProperties.Select(x => x._source.InventoryID).ToList();

                    //get rental unit contents for given inventoryIDs
                    RentalUnitContentQuery rentalUnitContentQuery = new RentalUnitContentQuery();
                    RentalUnitContentHit[] hits = rentalUnitContentQuery.GetRentalUnitContentByInventoryIDs(inventoryIDs).hits.hits;

                    RentalUnitContentHit hit = (from x in hits
                                                 where x._source.RentalUnitId == (long)value
                                                 select x).FirstOrDefault();

                    if (hit != null)
                    {
                        //get the property Hit that matches a rentalUnitID
                        propHit = (from x in sortedProperties
                                   where x._source.InventoryID == hit._source.InventoryId
                                   select x).ToList();

                        //fill the information
                        helper.FillPropertyInfos(propHit, pi);
                    }
                }
                else //search for property name
                {
                    psr = propertyQuery.GetPropertyByPartialName(searchPhrase);
                    PropertyHit[] hits = psr.hits.hits;

                    //make sure only inventories from the base query 
                    propHit = (from x in sortedProperties
                            join y in hits on
                            x._source.InventoryID equals y._source.InventoryID
                            select y).ToList();

                    helper.FillPropertyInfos(propHit.ToList(), pi);
                }

                //List<RefinementDimensions>

                //Construct the object to be returned to the client
                ReturnToLegoLand returnToLL = new ReturnToLegoLand()
                {
                    featuredProperties = featured.ToList(),
                    propertyInfos = pi.ToList(),
                    refinementDimensions = helper.GetRefinements(propHit),
                    currentDimensions = new List<RefinementDimensions>(),
                    TotalRecords = pi.Count
                };

                return returnToLL;
            }

            if (selectedDimensions != null)
            {
                if(selectedDimensions.Count() > 0)
                    sortedProperties = helper.FilterByRefinements(selectedDimensions, sortedProperties);
            }

            //Add a numerical rate to the base query results, for sorting
            helper.FillInRatesAndMerchOrder(sortedProperties);
            helper.FillInRatesAndMerchOrder(sortedFeatures);

            //sort the properties
            switch (sortKey.Key)
            {
                case "p_FromRateLowHigh":
                        sortedProperties = sortedProperties.OrderBy(x => x.rate).ToList();
                    break;
                case "p_FromRateHighLow":
                        sortedProperties = sortedProperties.OrderByDescending(x => x.rate).ToList();
                    break;
                case "p_bedroomsHighLow":
                        sortedProperties = sortedProperties.OrderByDescending(x => x._source.Sleeps).ToList();
                    break;
                case "p_bedroomsLowHigh":
                        sortedProperties = sortedProperties.OrderBy(x => x._source.Sleeps).ToList();
                    break;
                case "p_bedrooms":
                    if(sortKey.IsDescending)
                    {
                        sortedProperties = sortedProperties.OrderByDescending(x => x._source.Bedroom).ToList();
                    }
                    else
                    {
                        sortedProperties = sortedProperties.OrderBy(x => x._source.Bedroom).ToList();
                    }
                    break;
                default:
                    //sort by merchandising order
                    sortedProperties = sortedProperties.OrderBy(x => x.merchandiceOrder).ToList();
                    break;
            }

            //Get the list of refinements from given properties
            List<RefinementDimensions> refinements = helper.GetRefinements(sortedProperties);
            List<RefinementDimensions> currentRefinements = new List<RefinementDimensions>();

            //clear out list, but keep structure
            foreach (RefinementDimensions rd in refinements)
            {
                RefinementDimensions x = new RefinementDimensions()
                {
                    id = rd.id,
                    name = rd.name,
                    refinements = new List<Refinement>()
                };

                currentRefinements.Add(x);
            }

            //Remove any refinemtns that have been selected
            if (selectedDimensions != null)
            {
                foreach (SelectedDimension sd in selectedDimensions)
                {
                    string key = sd.name.Replace("--", " ").Replace("*", " "); ;

                    if (sd.searchUrl.ToLower().Contains("destination_or"))
                    {
                        string dest = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("or_destination")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {
                            Refinement r = (from x in rd.refinements
                                            where x.name.ToLower().Equals(dest.ToLower())
                                            select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if(t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                               where x.name.ToLower().Equals("or_destination")
                                                               select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }

                    if(sd.searchUrl.ToLower().Contains("city_or"))
                    {
                        string city = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("or_city")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {
                            Refinement r = (from x in rd.refinements
                                            where x.name.ToLower().Equals(city.ToLower())
                                            select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if (t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                                   where x.name.ToLower().Equals("or_city")
                                                                   select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }

                    if (sd.searchUrl.ToLower().Contains("state_or"))
                    {
                        string state = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("or_state")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {
                            Refinement r = (from x in rd.refinements
                                            where x.name.ToLower().Equals(state.ToLower())
                                            select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if (t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                                   where x.name.ToLower().Equals("or_state")
                                                                   select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }

                    if (sd.searchUrl.ToLower().Contains("neighborhood_or"))
                    {
                        string state = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("or_neighborhood")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {
                            Refinement r = (from x in rd.refinements
                                            where x.name.ToLower().Equals(state.ToLower())
                                            select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if (t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                                   where x.name.ToLower().Equals("or_neighborhood")
                                                                   select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }

                    if (sd.searchUrl.ToLower().Contains("bedrooms_or"))
                    {
                        string beds = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("or_bedrooms")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {
                            Refinement r = null;

                            if(beds.Contains("lots"))
                                r =(from x in rd.refinements
                                    where x.urlValue.Contains(beds)
                                    select x).FirstOrDefault();
                            else
                                r = (from x in rd.refinements
                                     where x.name.ToLower().Equals(beds.ToLower())
                                     select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if (t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                                   where x.name.ToLower().Equals("or_bedrooms")
                                                                   select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }

                    if (sd.searchUrl.ToLower().Contains("sleeps_or"))
                    {
                        string sleeps = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("or_sleeps")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {

                            Refinement r = null;

                            if(sleeps.Contains("lots"))
                                r = (from x in rd.refinements
                                    where x.urlValue.Contains("lots")
                                    select x).FirstOrDefault();
                            else
                                r = (from x in rd.refinements
                                     where x.name.ToLower().Equals("sleeps " + sleeps.ToLower())
                                     select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if (t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                                   where x.name.ToLower().Equals("or_sleeps")
                                                                   select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }

                    if(sd.searchUrl.ToLower().Contains("amenities_and"))
                    {
                        string amenity = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("and_amenities")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {
                            Refinement r = (from x in rd.refinements
                                            where x.name.Trim().ToLower().Equals(amenity.ToLower())
                                            select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if (t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                                   where x.name.ToLower().Equals("and_amenities")
                                                                   select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }

                    if (sd.searchUrl.ToLower().Contains("building"))
                    {
                        string building = key;

                        RefinementDimensions rd = (from x in refinements
                                                   where x.name.ToLower().Equals("or_buildingcategory")
                                                   select x).FirstOrDefault();

                        if (rd != default(RefinementDimensions))
                        {
                            Refinement r = (from x in rd.refinements
                                            where x.urlValue.ToLower().Equals(building.ToLower())
                                            select x).FirstOrDefault();

                            foreach (RefinementDimensions y in refinements)
                            {
                                bool t = y.refinements.Remove(r);

                                if (t)
                                {
                                    RefinementDimensions currRd = (from x in currentRefinements
                                                                   where x.name.ToLower().Equals("or_buildingcategory")
                                                                   select x).FirstOrDefault();

                                    if (currRd != default(RefinementDimensions))
                                    {
                                        int index = currentRefinements.IndexOf(currRd);

                                        currentRefinements[index].refinements.Add(r);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Thread safe concurrent bags, to hold filled property infos
            List<PropertyInfo> properties = new List<PropertyInfo>();
            List<PropertyInfo> featuredProperties = new List<PropertyInfo>();

            //Fill the property infos, via threads
            Task[] tasks = new Task[2];
            List<PropertyHit> subset = new List<PropertyHit>();

            if(sortedProperties.Count > (recordOffset + maxReturns))
            {
                subset = sortedProperties.GetRange(recordOffset != 0 ? recordOffset-1 : 0, maxReturns);
            }
            else if(recordOffset == 0 && sortedProperties.Count < maxReturns)
            {
                subset = sortedProperties;
            }
            else if((recordOffset + maxReturns) - sortedProperties.Count < maxReturns)
            {
                subset = sortedProperties.GetRange(recordOffset-1, sortedProperties.Count - recordOffset);
            }
            //else if(sortedProperties.Count < maxReturns)
            //{
            //    subset = sortedProperties;
            //}

            tasks[0] = Task.Run(() => helper.FillPropertyInfos(subset, properties));
            tasks[1] = Task.Run(() => helper.FillPropertyInfos(sortedFeatures, featuredProperties));
            Task.WaitAll(tasks);

            //Construct the object to be returned to the client
            ReturnToLegoLand returnToLegoLand = new ReturnToLegoLand()
            {
                featuredProperties = featuredProperties.ToList(),
                propertyInfos = properties.ToList(),
                refinementDimensions = refinements,
                currentDimensions = currentRefinements,
                TotalRecords = sortedProperties.Count
            };

            return returnToLegoLand;
        }

        /// <summary>
        /// Get PropertyHit results, based on information provided by the client, for base query results
        /// </summary>
        /// <param name="status"></param>
        /// <param name="luxury"></param>
        /// <param name="countries"></param>
        /// <param name="destinations"></param>
        /// <param name="destinationTypeCategories"></param>
        /// <param name="resorts"></param>
        /// <param name="propertyTypes"></param>
        /// <param name="businessUnits"></param>
        /// <param name="bedrooms"></param>
        /// <param name="cities"></param>
        /// <param name="neighborhoods"></param>
        /// <param name="states"></param>
        /// <returns></returns>
        private Tuple<List<PropertyHit>, List<PropertyHit>> GetBaseQueryResults(string status, string luxury, List<string> countries, List<string> destinations, List<string> destinationTypeCategories,
            List<string> resorts, List<string> propertyTypes, List<string> businessUnits, List<string> bedrooms, List<string> cities, List<string> neighborhoods, List<string> states)
        {
            PropertyQuery propertyQuery = new PropertyQuery();
            List<PropertyHit> propertySources = new List<PropertyHit>();
            List<PropertyHit> featuredPropertySources = new List<PropertyHit>();

            if (countries.Count > 0)
            {
                foreach (string c in countries)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesByCountry(c);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        if (c.ToLower().Equals("mexico"))
                        {
                            propertySources.AddRange((from x in propertyResults.hits.hits
                                                      where x._source.BuildingType != null && !x._source.BuildingType.ToLower().Contains("allinclusive") &&
                                                      !x._source.BuildingType.ToLower().Contains("cabin") && !x._source.BuildingType.ToLower().Contains("lodge")
                                                      select x).ToList());
                        }
                        else
                            propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (destinations.Count > 0)
            {
                foreach (string d in destinations)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyDestination(d);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (destinationTypeCategories.Count > 0)
            {
                foreach (string d in destinationTypeCategories)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyDestinationCategoryName(d);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            //if (resorts.Count > 0)
            //{
            //    foreach (string r in resorts)
            //    {
            //        ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyResort(r);

            //        if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //        {
            //            propertySources.AddRange(propertyResults.hits.hits);
            //        }
            //    }
            //}

            if (cities.Count > 0)
            {
                foreach (string c in cities)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyCity(c);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (neighborhoods.Count > 0)
            {
                foreach (string n in neighborhoods)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyNeighborhood(n);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (bedrooms.Count > 0)
            {
                foreach (string b in bedrooms)
                {
                    ElasticPropertySearchResults propertyResults = null;

                    if(luxury == "1")
                        propertyResults = propertyQuery.GetLuxuryPropertiesbyBed(b);
                    else
                        propertyResults = propertyQuery.GetPropertiesbyBed(b);

                    propertySources.AddRange(propertyResults.hits.hits);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (states.Count > 0)
            {
                foreach (string s in states)
                {
                    ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertiesbyState(s);

                    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
                    {
                        propertySources.AddRange(propertyResults.hits.hits);
                    }
                }
            }

            if (status != string.Empty)
            {
                propertySources = (from x in propertySources
                                   where x._source.Status.Equals(status)
                                   select x).ToList();
            }

            if (luxury == "1")
            {
                propertySources = (from x in propertySources
                                   where x._source.IsLuxury == true
                                   select x).ToList();
            }

            propertySources = (from x in propertySources
                               where x._source.Status.ToLower().Equals("active")
                               select x).ToList();

            featuredPropertySources = (from x in propertySources
                                       where x._source.IsFeaturedProperty == true
                                       select x).ToList();

            return new Tuple<List<PropertyHit>, List<PropertyHit>>(propertySources, featuredPropertySources);
        }

    }
}
