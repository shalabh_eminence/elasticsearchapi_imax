﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Amenities;
using ElasticSearch.Models.Responses.Geography;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearch.Models.Responses.RentalUnitContent;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Core.DataAccess.Impl;
using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using ElasticSearch.Models.Responses.NightlyAverage;
using ElasticSearch.Models.Responses.WeeklyRates;
using ElasticSearch.Models.Responses.Neighborhoods;
using System.Collections.Concurrent;
using Newtonsoft.Json;
using ElasticSearchCore.Models.Insert;
using ElasticSearchCore.Models.Responses.Amenities;
using ElasticSearchCore.Models.Responses.BusinessUnits;
using System.Net;
using ElasticSearchLoaderBulk2.Models;
using ElasticSearchCore.Models.Responses.Calendars;
using System.Net.Http;
using HtmlAgilityPack;

namespace ElasticSearchLoaderBulk2
{
    class Log
    {
        public void WriteLog(string message)
        {
            using (var writer = new StreamWriter(@"log.txt", true))
            {
                writer.WriteLine(message);
            }
        }
    }

    class Program
    {
        private static EndecaAmenitiesViewRepository _amenitiesRepo;
        private static EndecaPropertyViewRepository _propertiesRepo;
        private static EndecaWeeklyRatesViewRepository _weeklyRatesRepo;
        private static NightlyRateRepository _nightlyRateRepository;
        private static EndecaPromotionViewRepository _promotionRepo;
        private static MerchandisingLandingPageRepository _landingPageRepo;
        private static EndecaResortViewRepository _resortRepo;
        private static EndecaNeighborhoodViewRepository _neighborhoodRepo;
        private static EndecaGeographyViewRepository _geographyRepo;
        private static RentalUnitContentViewRepository _rentalUnitContentRepo;
        private static EndecaBusinessUnitRepository _endecaBusinessUnitRepository;

        static void Main(string[] args)
        {
            string host = ConfigurationManager.AppSettings.Get("ElasticSearchHost");
            int port = int.Parse(ConfigurationManager.AppSettings.Get("ElasticSearchPort"));

            Log l = new Log();
            l.WriteLog($"{DateTime.Now} Starting");

            _propertiesRepo = new EndecaPropertyViewRepository();
            _amenitiesRepo = new EndecaAmenitiesViewRepository();
            _weeklyRatesRepo = new EndecaWeeklyRatesViewRepository();
            _nightlyRateRepository = new NightlyRateRepository();
            _promotionRepo = new EndecaPromotionViewRepository();
            _landingPageRepo = new MerchandisingLandingPageRepository();
            _resortRepo = new EndecaResortViewRepository();
            _neighborhoodRepo = new EndecaNeighborhoodViewRepository();
            _geographyRepo = new EndecaGeographyViewRepository();
            _rentalUnitContentRepo = new RentalUnitContentViewRepository();
            _endecaBusinessUnitRepository = new EndecaBusinessUnitRepository();

            try
            {
                EndecaPropertyView pv = _propertiesRepo.GetPropertyByInventoryID(0);
            }
            catch (Exception)
            {
                string exception = "Could not connect to the database";
                l.WriteLog(exception);
                System.Threading.Thread.Sleep(3000);
                throw new System.InvalidOperationException(exception);
            }

            List<EndecaPropertyView> endecaProperties = null;
            List<EndecaAmenitiesView> endecaAmenities = null;
            List<EndecaGeographyView> endecaGeographies = null;
            List<RentalUnitContentView> endecaRentalUnitContents = null;
            List<ElasticSearchCore.Models.Insert.NightlyAverage> nightlyAverages = null;
            List<EndecaWeeklyRatesView> endecaWeeklyRates = null;
            List<EndecaNeighborhoodView> endecaNeighborhoods = null;
            List<string> splurgeAmenities = null;
            List<string> endecaBusinessUnitNames = null;

            //List<ElasticSearchCore.Core.Domain.NightlyRate> nightlyRates = _nightlyRateRepository.GetOneYearNightlyRatesForProperties();

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            var lowlevelClient = new ElasticLowLevelClient(settings);

            PropertyQuery pq = new PropertyQuery();
            AmenityQuery aq = new AmenityQuery();
            GeographyQuery gq = new GeographyQuery();
            RentalUnitContentQuery rq = new RentalUnitContentQuery();
            NightlyAverageQuery naq = new NightlyAverageQuery();
            WeeklyRateQuery wrq = new WeeklyRateQuery();
            NeighborhoodQuery nq = new NeighborhoodQuery();
            CalendarQuery calendarQuery = new CalendarQuery();

            List<Task> calendarTasks = new List<Task>();

            #region Properties
            endecaProperties = _propertiesRepo.GetAllProperties();
            List<PropertySource> propSrc = pq.GetAllProperties();

            //remove unnecessary documents from elasticsearch
            foreach (PropertySource s in propSrc)
            {
                int i = (from x in endecaProperties
                            where x.InventoryID == s.InventoryID
                            select x).Count();

                if (i == 0)
                {
                    pq.DeletePropertyByInventoryID(s.InventoryID);
                }
            }

            ProcessProperties(settings, endecaProperties);
            #endregion Properties

            #region Amenities
            endecaAmenities = _amenitiesRepo.GetAllAmenitiesWithProperties();
            List<AmenityHit> amenSrc = aq.GetAllAmenities();
            ConcurrentBag<EndecaAmenitiesView> notInElasticSearchAmen = new ConcurrentBag<EndecaAmenitiesView>();
            List<Task> tasks = new List<Task>();

            //remove unnecessary documents from elasticsearch
            foreach (AmenityHit s in amenSrc)
            {
                tasks.Add(Task.Run(() =>
                {
                    EndecaAmenitiesView i = (from x in endecaAmenities
                                                where x.InventoryID == s._source.InventoryID &&
                                                x.AttribName == s._source.AttribName
                                                select x).FirstOrDefault();

                    if (i == default(EndecaAmenitiesView))
                    {
                        aq.DeleteAmenity(s._id);
                    }
                }));
            }

            Task.WaitAll(tasks.ToArray());
            tasks.Clear();

            //find out which amenities are new to ES
            foreach (EndecaAmenitiesView v in endecaAmenities)
            {
                tasks.Add(Task.Run(() =>
                {
                    AmenityHit hit = (from x in amenSrc
                                        where x._source.InventoryID == v.InventoryID &&
                                        x._source.AttribName == v.AttribName
                                        select x).FirstOrDefault();

                    if (hit == default(AmenityHit))
                    {
                        notInElasticSearchAmen.Add(v);
                    }
                }));
            }

            Task.WaitAll(tasks.ToArray());

            ProcessAmenities(settings, notInElasticSearchAmen.ToList());
            #endregion Amenities
            
            #region Geographies
            endecaGeographies = _geographyRepo.GetAllGeographyThatHaveProperties();
            List<GeographyHit> geoSrc = gq.GetAllGeographies();

            foreach (GeographyHit s in geoSrc)
            {
                int i = (from x in endecaGeographies
                            where x.InventoryID == s._source.InventoryID
                            select x).Count();

                if (i == 0)
                {
                    gq.DeleteGeographyById(s._id);
                }
            }

            ProcessGeographies(settings, endecaGeographies);
            #endregion Geographies
           
            #region RentalUnits
            endecaRentalUnitContents = _rentalUnitContentRepo.GetAllRentalContentViewWithProperties();
            List<RentalUnitContentHit> rentalSrc = rq.GetAllRentalUnitContents();

            foreach (RentalUnitContentHit s in rentalSrc)
            {
                int i = (from x in endecaRentalUnitContents
                            where x.InventoryId == s._source.InventoryId
                            select x).Count();

                if (i == 0)
                {
                    rq.DeleteRentalUnitContentById(s._id);
                }
            }

            ProcessRentalUnitContent(settings, endecaRentalUnitContents);
            #endregion RentalUnits

            #region NightlyAverages
            nightlyAverages = _nightlyRateRepository.GetYearAverageNightlyRate();
            List<NightlyAverageHit> nightlyAvgSrc = naq.GetAllNightlyAverages();

            foreach (NightlyAverageHit s in nightlyAvgSrc)
            {
                int i = (from x in nightlyAverages
                            where x.InventoryID == s._source.InventoryID
                            select x).Count();

                if (i == 0)
                {
                    naq.DeleteAmenity(s._id);
                }
            }

            ProcessNightlyAverages(settings, nightlyAverages);
            #endregion NightlyAverages

            #region WeeklyRates
            endecaWeeklyRates = _weeklyRatesRepo.GetAllWeeklyRatesWithProperties();
            List<WeeklyRateHit> ratesSrc = wrq.GetAllWeeklyRates();

            foreach (WeeklyRateHit s in ratesSrc)
            {
                DateTime? start = s._source.FromRateStartDate == null ? (DateTime?)null : DateTime.Parse(s._source.FromRateStartDate);
                DateTime? end = s._source.FromRateEndDate == null ? (DateTime?)null : DateTime.Parse(s._source.FromRateEndDate);

                int i = (from x in endecaWeeklyRates
                            where x.InventoryID == s._source.InventoryID &&
                            x.FromRateStartDate == start &&
                            x.FromRateEndDate == end
                            select x).Count();

                if (i == 0)
                {
                    wrq.DeleteWeeklyRate(s._id);
                }
            }

            ProcessWeeklyRates(settings, endecaWeeklyRates);
            #endregion WeeklyRates

            #region Neighborhoods
            endecaNeighborhoods = _neighborhoodRepo.GetAllNeighborhoodsWithProperties();
            List<NeighborhoodHit> neighborhoodSrc = nq.GetAllNeighborhoods();

            foreach (NeighborhoodHit s in neighborhoodSrc)
            {
                int i = (from x in endecaNeighborhoods
                            where x.InventoryID == s._source.InventoryID
                            select x).Count();

                if (i == 0)
                {
                    nq.DeleteNeighborhood(s._id);
                }
            }

            ProcessNeighborhoods(settings, endecaNeighborhoods);
            #endregion Neighborhoods

            #region SplurgeAmenities
            splurgeAmenities = _amenitiesRepo.GetSplurgeAmenities();
            List<SplurgeAmenityHit> src = aq.GetSplurgeAmenities();
            List<string> notInElasticSearch = new List<string>();

            //check to see that no splurge amenities were removed from the database
            //but if so, also remove from elasticsearch
            foreach (SplurgeAmenityHit s in src)
            {
                int i = (from x in splurgeAmenities
                            where x == s._source.AmenityName
                            select x).Count();

                if (i == 0)
                {
                    aq.DeleteSplurgeAmenity(s._id);
                }
            }

            //find out which amenities are new to ES, but are in the database
            foreach (string s in splurgeAmenities)
            {
                SplurgeAmenityHit hit = (from x in src
                                            where x._source.AmenityName == s
                                            select x).FirstOrDefault();

                if (hit == default(SplurgeAmenityHit))
                {
                    notInElasticSearch.Add(s);
                }
            }

            ProcessSplurgeAmenities(settings, notInElasticSearch);
            #endregion SplurgeAmenities

             #region BusinessUnits
             endecaBusinessUnitNames = _endecaBusinessUnitRepository.GetBusinessUnitNames();
                 ProcessBusinessUnits(settings, endecaBusinessUnitNames);
             #endregion BusinessUnits

            #region Calendar
            foreach (RentalUnitContentView rental in endecaRentalUnitContents)
            {
                calendarTasks.Add(Task.Run(() =>
                {
                    WebClient webclient = new WebClient();
                    string response = webclient.DownloadString($"https://calendars.realvoice.com/calendars/v1/en-US/confirmationMethod/{rental.RentalUnitId}");

                    CalendarReply reply = JsonConvert.DeserializeObject<CalendarReply>(response);

                    if (reply.Code == "Success")
                    {
                        // Get nightly rates for this rental unit
                        List<ElasticSearchCore.Core.Domain.NightlyRate> nightlyRates = _nightlyRateRepository.GetOneYearNightlyRatesForProperties(rental.RentalUnitId);
                        List<CalendarHit> calendarHits = calendarQuery.GetRatesAndAvailability(rental.InventoryId);

                        if (nightlyRates.Count > 0)
                        {
                            List<Day> days = new List<Day>();

                            // For each day, get a rate
                            foreach (Models.Day day in reply.Calendar.Days)
                            {
                                NightlyRate rate = (from x in nightlyRates
                                                    where x.Date == day.Date &&
                                                    x.RentalUnitId == rental.RentalUnitId
                                                    select x).FirstOrDefault();

                                // If there is a rate for this particular day
                                // Insert into Elasticsearch (note: the _id should prevent duplicates
                                // and just update the current rate)
                                if (rate != default(NightlyRate))
                                {
                                    day.Rate = rate.RackRate;
                                    day.InventoryID = rental.InventoryId;
                                    days.Add(day);
                                }
                            }

                            // Check to see if Elasticsearch has records that
                            // are no longer supplied from the database (eg. old dates)
                            foreach (CalendarHit hit in calendarHits)
                            {
                                int count = (from x in nightlyRates
                                                where x.Date == hit._source.Date
                                                select x).Count();

                                // If so, then delete them from ElasticSearch
                                if (count == 0)
                                    calendarQuery.DeleteCalendar(hit._id);
                            }

                            if (days.Count > 0)
                            {
                                ProcessRatesAndAvailability(settings, days);
                            }
                        }
                    }
                }));
            }
            Task.WaitAll(calendarTasks.ToArray());
            #endregion Calendar

            #region Images
            endecaProperties = _propertiesRepo.GetAllProperties();
            ProcessImages(settings, endecaProperties);
            #endregion Images
            
            l = new Log();
            l.WriteLog($"{DateTime.Now} ending");
        }

        static void ProcessProperties(ConnectionConfiguration settings, List<EndecaPropertyView> endecaProperties)
        {
            EndecaBusinessUnitRepository endecaBusinessUnitRepository = new EndecaBusinessUnitRepository();

            var lowlevelClient = new ElasticLowLevelClient(settings);
            List<PropertySource> properties = new List<PropertySource>();
            Log l = new Log();

            foreach (EndecaPropertyView epv in endecaProperties)
            {
                List<EndecaBusinessUnit> endecaBusinessUnits = endecaBusinessUnitRepository.GetBusinessUnitsByInventoryID(epv.InventoryID);

                PropertySource p = new PropertySource()
                {
                    InventoryID = epv.InventoryID,
                    InventoryType = epv.InventoryType,
                    PropertyView = epv.propertyView,
                    SourceInventoryID = epv.SourceInventoryID,
                    InventoryName = epv.InventoryName,
                    NumImages = epv.NumImages,
                    Bedroom = epv.Bedroom == null ? 0 : (int)epv.Bedroom,
                    Sleeps = epv.Sleeps == null ? 0 : (int)epv.Sleeps,
                    InventoryDescription = epv.InventoryDescription,
                    ComplexName = epv.ComplexName,
                    BuildingName = epv.BuildingName,
                    Policies = epv.Policies,
                    DisplaySubOrder = epv.DisplaySubOrder,
                    VirtualTourURL = epv.VirtualTourURL,
                    VirtualTourFilmDate = epv.VirtualTourFilmDate == null ? "" : ((DateTime)epv.VirtualTourFilmDate).ToString(),
                    HasVirtualTour = epv.HasVirtualTour == null ? false : (bool)epv.HasVirtualTour,
                    ExpertReview = epv.ExpertReview,
                    RandomDisplay = epv.RandomDisplay,
                    EmailBooking = epv.EmailBooking == 0 ? false : true,
                    GeoPosition = $"{epv.Lat},{epv.Long}",
                    PropertyProximity = epv.PropertyProximity,
                    Status = epv.Status,
                    CurrencyType = epv.CurrencyType,
                    IsLuxury = epv.IsLuxury,
                    PropertyDisclaimer = epv.PropertyDisclaimer,
                    IsFeaturedProperty = epv.IsFeaturedProperty == null ? false : (bool)epv.IsFeaturedProperty,
                    IsActiveOnIndustrySite = epv.IsActiveOnIndustrySite == 0 ? false : true,
                    IsIndustrySiteOnlineBookingEnabled = epv.IsIndustrySiteOnlineBookingEnabled == 0 ? false : true,
                    IndustrySiteSupplierPhoneNumber = epv.IndustrySiteSupplierPhoneNumber == 0 ? false : true,
                    GatewayProviderTypeCode = epv.GatewayProviderTypeCode,
                    SupplierName = epv.SupplierName,
                    PropertyRating = epv.PropertyRating == null ? 0 : (int)epv.PropertyRating,
                    BuildingType = epv.BuildingType,
                    BusinessUnitNames = (from x in endecaBusinessUnits
                                         select x.BusinessUnitName).ToList()
                };
                properties.Add(p);
            }

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (PropertySource p in properties)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"properties\", \"_type\": \"property\", \"_id\":\"" + p.InventoryID + "\"} }" + Environment.NewLine + JsonConvert.SerializeObject(p) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert properties {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }

        static void ProcessAmenities(ConnectionConfiguration settings, List<EndecaAmenitiesView> endecaAmenities)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            List<AmenitySource> amenities = new List<AmenitySource>();
            Log l = new Log();

            foreach (EndecaAmenitiesView eav in endecaAmenities)
            {
                if (eav == null)
                    continue;

                AmenitySource a = new AmenitySource()
                {
                    InventoryID = eav.InventoryID,
                    AttribName = eav.AttribName
                };
                amenities.Add(a);
            }

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (AmenitySource src in amenities)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"amenities\", \"_type\": \"amenity\"} }" + Environment.NewLine + JsonConvert.SerializeObject(src) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert amenities {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }

        static void ProcessGeographies(ConnectionConfiguration settings, List<EndecaGeographyView> endecaGeographies)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            List<GeographySource> geography = new List<GeographySource>();
            Log l = new Log();

            foreach(EndecaGeographyView egv in endecaGeographies)
            {
                GeographySource g = new GeographySource()
                {
                    InventoryID = egv.InventoryID,
                    CityName = egv.CityName,
                    StateName = egv.StateName,
                    StateCode = egv.StateCode,
                    Country = egv.Country,
                    CountryCode = egv.Country,
                    IslandName = egv.IslandName,
                    Destination = egv.Destination,
                    DestinationCategoryName = egv.DestinationCategoryName
                };
                geography.Add(g);
            }

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (GeographySource g in geography)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"geographies\", \"_type\": \"geography\", \"_id\":\"" + g.InventoryID + "\"} }" + Environment.NewLine + JsonConvert.SerializeObject(g) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert geographies {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }

        static void ProcessRentalUnitContent(ConnectionConfiguration settings, List<RentalUnitContentView> endecaRentalUnitContent)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            List<RentalUnitContentSource> rentalUnits = new List<RentalUnitContentSource>();
            Log l = new Log();

            foreach(RentalUnitContentView rucv in endecaRentalUnitContent)
            {
                RentalUnitContentSource rentalUnit = new RentalUnitContentSource()
                {
                    RentalUnitId = rucv.RentalUnitId,
                    RentalUnitType = rucv.RentalUnitType,
                    InventoryId = rucv.InventoryId,
                    InventoryMode = rucv.InventoryMode,
                    UnitNumber = rucv.UnitNumber,
                    IsActive = rucv.IsActive,
                    SupplierId = rucv.SupplierId,
                    CurrencyCode = rucv.CurrencyCode,
                    IsLuxury = rucv.IsLuxury == null ? false : (bool)rucv.IsLuxury,
                    IsFeaturedProperty = rucv.IsFeaturedProperty == null ? false : (bool)rucv.IsFeaturedProperty,
                    GatewayPropertyId = rucv.GatewayPropertyId,
                    IsAvailableForBooking = !rucv.RatesRequireVerification,
                    Occupancy = rucv.Occupancy == null ? 0 : (int)rucv.Occupancy,
                    UnitDescription = rucv.UnitDescription,
                    Disclaimer = rucv.Disclaimer,
                    Bedding = rucv.Bedding,
                    StayRestrictionCalendarId = rucv.StayRestrictionCalendarId == null ? 0 : (int)rucv.StayRestrictionCalendarId,
                    DisplayPhotoDisclaimer = rucv.DisplayPhotoDisclaimer,
                    ImageCount = rucv.ImageCount,
                    DisplayOnline = rucv.DisplayOnline,
                    TaxId = rucv.TaxId,
                    PermitNumber = rucv.PermitNumber,
                    RateCalendarId = rucv.RateCalendarId == null ? 0 : (long)rucv.RateCalendarId,
                    CheckInTime = rucv.CheckInTime,
                    CheckOutTime = rucv.CheckOutTime,
                    SupIsAvailableForBooking = rucv.SupIsAvailableForBooking,
                    Latitude = rucv.Latitude == null ? "" : rucv.Latitude.ToString(),
                    Longitude = rucv.Longitude == null ? "" : rucv.Longitude.ToString(),
                    Proximity = rucv.Proximity,
                    ComplexName = rucv.ComplexName,
                    ComplexDescription = rucv.ComplexDescription,
                    Address1 = rucv.Address1,
                    Address2 = rucv.Address2,
                    PostalCode = rucv.PostalCode,
                    StateCode = rucv.StateCode,
                    PropertyCountryCode = rucv.PropertyCountryCode,
                    City = rucv.City,
                    BuildingName = rucv.BuildingName,
                    BuildingDescription = rucv.BuildingDescription,
                    BuildingType = rucv.BuildingType,
                    FloorPlanType = rucv.FloorPlanType,
                    Bedrooms = rucv.Bedrooms,
                    IsBathroomPrivate = rucv.IsBathroomPrivate,
                    FullBathrooms = rucv.FullBathrooms,
                    ThreeQuarterBathrooms = rucv.ThreeQuarterBathrooms,
                    HalfBathrooms = rucv.HalfBathrooms,
                    PrivateCalendar = rucv.PrivateCalendar,
                    RatesRequireVerification = rucv.RatesRequireVerification,
                    RequiresManualPricing = rucv.RequiresManualPricing,
                    DestinationId = rucv.DestinationId == null ? 0 : (int)rucv.DestinationId,
                    CityId = rucv.CityId == null ? 0 : (int)rucv.CityId,
                    NeighborhoodId = rucv.NeighborhoodId == null ? 0 : (int)rucv.NeighborhoodId,
                    ResortGroupId = rucv.ResortGroupId == null ? 0 : (int)rucv.ResortGroupId,
                    MerchandisingOrder = rucv.MerchandisingOrder,
                    RentalUnitOrder = rucv.RentalUnitOrder == null ? 0 : (int)rucv.RentalUnitOrder,
                    SupplierName = rucv.SupplierName,
                    SupplierPrimaryDestinationId = rucv.SupplierPrimaryDestinationId == null ? 0 : (long)rucv.SupplierPrimaryDestinationId,
                    SupplierTier = rucv.SupplierTier == null ? 0 : (int)rucv.SupplierTier,
                    IsSupplierActive = rucv.IsSupplierActive == null ? false : (bool)rucv.IsSupplierActive,
                    RandomDisplay = rucv.RandomDisplay,
                    AvailabilityCalendarId = rucv.AvailabilityCalendarId == null ? 0 : (long)rucv.AvailabilityCalendarId,
                    SalesNoteCollectionId = rucv.SalesNoteCollectionId == null ? 0 : (long)rucv.SalesNoteCollectionId,
                    NoteCollectionId = rucv.NoteCollectionId == null ? 0 : (long)rucv.NoteCollectionId,
                    PropertyRating = rucv.PropertyRating == null ? 0 : (int)rucv.PropertyRating
                };

                rentalUnits.Add(rentalUnit);
            }

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (RentalUnitContentSource ruc in rentalUnits)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"rentalunitcontents\", \"_type\": \"rentalUnitContent\", \"_id\":\"" + ruc.InventoryId + "\"} }" + Environment.NewLine + JsonConvert.SerializeObject(ruc) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert rentalunitcontents {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }

        static void ProcessNightlyAverages(ConnectionConfiguration settings, List<ElasticSearchCore.Models.Insert.NightlyAverage> nightlyAverages)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            Log l = new Log();

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (ElasticSearchCore.Models.Insert.NightlyAverage na in nightlyAverages)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"nightlyaverages\", \"_type\": \"nightlyAverage\", \"_id\":\"" + na.InventoryID + "\"} }" + Environment.NewLine + JsonConvert.SerializeObject(na) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert nightlyaverages {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }

        static void ProcessNightlyRates(ConnectionConfiguration settings, List<ElasticSearchCore.Core.Domain.NightlyRate> nightlyRates)
        {
            //var lowlevelClient = new ElasticLowLevelClient(settings);
            //List<ElasticSearchLoaderBulk2.Models.ElasticSearchModels.NightlyRate> rates = new List<ElasticSearchLoaderBulk2.Models.ElasticSearchModels.NightlyRate>();
            //Log l = new Log();

            //foreach(ElasticSearchCore.Core.Domain.NightlyRate nr in nightlyRates)
            //{
            //    ElasticSearchLoaderBulk2.Models.ElasticSearchModels.NightlyRate r = new Models.ElasticSearchModels.NightlyRate()
            //    {
            //        RentalUnitId = nr.RentalUnitId,
            //        Date = nr.Date,
            //        RackRate = nr.RackRate
            //    };

            //    var indexResponse = lowlevelClient.Index<BytesResponse>("nightlyrates", "NightlyRate", nr.NightlyRateId.ToString(), PostData.Serializable(r));

            //    if (!indexResponse.Success)
            //    {
            //        string exception = $"Error inserting NightlyRate with nightlyRateID: {nr.NightlyRateId.ToString()}{Environment.NewLine}";
            //        l.WriteLog(exception);
            //    }
            //}
        }

        static void ProcessWeeklyRates(ConnectionConfiguration settings, List<EndecaWeeklyRatesView> endecaWeeklyRates)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            Log l = new Log();

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (EndecaWeeklyRatesView weeklyRate in endecaWeeklyRates)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"weeklyrates\", \"_type\": \"weeklyRate\", \"_id\":\"" + weeklyRate.InventoryID + "\"} }" + Environment.NewLine + JsonConvert.SerializeObject(weeklyRate) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert weeklyRates {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }

        static void ProcessNeighborhoods(ConnectionConfiguration settings, List<EndecaNeighborhoodView> endecaNeighborhoods)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            Log l = new Log();

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (EndecaNeighborhoodView neighborhood in endecaNeighborhoods)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"neighborhoods\", \"_type\": \"neighborhood\", \"_id\":\""+ neighborhood.InventoryID +"\"} }" + Environment.NewLine + JsonConvert.SerializeObject(neighborhood) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert neighborhoods {Environment.NewLine}";
                l.WriteLog(exception);
            }

            //foreach (EndecaNeighborhoodView neighborhood in endecaNeighborhoods)
            //{
            //    var indexResponse = lowlevelClient.Index<BytesResponse>("neighborhoods", "neighborhood", neighborhood.InventoryID.ToString(), PostData.Serializable(neighborhood));

            //    if (!indexResponse.Success)
            //    {
            //        string exception = $"Error inserting Neighborhood with InventoryID: {neighborhood.InventoryID.ToString()}{Environment.NewLine}";
            //        l.WriteLog(exception);
            //    }
            //}
        }

        static void ProcessSplurgeAmenities(ConnectionConfiguration settings, List<string> splurgeAmenities)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            Log l = new Log();

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (string amenity in splurgeAmenities)
            {
                Task t = Task.Run(() =>
                {
                    SplurgeAmenity sa = new SplurgeAmenity()
                    {
                        AmenityName = amenity
                    };

                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"splurgeamenities\", \"_type\": \"splurgeAmenity\"} }" + Environment.NewLine + JsonConvert.SerializeObject(sa) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            if (bulkPostParts.Count > 0)
            {
                var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

                if (!indexResponse.Success)
                {
                    string exception = $"Couldn't bulk insert splurgeAmenities {Environment.NewLine}";
                    l.WriteLog(exception);
                }
            }
        }

        static void ProcessBusinessUnits(ConnectionConfiguration settings, List<string> endecaBusinessUnitNames)
        {
            BusinessUnitQuery businessUnitQuery = new BusinessUnitQuery();

            var lowlevelClient = new ElasticLowLevelClient(settings);
            Log l = new Log();

            foreach (string businessUnitName in endecaBusinessUnitNames)
            {
                List<EndecaBusinessUnit> endecaBusinessUnits = _endecaBusinessUnitRepository.GetBusinessUnitsByBusinessUnitName(businessUnitName);
                List<BusinessUnitHit> businessUnitHits = businessUnitQuery.GetAllBusinessUnitsByBusinessUnitName(businessUnitName);

                ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();
                List<Task> task2 = new List<Task>();

                //Check to see if what Elasticsearch has matches the database
                foreach(BusinessUnitHit buh in businessUnitHits)
                {
                    task2.Add(Task.Run(() => {

                        //Is the elasticsearch business unit in the database list?
                        int endecaHits = (from x in endecaBusinessUnits
                                          where x.BusinessUnitName == buh._source.BusinessUnitName &&
                                          x.InventoryId == buh._source.InventoryId &&
                                          x.RentalUnitId == buh._source.RentalUnitId
                                          select x).Count();

                        // If Elasticsearch has a record that the database doesn't,
                        // remove that record from Elasticsearch
                        if(endecaHits == 0)
                        {
                            businessUnitQuery.DeleteBusinessUnitById(buh._id);
                        }
                    }));
                }

                Task.WaitAll(task2.ToArray());

                List<Task> outerTask = new List<Task>();

                // Check what the database has, against what is in Elasticsearch.
                // If a record is not in elasticsearch, put it in there.
                foreach (EndecaBusinessUnit businessUnit in endecaBusinessUnits)
                {
                    outerTask.Add(Task.Run(()=> { 
                        // See if the Business Unit exists in Elasticsearch
                        bool exists = businessUnitQuery.BusinessUnitExists(businessUnit.BusinessUnitName, businessUnit.InventoryId, businessUnit.RentalUnitId);

                        // If it doesn't, add it
                        if (!exists)
                        {
                            bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"businessunits\", \"_type\": \"businessUnit\"} }" + Environment.NewLine + JsonConvert.SerializeObject(businessUnit) + Environment.NewLine);
                        }
                    }));
                }

                Task.WaitAll(outerTask.ToArray());

                if (bulkPostParts.Count > 0)
                {
                    var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

                    if (!indexResponse.Success)
                    {
                        string exception = $"Couldn't bulk insert businessUnits with {businessUnitName} {Environment.NewLine}";
                        l.WriteLog(exception);
                    }
                }
            } 
        }

        static void ProcessRatesAndAvailability(ConnectionConfiguration settings, List<Day> days)
        {
            var lowlevelClient = new ElasticLowLevelClient(settings);
            Log l = new Log();

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (Day day in days)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"calendars\", \"_type\": \"calendar\", \"_id\":\"" + day.InventoryID + "_" + day.DayId + "\"} }" + Environment.NewLine + JsonConvert.SerializeObject(day) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert Calendars {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }

        static void ProcessImages(ConnectionConfiguration settings, List<EndecaPropertyView> endecaProperties)
        {
            List<Image> images = new List<Image>();

            foreach (EndecaPropertyView epv in endecaProperties)
            {
                string url = $"http://images.realvoice.com/property/{String.Format("{0:D7}", epv.InventoryID)}/";

                try
                {
                    HttpClient httpClient = new HttpClient();
                    string response = httpClient.GetStringAsync(url).Result;

                    if (response != "")
                    {
                        var htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(response);

                        var nodes = htmlDoc.DocumentNode.SelectNodes("//a");

                        var hrefs = (from x in nodes
                                     where x.InnerText != "[To Parent Directory]"
                                     select x);

                        Image img = new Image()
                        {
                            InventoryID = epv.InventoryID,
                        };

                        foreach (HtmlNode node in hrefs)
                        {
                            img.Images.Add($"http://images.realvoice.com{node.GetAttributeValue("href", "")}");
                        }

                        images.Add(img);
                    }
                }
                catch (Exception)
                {

                }
            }

            var lowlevelClient = new ElasticLowLevelClient(settings);
            Log l = new Log();

            List<Task> tasks = new List<Task>();
            ConcurrentQueue<string> bulkPostParts = new ConcurrentQueue<string>();

            foreach (Image image in images)
            {
                Task t = Task.Run(() =>
                {
                    bulkPostParts.Enqueue("{\"index\" : { \"_index\": \"propertyimages\", \"_type\": \"images\", \"_id\":\"" + image.InventoryID + "\"} }" + Environment.NewLine + JsonConvert.SerializeObject(image) + Environment.NewLine);
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            var indexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(bulkPostParts));

            if (!indexResponse.Success)
            {
                string exception = $"Couldn't bulk insert images {Environment.NewLine}";
                l.WriteLog(exception);
            }
        }
    }
}
