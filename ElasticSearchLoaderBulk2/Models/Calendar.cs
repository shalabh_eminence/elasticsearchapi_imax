﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchLoaderBulk2.Models
{
    public class Day
    {
        public string ConfirmationMethod { get; set; }
        public string DayId { get; set; }
        public DateTime Date { get; set; }
        public decimal Rate { get; set; }
        public long InventoryID { get; set; }
    }
    public class Calendar
    {
        public Calendar()
        {
            Days = new List<Day>();
        }
        public string Type { get; set; }
        public List<Day> Days { get; set; }
    }
    public class CalendarReply
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public Calendar Calendar { get; set; }
    }
}
