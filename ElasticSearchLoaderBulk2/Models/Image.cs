﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchLoaderBulk2.Models
{
    public class Image
    {
        public Image()
        {
            Images = new List<string>();
        }

        public long InventoryID { get; set; }
        public List<string> Images { get; set; }
    }
}
